# The Mendelian Analysis Toolkit ("MATK") Configuration Guide

## The Configurer Role

### Two Parts: Gene Model File and Annotation Binding Code

In order to use the toolset, besides having annotated VCF input, two things are needed:
- A Gene Model File that enumerates any/all known genes for the phenotype under study.  This is very much the domain of Genetic Analysts and Counselors who likely already have much of this information in one form or another.  OMIM could be another source of this information. The information captured includes:
  - known inheritance models
  - maximum plausible population frequency
  - Is the gene known to display haploinsufficient-dominant inheritance?
  - confidence level in the gene/phenotype association


- A Python module (**Yes, this will involve a small amount of not-very complicated Python programming.**) that encodes the necessary "Annotation Bindings" which covers:
   - How/where in the VCF are the annotations stored
   - How should those annotations be used in the scoring and ranking of variants
   - What are the desired output columns when ranking solutions

Each of these is discussed in turn below.

### Gene Model CSV file

The key input of phenotypic domain knowledge is the Gene Model file, which is a simple CSV formatted file with a row of column headers at the top. The **required** columns are:
 -  "**Ensembl**": The Ensembl Gene ID (e.g. ENSG00000198691).  This is the critical ID that a VCF file's annotations are matched against. 
 -  "**Symbol**": The HGNC identifier for the gene.  This is used only for support of "-g" command option to limit to one gene.
 -  "**Disease Models**": Some combination of "AR", "AD", "XL", semi-colon separated
 -  "**AR Pop Freq**": - maximal plausible population frequency for a recessive variant
 -  "**AD Pop Freq**": - maximal plausible population frequency for a dominant variant
 -  "**XL Pop Freq**": - maximal plausible population frequency for an X-linked variant
 -  "*Phenotype*": whose column header is a user-chosen phenotype term, that will be supplied as input parameter to **BuildOnceCache.py**.  Below the header row, the actual column cells contain blank or any non-blank to indicate this gene’s inclusion in this phenotype. This allows multiple phenotypes to be included in a single file, each with their own phenotype column.  A cache file, once built, is phenotype-specific and the phenotype is recorded in the headers.

Additional, optional columns are:
 - "**Confidence**": A numerical value from 0.0 - 1.0 that is used to scale down solution scores for genes of uncertain phenotype association.  Any empty cell is assumed to have value 1.0 (i.e. no scaling)
 - "**HI-Dom**": A value of 'Y', or 'yes' or 1 indicates that this gene is known to have haploinsufficient inheritance and thus should be assessed for Loss-of-Function (rather than GoF) when doing Dominant solution scoring.

In addition to rows describing specific genes, there are two special rows allowed, indicated by the value in the "**Ensembl**" column:
  - "**default**" (in the **Ensembl** column): this row should be at the top, and it supplies default values for any cells that are left blank on the per-gene rows.
  - "*****" (in the **Ensembl** column): supplies values for any other gene, not otherwise specified in the per-gene rows.  This is only used when attempting new-gene-discovery (i.e. looking beyond the known genes for a phenotype).  Warning:  with WES data, including this can result in enormous output volume, unless there is a significant pedigree with which to filter, or the scoring cutoffs are set particularly high.


### Annotation Binding Module (Python Code)

##### Background: The Problem with Annotations
The VCF file format specification provides a standardized format for representing genetic variants. Within this specification, the "INFO" field is used to store “additional information” with "*arbitrary keys*". It is into this field that annotations of use to a genetic analyst are inserted. But the manner of that insertion (under what *arbitrary* key? in what format?) is not standardized. Some annotations (e.g. population frequency data, conservation scores) are single-valued for a given variant and so only the ‘arbitrary key’ and precise data format are undefined. But many annotations are made against transcript or epigenetic models and are many-to-one for a particular variant (e.g. one variant could be interpreted against multiple transcripts, genes or epigenetic features). This requires an extra level of complexity in the INFO field when one tag (usually ‘ANN’), instead of having a single value, or a list of values, actually contains a *list of lists* of values, with each position in the list corresponding to a different fields, each of different data types (and some of which are lists themselves!) and each described in the headers. The actual details how this list of lists is encoded is beyond the scope of the VCF specification and thus subject to still more variability. Mercifully, three of the most prominent tools used for annotations (**VEP**, **Annovar**, **SnpEff**) have worked together to try to standardize this list-of-lists representation - see here for details: http://snpeff.sourceforge.net/VCFannotationformat_v1.0.pdf.  This specification helps to standardize the structural representation of annotation data, but it does not speak to the details of the specific individual annotation fields in use and their interpretation.

##### Where are my annotations?

MATK attempts to ease the programming burden of the Bindings by creating a "VCF-addressable-space" for the annotations in your annotated VCF.  [The VCF file format specification](https://samtools.github.io/hts-specs/VCFv4.2.pdf) clearly defines nine columns (e.g. 'REF', 'ALT', 'INFO'), with most annotation tools placing content into the 'INFO' column which is a series of key-value pairs containing "*additional information*" with "*arbitrary keys*". (uh-oh)  And annotation tools like VEP will generally insert **lists of lists** (double uh-oh) of position-identified (via a header) fields, all joined together as one element (often 'ANN', but sometimes 'DSQ' or 'VEP' or anything else...) in the INFO field. Needless to say, determining *exactly* where some content is in any one site's VCF is a challenge.  MATK's VCF-addressable-space allows the Binding's Python code to use expressions like:

```
Fields['ALT']
Fields['INFO.CLINVAR']
Fields['INFO.ANN.SYMBOL']
```

to refer to field-values in a VCF record.  The first one is simply referring to the ALT field.  The second is refers to the CLINVAR tag (an example of an *arbitrary* tag) in the INFO field.  The third example above refers to the 'SYMBOL' *positional* field (as defined by the VCF header for ANN) in the 'ANN' field, which is a key in the INFO column.  MATK handles the parsing and organizing by Gene of the various annotations that you need to access.  But you need to be able to specify where in **your** annotated VCF file the various values of interest are located - this is because **MATK does not perform annotation - you configure it to start from your annotated VCFs.**


The "Annotation Binding" modules used in this software package attempts to utilize the VCF-addressable-space concept described above to bridge the gap between the non-standardized nature of the annotations in the incoming VCF and the manner in which they are to be used and interpreted in the task of variant ranking. Both the details of the incoming annotations and their desired application to variant ranking are highly institution- or researcher-specific and thus are the primary responsibility of a configurer of this toolkit - to specify the manner in which annotations should be used. The hope is that by factoring out these details into a compact module, the toolkit can supply a stable framework the relieves what would otherwise be a technically tedious task of data plumbing/management. This framework thus aims to change the task of scoring variants and assessing against pedigrees, which is currently manual, tedious and error prone into one that is applied efficiently, consistently and repeatably.

##### How a VCF file maps to a cache file

*Write something here about: one-row-per-gene-and-feature, NOT one-row-per-variant, or one-row-per-feature...   stay tuned*


##### Fields required in the Annotation Binding Module
All Annotation Bindings modules must define certain required attributes:
 - **ANN_tag**: what is tag name within the INFO field that contains the detailed annotations (the "list of lists" mentioned earlier). Usually "ANN". But sometimes "CSQ" or "VEP".
 - **cacheColumns**: enumerates fields in the incoming annotations that will be used to rank or describe variants and thus need to be put in the cache by **BuildOneCache.py**
 - **ScoreVariant()**: the locally defined scoring methodology, which should make use of any/all data from the cache, via the addressable space described in the earlier section.
 - **outputColumns**: enumerates columns in the CSV output generated by **AnalyzeCase.py**
 - **biotypes_of_interest**: a list of which BIOTYPES are to be considered.  All others are ignored.  List no doubt contains 'protein_coding' and perhaps also 'processed_transcripts'.  The **scoreVariant()** method should be able to deal with whatever BIOTYPES are included here.
 - **pop_Freq_columns**: a list of the columns in the cache that contain population frequencies.  These will only be used when -x is applied.
 -  **getSymbol()**: gives the location-path in the annotations for the field containing the HGNC Symbol.  Needed because SYMBOL is a required field in the output.
 -  **getGene()**: gives the location-path in the annotations for the Ensembl Gene ID.  Needed by **BuildOneCache.py** since gene is the primary collation of the cache file to be built.
 -  **DefaultScoreCutoff**: a Python dict giving the default scoring cutoff for each diease model.  User can override these defaults via -Z.
 -  **scoreVariant(LoF, LogMaxPlausibleAF, fields, debug)**: the critical variant scoring routine.  It receives the details of a given variant in 'fields' and must score the variant for either Loss- or Gain-of-Function depending on the first (boolean) argument, with the second argument providing the maximal plausible frequency (which comes from the Gene Model file).


#### Advanced Binding Methods   (under construction...)
 -  **updateFields(Fields)**
 -  **updateANNFields(ANN, SWinfo)**:
 -  **FinalAdjustments(variant_data)**:






