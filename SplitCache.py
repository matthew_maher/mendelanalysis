#!/usr/bin/env python3

import os
import sys
from time import gmtime

import MendelArgs as MA    ## Argument Parsing
import MendelFile as MF    ## File inhaling

### Configure, parse, validate our command line arguments
args = MA.ParseArgs('', 'CACHEin', 'Pedigree', 'OutDir')

if not os.path.exists(args.OutDir):
    sys.exit(f"The output target directory '{args.OutDir}' does not exist")
if not os.path.isdir(args.OutDir):
    sys.exit(f"The output target directory '{args.OutDir}' is not a directory")

###
###  Start by reading the headers off our cache to retrieve info we need to proceed
###
(cacheInput, CH) = MF.load_CacheHeaders(args.CACHEin)
samples = CH.Samples.split('\t')
print("%d samples seen in Cache file." % len(samples), file=sys.stderr)
AB = MF.getAnnotModule(CH.AnnotBinding)

if args.Pedigree != '-':

    Pedigree = MF.load_Pedigree(args.Pedigree)
    print("%d Families seen in Pedigree file." % len(Pedigree.keys()), file=sys.stderr)

    PedSamples = {s for Family in Pedigree for s in Pedigree[Family]['MEMBER'] }

    for psample in PedSamples:
        if psample not in samples:
            sys.exit(f"Sample '{psample}' from Pedigree not found in Cache")

    for sample in samples:
        if sample not in PedSamples:
            print(f"Sample '{sample}' from Cache not found in Pedigree - will be skipped")

else:
    ### Make a pedigree of probands only
    Pedigree = {}
    for sample in samples:
        Pedigree[sample] = {}
        Pedigree[sample]['MEMBER'] = [ sample ]

###
### First we need to initialize all the output files with headers
###
FamOut = {}
for Family in Pedigree:

    ### Open a separate file for each Family
    FamCache = args.OutDir + "/" + Family + ".cache"
    FamOut[Family] = open(FamCache, "w")

    ### Prepare slightly adjusted headers
    hdrs = MF.formatCacheHeaders(CH.SourceVCF, 
                                 CH.GeneModel,
                                 CH.AnnotBinding,
                                 CH.Phenotype,
                                 gmtime(),
                                 Pedigree[Family]['MEMBER'])

    ### And write them out
    for hdr in hdrs:
        print(hdr, file=FamOut[Family])

###
### Now move on to parceling out the input Cache to each output file as needed
###
print("Processing...", file=sys.stderr)
for cacheLine in cacheInput:
    row = cacheLine.strip().split(',')

    baselen = len(AB.cacheColumns)+1  ## +1 because the Gene ID implicitly column 1
    base = row[0:baselen]
    GTs  = row[baselen:baselen+len(samples)]
    GQs  = row[baselen+len(samples):]

    if len(samples) != len(GQs):      ## reality check
        print(f"{args.CACHEin}: Length mismatch {len(samples)} samples <-> {len(GQs)} GQs")
        sys.exit(1)

    GT = { s:t for (s,t) in zip(samples, GTs) }
    GQ = { s:q for (s,q) in zip(samples, GQs) }

    for Family in Pedigree:
        Fam_GTs = [ GT[sample] for sample in Pedigree[Family]['MEMBER'] ]
        Fam_GQs = [ GQ[sample] for sample in Pedigree[Family]['MEMBER'] ]
        if any('1' in g for g in Fam_GTs):
            newrow = base.copy()
            newrow.extend(Fam_GTs)
            newrow.extend(Fam_GQs)
            print(','.join(newrow), file=FamOut[Family])

###
### Close everything down in an orderly manner
###
for Family in Pedigree:
    FamOut[Family].close()

print("All Done.", file=sys.stderr)
