#!/usr/bin/env python3
import os
import re
import sys
import glob
import MendelFile as MF
import MendelArgs as MA    ## Arg parsing
from collections import defaultdict

ANNOT_SCORE = {
    'PROTEIN_CODING__LOF':         { 'LoF': 1.0, 'GoF': 0.5 },
    'PROTEIN_CODING__DUP_LOF':     { 'LoF': 1.0, 'GoF': 0.5 },
    'PROTEIN_CODING__COPY_GAIN':   { 'LoF': 0.5, 'GoF': 0.5 },
    'PROTEIN_CODING__DUP_PARTIAL': { 'LoF': 0.5, 'GoF': 0.5 },
    'PROTEIN_CODING__MSV_EXON_OVR':{ 'LoF': 0.8, 'GoF': 0.5 },
    'PROTEIN_CODING__INTRONIC':    { 'LoF': 0.5, 'GoF': 0.5 },
    'PROTEIN_CODING__INV_SPAN':    { 'LoF': 0.5, 'GoF': 0.5 },
    'PROTEIN_CODING__UTR':         { 'LoF': 0.5, 'GoF': 0.5 },
    'PROTEIN_CODING__PROMOTER':    { 'LoF': 0.5, 'GoF': 0.5 },
    'PROTEIN_CODING__NEAREST_TSS': { 'LoF': 0.5, 'GoF': 0.5 },
    'NO_INTERPRETATION':           { 'LoF': 0.8, 'GoF': 0.8 },
}

###
### Validate arguments
###
args = MA.ParseArgs('', 'GeneModel', 'Phenotype', 'GATKSVdir', 'gencodeGTF', 'Padding')

try:
    args.Padding = int(args.Padding)
except ValueError:
    print("Padding parameter (%s)  must be numeric" % args.Padding)
    sys.exit(1)

###
### Get our list of samples for which we will attempt to build a cache
###
infiles = [ os.path.basename(x) for x in glob.glob(args.GATKSVdir + "/*.vcf")]
print("%d input files found in '%s'" % (len(infiles), args.GATKSVdir), file=sys.stderr)

### inhale the gene model phenotype definitions
print("Reading in Phenotype-specific Gene Model File...", file=sys.stderr)
GeneModel = MF.Load_Gene_Model_File(args.GeneModel, args.Phenotype, True)
count_or_star = str(len(GeneModel)) if not 'R' in GeneModel['*'] else "'*'"
print("Gene Model Phenotype has %s genes" % count_or_star, file=sys.stderr)

###
### Process the gencode GTF looking for our genes of interest
###
unrecognized_symbols = set()
genes_by_chr = defaultdict(lambda: [])
print("Reading in GENCODE GTF (%s)..." % args.gencodeGTF, file=sys.stderr)
c = 0
lines = open(args.gencodeGTF, "r").read().splitlines()
GeneMap = {}
for line in lines:
    if line.startswith('#'):
        continue

    chrom, src, type, start, end, extra, strand, frame, rest = line.split('\t')
    if type != 'gene':
        continue
    chrom = chrom.replace('chr', '')
    restvals = re.split(r";\s*", rest)
    flds = {}
    for ival in restvals:
       if ival == '':
          continue
       m = re.match(r"^(.*?) (.*)$", ival)
       if m:
           key = m.group(1)
           val = m.group(2)
       else:
           print("could not parse key and val: %s" % ival, file=sys.stderr)
           sys.exit(1)
       val = val.replace('"', '')
       flds[key] = val

    ENS = flds['gene_id']
    SYM = flds['gene_name']
    ENS = re.sub(r"\.\d+$", '', ENS)
    if ENS not in GeneModel and not 'R' in GeneModel['*']:
        continue
    if ENS in GeneModel:
        del GeneModel[ENS]
    GeneMap[SYM] = ENS
    c += 1

    try:
        start = int(start)
    except ValueError:
        print("Start value not numeric in GTF file at %s: %s" % (ENS, start))
        sys.exit(1)
    try:
        end = int(end)
    except ValueError:
        print("End value not numeric in GTF file at %s: %s" % (ENS, end))
        sys.exit(1)
    genes_by_chr[chrom].append((start - args.Padding, end + args.Padding, ENS, SYM))


print("%d genes found in GTF file" % c, file=sys.stderr)
leftover = GeneModel.keys()
del GeneModel['*']
if len(leftover) > 0:
    print("%d Gene from Gene Model not found in GTF: %s" % (len(leftover), ', '.join(leftover)), file=sys.stderr)
    sys.exit(1)

    
print("Processing GATKSV input files...", file=sys.stderr)
for infile in infiles:
    print("Reading in %s..." % (args.GATKSVdir + '/' + infile), file=sys.stderr)
    samples = []
    lines = open(args.GATKSVdir + '/' + infile, "r").read().splitlines()
    for line in lines:
        if line.startswith('#CHROM'):
            (a, b, c, d, e, f, g, h, i, *samples) = line.split('\t')
            continue
        if line.startswith('#'):
            continue
        (chr, pos, oid, ref, alt, qual, filt, info, format, *calls) = line.split('\t')
        chr = chr.replace("chr", '')
        fields = format.split(':')

        ###
        ### Lets parse apart the INFO fields - colection gene-specific annotations along the way
        ###
        INFO = {}
        infovals = re.split(r";\s*", info)
        genelist = []
        for ival in infovals:
            if '=' not in ival:
                INFO[ival] = 1
            else:
                (key, val) = ival.split('=')

                ### Only Protein-Coding genes considered at present.
                ### Flip the below comment in order to include LINCRNA
               #if 'PROTEIN_CODING' in key or 'LINCRNA' in key:
                if 'PROTEIN_CODING' in key:
                    for gene in val.split(','):
                        genelist.append((key, gene))
                INFO[key] = val

        ###
        ### Some future generation will need to actually finish the work on these.
        ###
        if INFO['SVTYPE'] == 'BND' or INFO['SVTYPE'] == 'CPX':
            continue

        ###
        ### Determine a start - end position of the SV
        ### In case of BND, it's really a point, so set end=start
        ###
        start = pos
        if INFO['SVTYPE'] == 'BND':
            INFO['END'] = start
        if 'END' not in INFO:
            print("Could not get END position from INFO field in %s:\n%s\n" % (infile, line), file=sys.stderr)
            sys.exit(1)
        end = INFO['END']
        try:
            start = int(start)
        except ValueError:
            print("Start value not numeric in GATKSV file (%s): %s" % (infile, start))
            sys.exit(1)
        try:
            end = int(end)
        except ValueError:
            print("end value not numeric in GATKSV file (%s): %s" % (infile, end))
            sys.exit(1)

        ###
        ### Construct a compact ID for the event
        ###
        if INFO['SVTYPE'] == 'BND':
            id = "%s:%s:%s->%s" % (INFO['SVTYPE'], chr, pos, alt)
        else:
            id = "%s:%s:%s-%s"  % (INFO['SVTYPE'], chr, str(start), str(end))

        ###
        ### Hopefully, the SV is already annotated/interpreted to some gene(s)
        ### If not, we need to do it ourself, going through our gene model looking
        ### for any overlap with our padded genes.  If we find one, we will
        ### report it with a non-interpretation
        ###
        if len(genelist) == 0:
            for gene in genes_by_chr[chr]:
                (gene_start, gene_end, ENS, SYM) = gene
                if ((start >= gene_start and start <= gene_end) or
                    (end   >= gene_start and end   <= gene_end) or
                    (start <  gene_start and end   >  gene_end)):
                        genelist.append(( 'NO_INTERPRETATION', SYM ))

        ### And if still nothing... skip it - we can't associate it with any gene
        if len(genelist) == 0:
            continue

        ###
        ### Now process the individual calls
        ###
        for sample, call in zip(samples, calls):

            ### Most samples in the file are controls - skip them
            if sample.startswith('HG') or sample.startswith('NA'):
                continue

            ### Decode the per-sample call information
            callvals = { f:v for (f,v) in zip(fields, call.split(':')) }
        
            ### Even though this is a sampl eof interrest, the call may not pertain to it.  If not, skip it
            if INFO['SVTYPE'] == 'CNV':
                if callvals['CN'] in [ '2', '.' ]:
                    continue
            elif callvals['GT'].count('1') == 0:
                continue

          # for gene in genelist:
            for (Annot, SYM) in genelist:
                if SYM not in GeneMap:
                    if SYM not in unrecognized_symbols and 'R' in GeneModel['*']:
                        print("WARNING: Skipping Unrecognized Symbol: %s" % SYM, file=sys.stderr)
                    unrecognized_symbols.add(SYM)
                    continue
                ENS = GeneMap[SYM]
                if 'CN' not in callvals:
                    callvals['CN'] = ''
                if callvals['GQ'] == '.':
                    if INFO['SVTYPE'] == 'CNV':
                        callvals['GQ'] = callvals['CNQ']
                    else:
                        print("No GQ value for %s in file %s" % (id, infile), file=sys.stderr)
                print("%s,%s,%s,%d,%d,%s,%s,%0.3f,%0.3f,%s,%s,%s,%s,%s" % \
                  (ENS, SYM, chr, start, end, id, Annot, ANNOT_SCORE[Annot]['LoF'], ANNOT_SCORE[Annot]['GoF'], sample, \
                   callvals['GT'], callvals['GQ'], callvals['CN'],  callvals['EV'].replace(',',':')))

print("All Done.", file=sys.stderr)
