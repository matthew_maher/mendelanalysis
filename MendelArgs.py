#!/usr/bin/env python3

import os
import sys
import argparse
import importlib

###
###   Command-line parameter parsing for MendelAnalysis 
###
### The toolkit has numerous command line tools which share many
### command line parameters and switches.  So to ensure consistency in 
### naming, and to reduce repetition, all parsing and validation 
### is factored out to this file.
### 
### 'ParseArgs' is the main entry, which just needs to be supplied a summary 
###           string of which options/parameters are supported. 
### 
### 'switchString' is a convenience for programs that are wrappers that call
###           others, and which need to simply pass along supported options
###


### This model is essentially a singleton (we only have one command line)
### The only state here is two-fold:
###   - the known universe of command line options (extra modules can add to the list)
###   - the list of "extra" data source modules that have registered
###
class static_state:
    pass
state = static_state()


###
### What are all the valid positional parameters and switches?
###
state.valid_params = { 
    'GeneModel':          'Gene Model - Phenotype mappings file',
    'Phenotype':          'Phenotype (must be column in Gene Model File)',
    'AnnotBinding':       'Annotation Binding package',
    'VCFinDirOrFile':     'Directory containing (or one single) UNannotated VCF Files',
    'VCFoutDirOrFile':    'Directory (or file) to write ANNOTATED VCF output to',
    'VCFdir':             'Directory containing ANNOTATED VCF Files',
    'VCFin':              'VCF file to read',
    'VCFout':             'VCF file to write (with scores)',
    'CACHEout':           'Cache file to create',
    'CACHEin':            'Cache file to read from',
    'OutDir':             'Output Directory',
    'DiseaseModel':       'Disease Model: [R]ecessive Pairs, [D]ominant, [X]-Linked, Recessive [1]-hits ',
    'Pedigree':           '<Pedigree file> or \'-\' if using -P <proband>',
    'Family':             '<Family ID> in pedigree to analyze or \'-\' if using -P <proband>',
    'CohortDirOrCache':   'Cohort Directory or Single Cache File',
    'CohortDirOrVCF':     'Cohort Directory or Single VCF File',
    'CohortDir':          'Cohort Directory',
    'CohortManifest':     'Cohort Manifest File',
    'GATKSVdir':          'Directory of GATK-SV output files',
    'GCNVdir':            'Directory of GCNV output files',
    'GCNVsummary':        'GCNV summary output file',
    'gencodeGTF':         'gencode gene model file (GTF)',
    'Padding':            '# of bp of padding on each side of gene definition when determining overlap',
    'CNVfile':            'CNV file to have scored',
    'SVfile':             'SV file to have scored',
    'LoFGoF':             "Loss or Gain of Function? either 'LoF' or 'GoF'",
    'Chr':                'Chromosome to search for',
    'Pos':                'Genomic Position to Search for'
}

###
### What are all the valid binary switches?
###
state.valid_bin_switches = { 
    'm': 'for machine-friendly output (no meta lines)',
    'w': 'for wide CSV format for recessive pairs',
    's': 'to silence headers and summary information',
    'H': 'output only column headers and then exit',
    'N': 'No column headers displayed',
    'f': 'screen only the first-affected (Disease Model of \'1\' only',
    'r': 'Modify -R logic such that for recessive, LOWER SCORING variant must be in a range',
    'U': 'All solutions must contain at least one UTR variant',
}

###
### What are all the valid switches that take a value?
###
state.valid_val_switches = { 
    'I': '<sample ID> to build cache with - for use with single sample VCFs only',
    'P': '<Proband-Only ID> to select from multi sample VCF',
    'Z': '<score cutoff filter>',
    'g': '<gene symbol> to restrict to single gene',
    'x': '<Hard Pop Freq cutoff filter>',
    'i': '<In-cohort max freq>',
    'd': '<position> to score components (debugging)',
    'R': '<BED file> of ranges - all solutions must contain at least one variant in a range',
    'O': '<max # of output rows>',
    'o': '<max pct drop from top result>',
    'X': "Add an 'extra' call source (see documentation)",
    'A': "Add an extra per-gene annotation source",
}

### Some switches get the 'append' option, allowing for multiple usages
state.switches_append = [ 'A' ]

###
### Entry point for an "Extra" module to register
###
state.ExtraSourceList = []
state.ExtraSwitches = []
def ExtraSources():
    return state.ExtraSourceList

def RegisterExtra(extra):

    state.ExtraSourceList.append(extra)

    for switch, desc in extra.val_switches.items():
        if switch in state.valid_val_switches and state.valid_val_switches[switch] != desc:
            sys.exit(f"INTERNAL ERROR: Conflicting descriptions for cmd line switch '{extra}'")
        else:
            state.valid_val_switches[switch] = desc
            state.ExtraSwitches.append(switch)
    for switch, desc in extra.bin_switches.items():
        if switch in state.valid_bin_switches and state.valid_bin_switches[switch] != desc:
            sys.exit(f"INTERNAL ERROR: Conflicting descriptions for cmd line switch '{extra}'")
        else:
            state.valid_bin_switches[switch] = desc
            state.ExtraSwitches.append(switch)

###
### Deal with actual command line arguments
###
def ParseArgs(switches, *params):

    pre_parser = argparse.ArgumentParser(add_help=False)
    pre_parser.add_argument("-B", help="<Path to Module Location>")
    pre_parser.add_argument("-X", help="Add an 'extra' call source", action='append')
    pre_args, args = pre_parser.parse_known_args()
    if pre_args.X != None:
        for extra in pre_args.X:
            importlib.import_module(extra)

    ### Internal check to note obsolete or bad switch values
    for switch in switches:
        if switch not in state.valid_val_switches and switch not in state.valid_bin_switches:
            sys.exit(f"internal error - bad switch value: '{switch}'")

    ### Configure the available / allowable command line args
    parser = argparse.ArgumentParser()
    for param in params:
        parser.add_argument(param, help=state.valid_params[param])
    for switch in state.valid_val_switches:
        if switch in switches or switch in state.ExtraSwitches:
            if switch in state.switches_append:
                parser.add_argument("-" + switch, help=state.valid_val_switches[switch], action='append')
            else:
                parser.add_argument("-" + switch, help=state.valid_val_switches[switch])
    for switch in state.valid_bin_switches:
        if switch in switches or switch in state.ExtraSwitches:
            parser.add_argument("-" + switch,  action='store_true', help=state.valid_bin_switches[switch])

    ### parse what the user actually supplied
    args = parser.parse_args(args)

    ### Support convenience ENV settings
    if 'GeneModel' in args and args.GeneModel == '-':
        if 'MATK_GMF' not in os.environ:
            sys.exit("Env Var 'MATK_GMF' must be set when supplied GeneModelFile value is '-'")
        args.GeneModel = os.environ['MATK_GMF']

    if 'Phenotype' in args and args.Phenotype == '-':
        if 'MATK_PHEN' not in os.environ:
            sys.exit("Env Var 'MATK_PHEN' must be set when supplied Phenotype value is '-'")
        args.Phenotype = os.environ['MATK_PHEN']

    if 'AnnotBinding' in args and args.AnnotBinding == '-':
        if 'MATK_BINDING' not in os.environ:
            sys.exit("Env Var 'MATK_BINDING' must be set when supplied AnnotBinding value is '-'")
        args.AnnotBinding = os.environ['MATK_BINDING']

    ### And stick back in the switches that were caught on the pre_parse.
    ### They need to be reincluded because they may get passed down to another command
    args.X = pre_args.X

    ### miscellaneous validations of input parameters
    if 'O' in switches and args.O:
        args.O = int(args.O)

    if 'o' in switches and args.o:
        args.o = float(args.o)

    if 'i' in switches and args.i:
        args.i = float(args.i)
        if args.i <= 0 or args.i >= 1:
            sys.exit("-i value (in-cohort frequency Hard Filter) must be between 0 and 1")

    if 'x' in switches and args.x:
        args.x = float(args.x)
        if args.x <= 0 or args.x >= 1:
            sys.exit("-x value (Allele Frequency Hard Filter) must be between 0 and 1")

    if 'DiseaseModel' in params and args.DiseaseModel not in ['R', 'D', 'X', '1', 'M']:
        sys.exit("Disease Model must be one of: [R]ecessive Pairs, [D]ominant, [X]-Linked, Recessive [1]-hits, [M]itochondrial")

    if 'LoFGoF' in params and args.LoFGoF not in ['LoF', 'GoF']:
        sys.exit(f"LoFGoF parameter must have value of either 'LoF' or 'GoF'")

    if 'w' in switches and 'm' in switches and args.w and args.m:
        sys.exit("-w and -m are mutually exclusive")

    if 'r' in switches and 'R' in switches and args.r and not args.R:
        sys.exit("-r not valid without -R")

    if 'f' in switches and args.f and args.DiseaseModel != '1':
        sys.exit(f"-f ('first-only') only valid for DiseaseModel='1'")

    return args


###
### Build a literal string of the various command line switches in effect, for passing along...
### This is used by a couple helper/wrapper scripts (e.g. CreateCombinedAnalysis) who have many 
### possible options, most of which should be passed right along to AnalyzeCohort.py, etc.
###
def switchString(args, switches):
    switches_to_pass = []
    for switch in state.valid_val_switches:
        if switch in switches:
            val = getattr(args,switch)
            if val is not None:
                if not isinstance(val, list):
                    val = [ val ]
                for v in val:
                    switches_to_pass.append(f"-{switch} {str(v)}")
    for switch in state.valid_bin_switches:
        if switch in switches:
            if getattr(args,switch):
                switches_to_pass.append(f"-{switch}")
    return " ".join(switches_to_pass)


###
### Extra Data Sources that are to be included by default
### If not imported here, they can still be used by giving -X <name> (and -B path if needed)
### Try it: comment out the line below and do "AnalyzeCase.py -h" see what happens to -C and -t
### Then do "-h" again, but along with "-X MendelCNV".  -C and -t should return
###
import MendelCNV  as anything_you_want_it_will_register_itself
import MendelSV   as anything_you_want_it_will_register_itself
