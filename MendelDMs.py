#!/usr/bin/env python3

### This file contains the routines that do the testing for disease-model-specific segregation 
### 
### Each of the disease model routines below expects to get:
###     calls:  a dict of genotypes keyed:  [<variant>][<sample id> or <group>]
###                         where <variant> is '<chr>-<pos>-<ref>-<alt>'
###                               <group> could be 'SUMMARY' for 'all the affecteds'
### 
###     In each case, the job of routine is to return a list of possible solutions, with each
###     solution being a tuple of two <variant>s for recessive, or the second <variant> blank for other models
### 

import itertools
import MendelFile as MF
import sys

supported_disease_models = { 'R': "Recessive", 
                             'D': "Dominant",
                             'X': "X-Linked", 
                             '1': "1-Hit Recessive",
#                            'M': "Mitochondrial"
                            }

###
### Basic Routines to assess a 'genotype', which is a 2-tuple of 
### either '1' and anything else (usually '0').  
### Note that '.' ("no call") is treated as '0' ("ref call")
###

### Heterozygous
def ishom(GT):
    return (GT[0] == '1') and (GT[1] == '1')

### Homozygous
def ishet(GT):
    return (GT[0] == '1') !=  (GT[1] == '1')

### At Least Heterozygous
def isalh(GT):
    return (GT[0] == '1') or  (GT[1] == '1')

### How many alleles?
def dosage(GT):
    return GT.count('1')

### How many alleles?
def max_dosage(GT):
    return GT.count('1') + GT.count('.')

###
### We will use this routine to calculate summary facts about calls on each variant.
### This hopefully makes it easier to code/reason about the variants in the disease-model-
### specific routines to follow.  Each summary fact is a three part fact:
###   'ALL' or 'ANY'
###   'AFF', 'UNA', '1ST'  for Affected, Unaffected or 1st Affecteda(Proband) 
###   'HET', 'HOM' or 'ALH' (at least het)
###
### HOPEFULLY, these precomputed values make it easier and clearer to follow the routines below
###
def summarize_genotypes(GTs, affecteds, unaffecteds, FirstOnly, Proband):
    return {
        'ALL_AFF_HET': all(ishet(GTs[s]) for s in affecteds),
        'ALL_AFF_HOM': all(ishom(GTs[s]) for s in affecteds),
        'ALL_AFF_ALH': all(isalh(GTs[s]) for s in affecteds),
        'ANY_AFF_ALH': any(isalh(GTs[s]) for s in affecteds),
        'ANY_UNA_ALH': any(isalh(GTs[s]) for s in unaffecteds),
        'ANY_UNA_HOM': any(ishom(GTs[s]) for s in unaffecteds),
        '1ST_AFF_HET':     ishet(GTs[Proband]) if FirstOnly else False,
    }


def find_aff_unaff(Pedigree, Family):
    Proband = Pedigree[Family]['PROBAND']
    affecteds   = [s for s in Pedigree[Family]['MEMBER'] if Pedigree[Family]['MEMBER'][s]['Status'] == 'affected'  ]
    unaffecteds = [s for s in Pedigree[Family]['MEMBER'] if Pedigree[Family]['MEMBER'][s]['Status'] == 'unaffected']
    return (Proband, affecteds, unaffecteds)


def find_candidates(DiseaseModel, GTs, Pedigree, Family, FirstOnly):

    ### These analyses are all limited to the specified family only

    ### UNRESOLVED: how to handle the OTHER families in the pedigree or cache?
    ### Assuming they're in both, then we know their status and the DiseaseModel
    ### code COULD filter against them as well.  Need to discuss...
    ### A person could be in cache (so we have calls) but not in the pedigree (so status unknown).
    ### i.e. In AnalyzeCase, we only validated the family-of-interest to see if all members in cache

    (Proband, affecteds, unaffecteds) = find_aff_unaff(Pedigree, Family)
    for var in GTs:
        GTs[var]['SUMMARY'] =  summarize_genotypes(GTs[var], affecteds, unaffecteds, FirstOnly, Proband)

    if DiseaseModel not in supported_disease_models:
        sys.exit(f"Unsupported disease model: {DiseaseModel}")

    if DiseaseModel == 'D':
        return find_dominants(GTs)
    if DiseaseModel == 'R':
        return find_recessive(GTs, Pedigree, Family, affecteds, unaffecteds)
    if DiseaseModel == 'X':
        return find_X_linked(GTs, Pedigree, Family)
    if DiseaseModel == '1':
        return find_one_hits(GTs, FirstOnly)
    if DiseaseModel == 'M':
        return find_mitochondrial(GTs)

###
###
###  Below here are the per-disease model subroutines that test for those specific segregation patterns
###
###

def find_one_hits(GTs, FirstOnly):

    ## depending on -f we'll get calls that are HET for all affected or just the first affected
    field_to_check = '1ST_AFF_HET' if FirstOnly else 'ALL_AFF_HET'
    ONEHIT_calls = [x for x in GTs if GTs[x]['SUMMARY'][field_to_check] ]

    for var in ONEHIT_calls:
        yield (var,)

########################################################################################

def find_X_linked(GTs, Pedigree, Family):

    ALL_AFF_ALH_calls = { x for x in GTs if GTs[x]['SUMMARY']['ALL_AFF_ALH'] }
    
    for var in ALL_AFF_ALH_calls:

        ##
        ## We need to ensure that none of the unaffected Males have this variant
        ## What if we don't know the gender?
        ##
        unaff_male_ALH_X = any(isalh(GTs[var][s]) \
                                for s in Pedigree[Family]['MEMBER'] \
                                    if   Pedigree[Family]['MEMBER'][s]['Status'] == 'unaffected' \
                                    and  Pedigree[Family]['MEMBER'][s]['Gender'] == 'male')

        if unaff_male_ALH_X:
            continue

        yield (var,)

########################################################################################

def find_dominants(GTs):

    # When assuming dominant, we're going to bypass Autosomal HOM calls - too many and too unlikely
    # We just want variants with uniform HET call for the affecteds and no evidence to the contrary
    ALL_AFF_HET_calls = { x for x in GTs if GTs[x]['SUMMARY']['ALL_AFF_HET'] }
    ANY_UNA_ALH_calls = { x for x in GTs if GTs[x]['SUMMARY']['ANY_UNA_ALH'] }
    
    for var in (ALL_AFF_HET_calls - ANY_UNA_ALH_calls):
        yield (var,)


########################################################################################

def find_mitochondrial(GTs):

    sys.exit("Mitochondrial Disease not yet implemented")

### Return the total allele count for a sample for a list of variants
def Allele_Count(GTs, s, varlist):
    return sum(dosage(GTs[var][s]) for var in varlist)


### return the subset of a variant list that a sample actually has
def calls_for_sample(GTs, s, varlist):
    return { var for var in varlist if max_dosage(GTs[var][s]) > 0 }


### Can we determine that these two variants are in cis?
### Returning False does not necessarily mean in-trans (just unable to prove cis)
def check_cis(sample, varlist, GTs, Father, Mother):

    ### We can't tell anything if we have no parents
    if Father == '' and Mother == '':
        print("Internal Error - both parents blank")
        sys.exit(1)

    sample_calls = calls_for_sample(GTs, sample, set(varlist))

    ### If we only have the Mother, she must have at least one allele
    if Father == '':
        return len(calls_for_sample(GTs, Mother, sample_calls)) == 0

    ### If we only have the Father, he must have at least one allele
    if Mother == '':
        return len(calls_for_sample(GTs, Father, sample_calls)) == 0

    ### We have both parents, so let's take a careful look
    mother_calls = calls_for_sample(GTs, Mother, sample_calls)
    father_calls = calls_for_sample(GTs, Father, sample_calls)

    return is_cis(sample_calls, mother_calls, father_calls)

###
### We have trio, and want to check of the calls in the child
### appear to be in cis (based on the parents)
### The input sample_calls is a set of either one variant (HOM) or two (HET)
### And the mother and father inputs are the relevant SUBsets for those
###
def is_cis(sample_calls, mother_calls, father_calls):

    ### Do we appear to have a Mendelian Violation?
    if (mother_calls | father_calls) != sample_calls:
       #### Until we discuss some other strategy, we rule out the solution as unlikely
        return True

    ### If we have both parents, they each must have at least one allele
    return len(mother_calls) == 0 or len(father_calls) == 0

########################################################################################
def find_recessive(GTs, Pedigree, Family, affecteds, unaffecteds):

    ### Let precompute the relationships that we need to check for cis/trans so that
    ### we don't have to keep determining this fresh each time in the loop below
    FamPed = Pedigree[Family]['MEMBER']
    parents_to_check = [ (a, FamPed[a]['Father'], FamPed[a]['Mother']) 
                          for a in affecteds \
                          if FamPed[a]['Father'] != '' or FamPed[a]['Mother'] != '' ]

    ### Our working group is any variant in which any affected is at least HET
    ANY_AFF_ALH_calls = { x for x in GTs if GTs[x]['SUMMARY']['ANY_AFF_ALH'] }

    ### In a typical recessive family, where all affecteds are sibs, we are looking for 
    ### at most two variants (comp-het).  BUT, if we also have an affected parent, then 
    ### we also know how to look for a 'pseudominant' solution that can take up to 3
    max_allele_count = 3 if MF.pseudoDominant(Pedigree[Family]) else 2

    ### A valid solution is set of variants, set size from 1 to the max (above)
    for varcount in range(1, max_allele_count + 1):

        ### for each possible set size, let's check all combinations of avail variants
        for varlist in itertools.combinations(sorted(ANY_AFF_ALH_calls), varcount):

            ### Basic criteria: 
            ###   All the affecteds must have exactly two alleles
            ###   All the UNaffecteds must have less than two alleles
            if any(Allele_Count(GTs, s, varlist) != 2 for s in affecteds):
                continue
            if any(Allele_Count(GTs, s, varlist) >= 2 for s in unaffecteds):
                continue

            ### We need to check: IF we can establish the solving variants are in cis
            ### for any of the affecteds, then it's not a valid solution
#           if any(in_cis(s, varlist, GTs, Pedigree, Family) for s in affecteds):
#               continue
            if any(check_cis(a, varlist, GTs, Father, Mother) for (a, Father, Mother) in parents_to_check):
                continue

            yield tuple(varlist)
