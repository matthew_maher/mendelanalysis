#!/usr/bin/env python3

import re
import sys

####
####  The output columns are produced in several left-to-right sections.
####
####  This code is structured so as to make sure the headers stay aligned with the content
####
####  Section A is only produced in 'machine-friendly' mode (-m)
####  in NON-machine mode, section A is NOT produced and section B gets blanks on any second
####              row of a recessive display (because the data are unchanged from row above)
####
####  Section B provides: Score, Gene, Chr - these will NOT be repeated on 2nd recessive variant
####
####  Section C provides: POS, REF, ALT - these WILL be displayed for 2rd hit (likely different)
####
####  Section D contains the columns defined in the Bindings. So in wide more, it is shown twice.
####
####  After each section D, there is a HOM/HEMI indicator column, denoted 'HH' below
####
####  In 'wide mode' (-w) - for recessive only - both variants are displayed on the same row.
####
####  In non-wide mode the sections are:
####   [A]  B   C  HH  D             Note that each recessive solution takes two rows
####
####  In wide mode (which is only relevant to Recessive) the sections are:
####   [A] [B]  C  HH  D  C  HH  D
####

###
### Left most is Section A  - only displayed in -m mode
###
def sectionA(headers, args, ABName, rank):
    if headers:
        return 'SampFam', 'Binding', 'Model', 'Rank'
    else:
        return args.Family, ABName, args.DiseaseModel, str(rank)

###
### Section B
###
def sectionB(headers, score, datavar):
    if headers:
        return 'Score', 'Gene', 'Chr'
    else:
        return score, datavar['SYMBOL'], datavar['CHROM']

###
### Section C
###
def sectionC(headers, datavar):
    if headers:
        return 'POS', 'REF', 'ALT', 'GQ'
    else:
        return str(datavar['POS']), datavar['REF'], datavar['ALT'], datavar['GQ']

###
### Section D is completely controlled by definitions in the active Bindings module
###
def sectionD(headers, datavar, AB):
    if headers:
        ## reduce the column names to just the 'base' (e.g. INFO.ANN.XYZ --> XYZ)
        return [ col.split('.')[-1]                           for col in AB.outputColumns ]
    else:
        return [ str(datavar[col]) if col in datavar else ''  for col in AB.outputColumns ]

###
###   Routine to print Case Analysis headers
### 
def headers(args, AB):

    outvals = []
    if args.m:
        outvals.extend(sectionA(True, None, None, None))

    outvals.extend(sectionB(True, None, None))
    outvals.extend(sectionC(True, None))
    outvals.append("HH")
    outvals.extend(sectionD(True, None, AB))

    ### The headers could be one or two sets of columns if user wants the 'wide' recessive format
    if (args.DiseaseModel == 'R' and args.w):
        outvals.extend(sectionC(True, None))
        outvals.append("HH")
        outvals.extend(sectionD(True, None, AB))

    return outvals

###
### The 'HOM' column of the output supports a special possible value when
### A call has been deemed to be HEMIzygous. If so, we will indicate this
### and possibly (if not proband-only) list the samples that were deemed HEMI.
### The values that drive this were determined by a 'extra' source (CNVs)
### when it reviewed it's calls against the SNV calls.
###
def HEMI_label(HETHOM, vardata, Proband):

    ### 99% of the time, we just return the default value
    if 'HEMI' not in vardata:
        return  HETHOM

    ### This may need revisiting...
#   if Proband != None:
    if HETHOM != '':
        return 'HEMI:' + HETHOM
    else:
        return 'HEMI'

###
###   Main output of a Case Analysis
### 
def final_output(rows_output, AB, args, Proband):

    if not args.s or args.H:
        yield headers(args, AB)

    ordered_rows_output = sorted(rows_output, key=lambda tup: tup[0], reverse=True)
    rank = 0
    for row in ordered_rows_output:
        (sortkey, combined_score, scores, data, varlist ) = row
        rank += 1 

        callinfo = [ data[v]['AFF_ALLS'] for v in varlist ]
        if len(varlist) == 1:
                callinfo[0] = '' if args.DiseaseModel != 'R' else 'HOM'
        if len(varlist) == 2:
            if  callinfo[0] == callinfo[1]:
                callinfo[0] = ''
                callinfo[1] = ''

        outvals = []
        if args.m:
            outvals.extend(sectionA(False, args, AB.Name, rank))

        score = "%4.2f" % combined_score
        if len(varlist) > 1:
            score = score + "=" + '+'.join(["%4.2f" % s for s in scores])
            score = score.replace('+-','-')

        var1 = varlist[0]
        outvals.extend(sectionB(False, score, data[var1]))
        outvals.extend(sectionC(False, data[var1]))
        HETHOM1 = HEMI_label(callinfo[0], data[var1], Proband)
        outvals.append(HETHOM1)
        outvals.extend(sectionD(False, data[var1], AB))

        for i in range(1, len(varlist)):
            vari = varlist[i]

            ### Unless we're in wide mode, we need to do carriage return back to the left side
            if not args.w:
                yield outvals
                outvals = []

                ### produce the output for var2 to match the structure of var1 above
                if args.m:
                    outvals = []
                    outvals.extend(sectionA(False, args, AB.Name, rank))
                    outvals.extend(sectionB(False, score, data[vari]))
                else:
                    outvals = ['','','' ]  ## skip 'A' and supply a blank B

            outvals.extend(sectionC(False, data[vari]))
            HETHOMI = HEMI_label(callinfo[i], data[vari], Proband)
            outvals.append(HETHOMI)
            outvals.extend(sectionD(False, data[vari], AB))

            ### The output is complete - let's just do some reality checking to make sure things look okay 
            ### This is because we don't necessarily trust our data sources to keep symbols and chrs consistent

            ### Integrity check - this could happen if SNV data and an extra call source do not use consistent symbols
            var1_symbol = data[var1]['SYMBOL']
            vari_symbol = data[vari]['SYMBOL']
            if var1_symbol != vari_symbol:
                sys.exit(f"Internal Error: SYMBOL mismatch between {var1}({var1_symbol}) and {vari}({vari_symbol})")

            ### Integrity check - this really shouldn't happen - data sources disagree about what chromosome a gene is on?
            var1_chr = data[var1]['CHROM']
            vari_chr = data[vari]['CHROM']
            if var1_chr != vari_chr:
                sys.exit(f"Internal Error: CHR mismatch between {var1}({var1_chr}) and {vari}({vari_chr})")

        ### ship it
        yield outvals

        ### In 'human-friendly mode' we put extra blank line after each pair for readability
        if args.DiseaseModel == 'R' and not args.w and not args.m:
            yield ['']

