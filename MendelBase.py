#!/usr/bin/env python3

import os
import sys
import math

###
### Based on a gene and disease model, are we scoring for LoF (True) or GoF (False) ?
###
### This is a spot where special domain/phenotype knowledge can come into play:
###  The HSDOM_genes list is specified in the AnnotBinding module
###
def LoF(Gene, Model, HI_flag):

    ## test for special knowledge: is this gene known to be LoF/Dom?
    if Model == 'D' and HI_flag:
           return True

    ### End of special knowledge rules: below this are the standard assumptions

    if Model in [ 'R', '1', 'X', 'M' ]:
       return True

    if Model == 'D':
        return False

    sys.exit("INTERNAL ERROR: Bad Model in LoF")


###
### Given an Allele Frequency for a variant, score it against the maximum
### plausible frequency for the gene/disease model
###
popF_limit = .000001     ## Don't give 'rarity' points for anything below this level
def scorePopFreq(variantAF, LogMaxPlausibleAF, indel):

    ###
    ### Should we assume that missing PopFreq annotation means freq = 0?  (maybe diff for indels?)
    ###
    if variantAF == '':
        variantAF = None
#    if variantAF == None and not indel:
#        variantAF = popF_limit
    if isinstance(variantAF, list) and len(variantAF) == 1:
        variantAF = variantAF[0]

    ###
    ### The given score is scaled based on the maximum plausible allele frequency
    ### This formula gives 2 points for every decimal place below the max plausible
    ###
    if variantAF == None:
        return 3            ## Arbitrary.  In a perfect world, we'll refer to the GnomAD coverage files
    else:
        return 2*(LogMaxPlausibleAF-math.log10(max(popF_limit, float(variantAF))))


###
###  Score a Variant
###    1.  Figure out what entry in gene model to use (for freq, etc)
###    2.  Figure out are we scoring for LoF or GoF
###    3.  Call the local-bindings routine to do the base scoring
###    4.  apply 'candidate' and 'NULL' scoring adjustments that MendelAnalysis built-ins
###
def scoreVariant(fields, GeneModel, gene, DiseaseModel, AB, debug):

    if gene in GeneModel:
       HIDomFlag = GeneModel[gene]['HI-Dom']
    else:
       HIDomFlag = False
    LoFflag = LoF(gene, DiseaseModel, HIDomFlag)

    ### use actual or 'other' gene model info
    GeneModelRow = GeneModel[gene] if gene in GeneModel else GeneModel['*']

    ### if this gene doesn't support this model, there's nothing to do
    if DiseaseModel not in GeneModelRow:
        return 0

    score = AB.scoreVariant(LoFflag, GeneModelRow[DiseaseModel], fields, debug)

    ### is there any adjustment for 'candidate' genes?
    if 'Confidence' in GeneModelRow:
        score *= GeneModelRow['Confidence']

    return score

###
###  Preprocess the incoming Consequence/Severity table into separate dicts 
###  storing reverse lookups.
###  Encoding the consequences will make the caches much smaller and speed processing
###  Encoding the Impact Ranks allows for numerical calculations of severity 
###
###  The two dicts populated here are used in the maxImpact routine below
###
Consequence_Code = {}
Impact_Rank = {}
def InitializeConsequenceTable(Consequences, Impact_ORDER):

    for cons_code in range(len(Consequences)):
        Consequence_Code[Consequences[cons_code][0]] = cons_code

    for impact_code in range(len(Impact_ORDER)):
        Impact_Rank[Impact_ORDER[impact_code]]=impact_code

    return (Consequence_Code, Impact_Rank)


def maxImpact(Consequences, cons_codes, LoF):
    impactRanks = [ Impact_Rank[Consequences[int(x)][1 if LoF else 2]] for x in cons_codes ]
    return max(impactRanks)
