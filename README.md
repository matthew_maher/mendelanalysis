# The Mendelian Analysis Toolkit ("MATK")

## Overview/Purpose

The software in this repository is a toolkit developed at Massachusetts Eye and Ear ("MEE") for the purpose of automating much of the effort involved in the task of analyzing/diagnosing cases of fully-penetrant Mendelian disease.  This task is often done by human analysts in what can be a time-consuming, error-prone, inconsistent and non-reproducible manner.  This toolkit hopes to ease those problems by providing a configurable framework for the automation of variant scoring (according to rules *that you define*) and solution ranking from source data originating in *your own arbitrarily annotated VCF files*.  However:

1. MATK does **not** purport to provide confirmed diagnostics - the expert human curator is still needed to review and validate all results.  But hopefully, that human's productivity will be much higher, their results will be more consistent and error-free, and MATK artifacts can provide a basis for future automated determination of when a case reassessment is needed, due to new/changed information. 
2. MATK is **not** a ready-to-go-out-of-the-box toolkit.  Various sets of data/information must be prepared and configured before use, including:
   - Phenotype-specific information - which genes and disease models are implicated in the phenotype-of-interest?
   - Annotation information - under what tags in the VCF input are the annotations-of-interest located?
   - Rules for weighing Evidence of Pathogenicity, derived from the annotations in an input VCF file.

Although you would need to build and configure the above items for your specific phenotype space and your VCF annotations, versions of these in use at MEE are included in this repository to serve as a demonstration and guide. 

While the original use-case phenotype at MEE was Inherited Retinal Degenerations, the software is structured in a manner intended to allow its re-use in any phenotype domain for Mendelian disease - the software expects to be supplied with domain-specific knowledge (the items noted above), hopefully able to be maintained by the phenotype experts and requiring minimal additional software development.

-----------------

## Prerequisites

This software is all Python3 - most currently tested on 3.7.6, but probably anything 3.6 minimum is likely to work.

This software was developed on a Linux platform and to date, only tested there.

This software uses the 3rd-party module [**vcfpy**](https://pypi.org/project/vcfpy/) for parsing VCF files.  You must have it installed and available on your PYTHONPATH.  **TODO (?):  get 'vcfpy' out of BuildCache.py to eliminate this pre-req for most users**

The toolkit is comprised of a set of command-line tools, many of which call others.  As such, you will need to have the location of the top-level of the directory tree included in your execution "PATH" variable, as well as your PYTHONPATH for locating modules.  i.e.:

```
export PATH=$PATH:<your check-out location>
export PYTHONPATH=<your check-out location>
```

As this toolkit is all directly executable Python3 code, there is no build step required.   But there is that configuration effort mentioned earlier...

----------------

## Three Roles, not Two

Most software tools have two roles: the user and the supporting developer. But because this software tool**kit** requires a phase of configuration and set-up before it can be used for a given phenotype and VCF-annotation set, it is important to distinguish *three* different roles in which a person may interact with this toolkit:

- **User Role:** primary use of the toolkit for scoring and ranking variants - *IF the necessary phenotype and annotation source-specific configurations are complete and available*.
- **Configurer Role**: constructing the necessary phenotype and annotation source-specific configurations and software bindings for a particular combination of phenotype and annotations.
- **Developer Role**: support for the toolkit's underlying software, independent of any specific phenotype or annotation set.

Each of these roles will be discussed in turn below.

-------------------------------------
## But First, The Big Picture

A major hurdle to a generalized solution for performing Mendelian Analysis from annotated VCF files is that different labs use different annotation tools or often *combinations* of such tools, and many may use their own private (e.g. tissue-specific data) annotation datasets.  The exact field names and data formats that a given lab or site uses for annotations are thus often highly bespoke. Further, different labs may have different views on how to assess the evidence present in the annotations.  MATK's goal is to be architected in a way to allow users to configure a software "Binding" that captures the precise details of the annotations in your VCFs and the rules by which you want them used in the task of variant assessment.  MATK then provides a simple two-step process (roughly diagramed below) that allows users to go from their annotatated VCF to a thorough ranking of potentially pathogenic variants and case solutions. The first step preprocesses the annotated VCF input, extracting all relevant data, and saving in a condensed 'cache' file.  The second step analyzes that cache to find candidate solutions for a specific inheritance mode.  That separate preprocessing steps allows for the optimization of the second step, as it may well be executed repeated with varying parameters.  The second-step analysis can also take into account complex pedigrees and orthogonal call sources such as Copy-Number Variations (CNVs).

A key point to take from this diagram is that MATK does NOT perform annotation of the VCF input files.  This is unlike some other scoring/ranking tools which aim to provide a convenient end-to-end (annotation-to-ranking) service.  For many users, that approach may suffice, but it is essentially a one-size-fits-all solution for users who seek ready results which they hope are 'good enough'.  MATK, by contrast, is intended for users you seek to have *complete and detailed control* of the scoring and ranking methodology being performed - this is why the extra 'configuration' step exists.

```mermaid
graph TD;
  Start[ ] == Unannoted VCF ==> YAT[YOUR Annotation Tool]
  YAT == Annotated VCF ==> BOC[BuildOneCache.py]
  BOC == Cache ==> AC[AnalyzeCase.py]
  AC == Analysis ==> End[ ]
  B((YOUR Bindings)) --> BOC
  B --> AC

  style AC fill:#9ff
  style BOC fill:#9ff
  style YAT fill:#eeac99, stroke-width:2px,stroke-dasharray: 5, 5
  style B fill:#eeac99, stroke-width:2px
```
-------------------------------------

## A brief demo

With the code-base checked out, take a look at: **demo/proband.vcf**.  This is a **made-up** sample VCF file that has already been annotated with MEE's internal annotation process (which is based on VEP and makes use of HGMD and ClinVar).   Now try this command:

```
BuildandCombinedAnalysis.py GEDI.latest.csv GEDIR MBC1_v1 demo/proband.vcf - > demo/proband.csv
```

This demonstration command is actually performing both of the two steps show in blue in the diagram above, and is making use of MEE-specific configurations that are included in this repository:

- **GEDI.latest.csv** contains information about various genes related to Ocular diseases.
- **GEDIR** is a column in the GEDI.latest.csv  file
- **MBC1_v1** refers to a software "Binding" module that provides mappings specific to the MEE annotations in that input VCF file

If you take a look at the output file created (it is generally intended for viewing in Excel) - it is a dense spreadsheet of ranked solutions and variants from the input VCF, as pertains to Retinal Degenerative Disease.  Very briefly, the "Model" column indicates that possible candidate solutions/variants are being suggested in (**R**)ecessive genes (note that the rows come in pairs in this section - there are two with Rank=1, two with Rank=2, etc.),  (**D**)ominant genes and also noted are prominent "(**1**)-hits" - variants in recessive genes, but unpaired.   Notable at the very top of this **made-up example** is that there are a pair of variants - one a frameshift, one a splice-site SNP - in the recessive-associated gene PDE6A.  In a real case, this would be the obvious place to start as those two putative Null variants would be overwhelmingly likely to explain the patient's disease. 

The key point to appreciate about this demo is that nearly everything about the output - which genes were selected in which disease models, what scores were assigned to each variant, even what columns comprise the output - are all part of the pre-built MEE configurations which were referenced by the parameters **GEDI.latest.csv**, **GEDIR** and **MBC1_v1**.  So...

###	Next Step - pause to configure

If you happen to be investigating Mendelian Ocular Diseases and you have VCFs annotated in a manner *exactly* like MEE's internal annotation process, well then this is your lucky day - no additional work is required and you're ready to go!  But no doubt, you have your own annotation process, with perhaps custom fields; your own phenotype-specific gene-set of interest; and your own views of how different pieces of evidence stack up for or against pathogenicity.  If so, then you, or someone else with the requisite domain knowledge (genes, inheritance models, annotations), will need to first fulfill the Configuration Role (below) before returning to the User Role.


## The User Role
IF the configuration step has been completed, then for more detailed user-role instructions, please see [**The User Guide**](USER.md)




## The Configurer Role

For details on performing MATK configuration, please see [**The Configuration Guide**](CONFIG.md)


## The Developer Role

For details relevant to Development, please see [**The Developer Notes**](DEVELOP.md)

