#!/usr/bin/env python3

import re
import os
import sys
import math
import importlib
import importlib.util
from time import strftime
from collections import defaultdict

###################  Gene Model File   ##########################################

###
###  Process the user-supplied gene file
###
def Load_Gene_Model_File(GMfile, Phenotype, silent):

    ### The user can specify a full path to gene file or reference one in the GeneSets directory
    if not os.path.isfile(GMfile) and '/' not in GMfile:
        GMfile = os.path.dirname(__file__) + "/GeneSets/" + GMfile

    GMcontent = open(GMfile, "r").read().splitlines()

    return Process_Gene_Model_Content(GMcontent, Phenotype, silent)


###
###  Process the user-supplied gene file
###
def Process_Gene_Model_Content(GMcontent, Phenotype, silent):

    if len(GMcontent) <= 1:
        sys.exit("Empty Gene Model File")

    ### The data structure that we will be build and return
    Gene_Model = defaultdict(lambda: {})   # keyed:  [ <gene> ] [ <dis model> ] = max plausible pop freq

    Gene_file_columns = GMcontent[0].split(',')
    required_columns = [ 'Ensembl', 'Disease Models', 'AR Pop Freq', 'AD Pop Freq', 'XL Pop Freq', 'Symbol' ]
    required_columns.append(Phenotype)
    for required_column in required_columns:
        if required_column not in Gene_file_columns:
            sys.exit(f"The Gene file must have a column named '{required_column}'")

    ### Process each gene in turn
    for line in GMcontent[1:]:

        ### First, unpack the row into a dict with named fields so we more easily deal with it
        gene_values = line.split(',')
        fields = { f:v for (f,v) in zip(Gene_file_columns, gene_values) }

        ## XL column SHOULD be required, but for now, we'll steal from AR
        if 'XL Pop Freq' not in fields:
            fields['XL Pop Freq'] = fields['AR Pop Freq']

        ENSid = fields['Ensembl']
        Symbol = fields['Symbol']

        ### If this is the default row, save it and move on
        if ENSid == 'defaults':
            defaults = fields.copy()
            continue

        ###
        ### Ignore all genes except for the desired phenotype (if any specified)
        ###
        if fields[Phenotype] == None or fields[Phenotype] == '':
            continue

        ### Symbol names are used to support -g on AnalyzeCase.py and leading field in output
        Gene_Model[ENSid]['Symbol'] = fields['Symbol']

        ### The optional 'Condfidence' column provides abilty to indicate candidate status
        Gene_Model[ENSid]['Confidence'] = float(fields['Confidence']) if ('Confidence' in fields and fields['Confidence'] != '') else 1.0  

        ### The optional 'HI-Dom' column provides abilty to indicate a gene is Happloinsufficient Dominant
        if 'HI-Dom' in fields:
            if fields['HI-Dom'].lower() in ('y', 'yes', '1'):
                Gene_Model[ENSid]['HI-Dom'] = True
            elif fields['HI-Dom'].lower() in ('n', 'no', '0', '' ):
                Gene_Model[ENSid]['HI-Dom'] = False
            else:
                sys.exit(f"Invalid value (\'{fields['HI-Dom']}\') in HI-Dom column for {ENSid}.  Use 'Yes' or 'No'")
        else:
            Gene_Model[ENSid]['HI-Dom'] = False

        ## Turn supported disease models into a list
        ## Look for just 'XL' prefix, as XL-Dom and XL-Rec await possible implementation
        DMs = [ 'XL' if x[:2] == 'XL' else x for x in fields['Disease Models'].split(';') ]
        DiseaseModelMap = { 'AR': 'R',
                            'AD': 'D',
                            'XL': 'X',
                            'MT': 'M'
                          }

        for DM in DMs:
            ### validate the disease model column contents
           #if (DM not in [ 'AD', 'AR', 'XL', 'MT' ]):
            if (DM not in DiseaseModelMap):
                if not silent and DM != '':
                    print(f"Ignoring unrecognized disease model: '{DM}' on gene {ENSid} ({Symbol})", file=sys.stderr)
                continue

            ### Every DM specified better have a valid freq value or a default must be available
            PFcolumn = DM + ' Pop Freq'
            if PFcolumn in fields:
                PFval = fields[PFcolumn] if fields[PFcolumn] != '' else defaults[PFcolumn]
            else:
                PFval = 1

            try:
                PopFreq = math.log10(float(PFval))
            except ValueError:
                sys.exit(f"Invalid Pop Freq value ('{fields[PFcolumn]}') for {ENSid} in column '{PFcolumn}'")

            Gene_Model[ENSid][DiseaseModelMap[DM]] = PopFreq
            if DM == 'AR':
                Gene_Model[ENSid]['1'] = PopFreq

    return Gene_Model

###
###  Validate a gene symbol against a Model as loaded above, and translate to gene ID
###
def check_and_translate_gene(symbol, GeneModel, GeneModelFile):
    genes = [gene for (gene, modelrow) in GeneModel.items() if modelrow['Symbol'] == symbol ]
    if len(genes) == 0:
        sys.exit(f"Gene symbol {symbol} not in Gene Model '{GeneModelFile}'")
    if len(genes) >  1:
        sys.exit(f"Gene symbol {symbol} appears {len(genes)} times Gene Model '{GeneModelFile}'!")
    return genes[0]  ## return the one and only


###################  Ranges File       ##########################################

###
### Load up any Genomic Ranges supplied in a file, which we'll use to filter results
###
def load_RangesFile(RangesFile):
    Ranges = defaultdict(lambda: [])
    with open(RangesFile, "r") as rf:
        for line in rf:
            (rangechr, rangestart, rangestop) = re.split(r'\t', line)
            try:
                rangestart = int(rangestart)
            except ValueError:
                sys.exit(f"Start value not numeric in Ranges file ({RangesFile}): {line}")
            try:
                rangestop = int(rangestop)
            except ValueError:
                sys.exit(f"Stop value not numeric in Ranges file ({RangesFile}): {line}")

            if rangestop < rangestart:
                sys.exit(f"Stop value less than Start in Ranges file ({RangesFile}): {line}")

            Ranges[rangechr].append((rangestart, rangestop))

    return Ranges

###
### Test to see if a given variant falls within list of supplied ranges
###
def RangeHit(chr, pos, Ranges):
    if chr not in Ranges:
        return ''
    for (rangestart, rangestop) in Ranges[chr]:
        if pos >= rangestart and pos <= rangestop:
            return '%d-%d' % (rangestart, rangestop)
    return ''

###################  Pedigree File     ##########################################

###
### Load up a pedigree file
###
def load_Pedigree(PedigreeFile):
    PedigreeContent = open(PedigreeFile, "r").read().splitlines()
    return process_Pedigree(PedigreeContent)

###
### Load up a pedigree file
###
def process_Pedigree(PedigreeContent):

    Pedigree = defaultdict(lambda: defaultdict(lambda: {}))     ## The structure that we'll build
    samples_seen = set() ## We need to ensure uniqueness, even across families

#   with open(PedigreeFile, "r") as pf:
    for line in PedigreeContent:
            if re.match(r"Family\s*", line, re.IGNORECASE):        # skip any headers
                continue
            (Family, Sample, Father, Mother, Gender, Status, *rest) = line.strip().split(r',')

            if Mother == '.':
                Mother = ''
            if Father == '.':
                Father = ''
            ## Make sure no sample ID reuse
            if Sample in samples_seen:
                 sys.exit(f"Sample ID '{Sample}' seen twice in Pedigree. Sample IDs must be unique even across families")
            samples_seen.add(Sample)
    
            ## Validate the affected status - accept code or explicit representation
            if   Status.lower() in ('1', 'unaffected'):
                 Status = 'unaffected'
            elif Status.lower() in ('2', 'affected'):
                 Status = 'affected'
            else:
                 sys.exit(f"Unrecognized Affected Status ('{Status}') for Sample {Sample} in Family {Family}")

            ## Validate gender - accept code or explicit representation
            if   Gender.lower() in ('2', 'female'):
                 Gender = 'female'
            elif Gender.lower() in ('1', 'male'):
                 Gender = 'male'
            elif Gender.lower() in ('?', 'unknown'):
                 Gender = 'unknown'
            else:
                sys.exit(f"Unrecognized Gender ('{Gender}') for Sample {Sample} in Family {Family}")
    
            ## record the first affected we see in each family
            if Status == 'affected' and 'PROBAND' not in Pedigree[Family]:
                Pedigree[Family]['PROBAND'] = Sample

            Pedigree[Family]['MEMBER'][Sample] = {
               'Father': Father,
               'Mother': Mother,
               'Gender': Gender,
               'Status': Status
            }

    if len(Pedigree) == 0:
        sys.exit(f"Empty Pedigree File")

    return Pedigree


def check_and_clean_Pedigree_Family(Pedigree, Family, DiseaseModel):

    if Family not in Pedigree:
        sys.exit(f"Family {Family} not found in Pedigree")

    if 'PROBAND' not in Pedigree[Family]:
        print(f"No affecteds seen for Family {Family} in Pedigree", file=sys.stderr)
        sys.exit(0)

    if DiseaseModel == 'R':
        Mothers = { sample['Mother'] for sample in Pedigree[Family]['MEMBER'].values() if sample['Status'] == 'affected' }
        Fathers = { sample['Father'] for sample in Pedigree[Family]['MEMBER'].values() if sample['Status'] == 'affected' }
        if len(Mothers) > 1 or len(Fathers) > 1:
            if not pseudoDominant(Pedigree[Family]):
                print("Family %s Pedigree inconsistent with Recessive Analysis" % Family, file=sys.stderr)
       #        print("Family %s Pedigree inconsistent with Recessive Analysis" % Family)
                sys.exit(0)
       #    else:
       #        print("Family %s Pedigree is pseudodominant" % Family)
       #        sys.exit(0)
       #print("Family %s Pedigree is regular" % Family)
       #sys.exit(0)

    ###
    ### Remove any referenced person in family who is not themselves present in pedigree
    ###
    for Sample in Pedigree[Family]['MEMBER']:
        Father  = Pedigree[Family]['MEMBER'][Sample]['Father']
        Mother  = Pedigree[Family]['MEMBER'][Sample]['Mother']
        if (len(Father)>0 and Father not in Pedigree[Family]['MEMBER']):
            print(f"Ignoring missing father '{Father}' in family {Family} sample {Sample}", file=sys.stderr)
            Pedigree[Family]['MEMBER'][Sample]['Father'] = ''
        if (len(Mother)>0 and Mother not in Pedigree[Family]['MEMBER']):
            print(f"Ignoring missing mother '{Mother}' in family {Family} sample {Sample}", file=sys.stderr)
            Pedigree[Family]['MEMBER'][Sample]['Mother'] = ''


###
###  Is this pseudoDominant?  all affecteds are siblings except also one parent
###
def pseudoDominant(FamPed):
    affecteds = { s for s in FamPed['MEMBER'] if FamPed['MEMBER'][s]['Status'] == 'affected' }

    ###
    ### Remove any referenced person in family who is not themselves present in pedigree
    ###
    for Sample in Pedigree[Family]['MEMBER']:
        Father  = Pedigree[Family]['MEMBER'][Sample]['Father']
        Mother  = Pedigree[Family]['MEMBER'][Sample]['Mother']
        if (len(Father)>0 and Father not in Pedigree[Family]['MEMBER']):
            print(f"Ignoring missing father '{Father}' in family {Family} sample {Sample}", file=sys.stderr)
            Pedigree[Family]['MEMBER'][Sample]['Father'] = ''
        if (len(Mother)>0 and Mother not in Pedigree[Family]['MEMBER']):
            print(f"Ignoring missing mother '{Mother}' in family {Family} sample {Sample}", file=sys.stderr)
            Pedigree[Family]['MEMBER'][Sample]['Mother'] = ''


###
###  Is this pseudoDominant?  all affecteds are siblings except also one parent
###
def pseudoDominant(FamPed):
    affecteds = { s for s in FamPed['MEMBER'] if FamPed['MEMBER'][s]['Status'] == 'affected' }
    Mothers = { FamPed['MEMBER'][s]['Mother'] for s in affecteds }
    Fathers = { FamPed['MEMBER'][s]['Father'] for s in affecteds }
    if len(Mothers) == 1 and len(Fathers) == 1:
        return False
    affected_parents = affecteds & (Mothers | Fathers)
    if len(affected_parents) == 1:
        return True
    return False

###
###  Make a skeletal Pedigree file (just like the full-sized ones created above) for a Proband-Only execution
###
def skeleton_Pedigree(Proband):
    return {
          Proband: {                    ## This key is for the family (i.e. family ID will = person ID)
             'PROBAND': Proband,
              'MEMBER': {
                            Proband: {  ## This key is for the person
                               'Father': '',
                               'Mother': '',
                               'Gender': 'unknown',
                               'Status': 'affected'
                            }
                        }
         }
    }

###################  Cache File (Headers Only) ##################################

###
###  Read the Cache Headers (only; returning read handle for the content)
###
def load_CacheHeaders(CacheFile):
    headers = []
    cacheinput = open(CacheFile, "r")
    for line in cacheinput:
        line = line.strip()
        headers.append(line)

        ## if we're reached the end of headers, exit this loop
        ## NOTE that we're leaving the read handle open and returning it
        if line == '#':
            break

    return cacheinput, parseCacheHeaders(headers)


def parseCacheHeaders(headers):
    required_hdrs = ['SourceVCF', 'GeneModel', 'AnnotBinding', 'Phenotype', 'BuiltWhen', 'Samples']

    ### Wee're going to use a bare object to return the specific values as attributes
    class ch: pass
    CH = ch()

    ### Parse each '<key> <value>' and store as attribute
    for line in headers:
        if line == '#':
            continue
        m = re.match(r'^#(.*?)\s+(.*)', line)
        if not m:
            sys.exit(f"Invalid header in cache file: {line}")
        setattr(CH, m.group(1), m.group(2))

    ### Do some basic QC checks
    for hdr in required_hdrs:
        if not hasattr(CH, hdr):
            sys.exit(f"No '{hdr}' header seen in Cache file")
    
    samples = CH.Samples.split("\t")
    if len(samples) == 0:
        sys.exit("Empty sample list header seen in Cache file")

    return CH

def getAnnotModule(AnnotBinding):
    ### Do the Python import of the specific annotation bindings requested
    ### Either from a user-specified location or the built-in collection
    if '/' in AnnotBinding:
        spec = importlib.util.spec_from_file_location("module.name", AnnotBinding)
        AnnotBindingModule = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(AnnotBindingModule)
    else:
        AnnotBindingModule = importlib.import_module("Bindings." + AnnotBinding)
    AnnotBindingModule.Name = AnnotBinding
    return AnnotBindingModule


def formatCacheHeaders(VCFin, GeneModel, AnnotBinding, Phenotype, time, samples):
    return [
        "#SourceVCF\t" + VCFin,
        "#GeneModel\t" + GeneModel,
        "#AnnotBinding\t" + AnnotBinding,
        "#Phenotype\t" + Phenotype,
        "#BuiltWhen\t" + strftime("%Y-%m-%d %H:%M:%S", time),
        "#Samples\t" + "\t".join(samples),
        "#"
    ]
