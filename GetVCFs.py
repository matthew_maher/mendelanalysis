#!/usr/bin/env python3

import os
import sys
import shutil
import MendelArgs as MA

###
### Validate arguments
###
args = MA.ParseArgs('', 'CohortManifest', 'CohortDir')

if not os.path.exists(args.CohortDir):
    os.mkdir(args.CohortDir)
if not os.path.isdir(args.CohortDir):
    print("Fatal Error: %s exists but is NOT a directory!" % args.CohortDir)
    sys.exit(1)

###
### Loop through everything matching the filepath wildcard
###
for line in open(args.CohortManifest, "r").read().splitlines():
    print(line)

    (sample, VCF) = line.split(',')
    sample = sample.strip()
    VCF = VCF.strip()

    ### Skip the header
    if VCF == 'VCF':
        continue

    ###
    ### Copy the VCF file into the sub-subdir
    ###
    shutil.copyfile(VCF, args.CohortDir + "/" + sample + ".vcf")

    print("Sample %s copied\n\n" % sample)
