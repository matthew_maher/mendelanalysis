#!/usr/bin/perl

use strict;

###
###  This script is specific-to and only intended for use at MEE.  It will generate SQL insert
###  statements that are specific to an MEE-internal database table
###


die "usage: <Gene Model File> <Pheno/column> <input CSV from scoring tool> <run name>\n" unless @ARGV==4;
my ($GENE_file, $Pheno, $solutions_file, $runname) = @ARGV;

unless (-r $GENE_file && $GENE_file !~ /\//) {
    my $me = __FILE__;
    $me =~ s/MEE-specific\/[^\/]+$/GeneSets\//;
    $GENE_file = $me . $GENE_file;
}
die "Gene file '$GENE_file' not found/readable\n" unless -r $GENE_file;
my @GENE_file = `cat $GENE_file`;
my $hdr = shift @GENE_file;
my @cols = split /,/, $hdr;
die "Column '$Pheno' not found in Gene Model File\n"
    unless grep { $_ eq $Pheno } @cols;
my @DOM_genes;
my @REC_genes;
my @XL_genes;
my %gene_set;
my %ALT_symbols;
my %refseq;
foreach (@GENE_file) {
    ## Parse the fields and store in a hash by column name
    my @vals = split /,/;
    my %vals;
    foreach my $fld (@cols) {
        $vals{$fld} = shift @vals;
    }

    ## Skip genes not included in our Phenotype
    next if $vals{$Pheno} eq '';

    ## Let's record some symbol+transcript info that we'll need later
    $refseq{$vals{'Symbol'}}=$vals{'Pref RS Tran'};
    $refseq{$vals{'Alt Symbol'}}=$vals{'Pref RS Tran'};
    $gene_set{$vals{'Symbol'}}=1;
    $gene_set{$vals{'Alt Symbol'}}=1;
    $ALT_symbols{$vals{'Alt Symbol'}}=$vals{'Symbol'};

    ## Record all supported models 
    ## index by gene symbol as that's how the solutions file is coming in
    my @DMs = split /;/, $vals{'Disease Models'};
    foreach my $DM (@DMs) {
        push @DOM_genes, $vals{'Symbol'} if $DM eq 'AD';
        push @REC_genes, $vals{'Symbol'} if $DM eq 'AR';
        push @XL_genes, $vals{'Symbol'} if $DM =~ 'XL';
        if ($vals{'Alt Symbol'} ne '') { 
            push @DOM_genes, $vals{'Alt Symbol'} if $DM eq 'AD';
            push @REC_genes, $vals{'Alt Symbol'} if $DM eq 'AR';
            push @XL_genes, $vals{'Alt Symbol'} if $DM =~ 'XL';
        }
    }
}

### 
### Gene Model is inhaled.  Time to move on to the actual solutions file
###
die "Could not read solutions_file: $solutions_file\n" unless -r $solutions_file;
my @content = `cat $solutions_file`;

### 
### read the headers and note the column ordinal position of each
###
my $hdr = shift @content;
chop $hdr;
my @cols = split /,/, $hdr;
my %colpos;
for my $i (0..@cols) {
   ## There are certain column name synonyms that we will accept
   $cols[$i] =~ s/SampFam/Sample/i;
   $cols[$i] =~ s/cChange/HGVSc/i;
   $cols[$i] =~ s/pChange/HGVSp/i;
   $cols[$i] =~ s/\s//g;
   $colpos{uc($cols[$i])} = $i;
}

my @solutions;
###
### Gather/group all the calls by sample
###
my %calls;
foreach (@content) {
    chop;
    my @vals = split /,/;
    push @{$calls{$vals[$colpos{'Sample'}]}}, \@vals;
}

###
###  Validate the input columns - certain are required
###  Others SHOULD have consistent values within a given sample
###
my @required_columns = ('SAMPLE', 'CHR', 'POS', 'REF', 'ALT', 'GENE', 'HGVSC', 'HGVSP');
my @should_be_consistent = ('FINAL SOLUTION', 'GENE', 'CHR', 'RANK');

foreach my $col (@required_columns) { 
    die "ERROR: Column '$col' not found in input column headers\n" unless defined($colpos{$col});
}

###
### Examine each sample in turn, generating necessary insert statements
###
my @samples = sort keys %calls;
SAMPLE:
foreach my $sample (@samples) { 

    ## everyone appearing in the input will have prior calls removed. 
    ## thus people can be put back to 'unsolved'
    print "delete OGIDATA.dbo.HG19_SOLUTION where sample='$sample';\n";
    
    my @calls = @{$calls{$sample}};

    ###
    ### Confirm that fields that SHOULD be consistent ARE consistent
    ###
    foreach my $col (@should_be_consistent) { 
        if ($colpos{$col}) {
            my @values = map { $_->[$colpos{$col}] } @calls;
            my %check;
            @check{@values} = (1) x @values;
            if (keys %check > 1) { 
                warn "WARNING: Sample $sample has Inconsistent '$col' values for $sample\n";
            }
        }
    }

    my $gene = $calls[0]->[$colpos{'GENE'}];

    if ($colpos{'FINAL SOLUTION'}) {
        my $solution = $calls[0]->[$colpos{'FINAL SOLUTION'}];
        if ($solution =~ /unsolved/i) {
            warn "WARNING: Sample $sample has gene NOT blank ('$gene') but solution is 'unsolved'\n"
                unless $gene eq '';
        }
        if ($gene eq '') {
            warn "ERROR (skipped): Sample $sample has gene blank but NOT 'unsolved'\n"
                unless $solution =~ /unsolved/;
            next;
        }
    }

    unless ($gene_set{$gene}) { 
        warn "ERROR (skipped): Sample $sample has Gene '$gene' which is not seen in gene / disease model file\n";
        next;
    }

    ###
    ### Confirm that we have the correct # of variant calls for this gene's supported disease models
    ###
    if (@calls > 2) {
        warn "WARNING: More than 2 variants given for $sample\n";
    } elsif (@calls == 2) {
        ### check that recessive
        unless (grep { $_ eq $gene } @REC_genes) {
            warn "WARNING: Gene $gene is not REC, but two variants given for $sample\n";
        }
    } else {
        unless (grep { $_ eq $gene } @DOM_genes or grep { $_ eq $gene } @XL_genes) {
            warn "WARNING: Gene $gene is not DOM or X-L, but only one variant given for $sample\n";
            next;
        }
    }

    ###
    ### All the whining and warning is done.  Time to generate insert statements
    ###
    foreach my $call (@calls) { 
        my @vals = @$call;

        ### ASSERT 
        die "Huh?\n" unless $sample eq $vals[$colpos{'SAMPLE'}];

        my $chr   = $vals[$colpos{'CHR'}];
        my $pos   = $vals[$colpos{'POS'}];
        my $ref   = $vals[$colpos{'REF'}];
        my $alt   = $vals[$colpos{'ALT'}];
        my $gene  = $vals[$colpos{'GENE'}];
        my $HGVSc = $vals[$colpos{'HGVSC'}];
        my $HGVSp = $vals[$colpos{'HGVSP'}];

        ### Figure out if we want to flag this as a HOM call
        ### and look for consistency between the # of repetitions of the call and
        ### the 'HOM' flag in the output column of same name.
        my $poscnt = grep { $pos == $_->[$colpos{'POS'}] } @calls;
        my $inputHOM = grep { $_->[$colpos{'HOM'}] eq 'HOM' } @calls;
        my $HOM = ( $poscnt == 2 ) ? 1 : 0;
        if ($HOM && $inputHOM != 2) { 
             warn "Inconsistent HOM status for $sample variant $chr:$pos\n";
             warn "    Input 'HOM' flag was seen $inputHOM times.\n";
             warn "    variant row was repeated $poscnt times\n";
        }
        
        ## Hopefully, the input has a transcript column.  But if not, we'll proceed with blanks
        my $trans = $colpos{'TRANSCRIPT'} ? $vals[$colpos{'TRANSCRIPT'}] : '';
        
        my $Alt = $vals[$colpos{'ALT'}];
        if ($Alt =~ /CNV|ALU/) { 
            $HGVSc = $Alt;
            $HGVSp = '';
        }
        if ($HGVSc eq '') {
            warn "No variant detail for $sample Gene $gene - skipping\n";
            next;
        }
        
        my $canon_sample = canon($sample);

        print "insert OGIDATA.dbo.HG19_SOLUTION values('$canon_sample','$chr','$pos','$ref','$alt'," .
              "'$gene','$trans','$HGVSc','$HGVSp', $HOM, 'NGS', '$runname');\n";
    }

}

##################################################################################################

###
### Discern what TYPE of ID any given ID is
###
sub idtype {
    my $id = shift @_;
    $id =~ s/^Sample_//;
    return "DNUM" if $id =~ /^D/;
    return "OGI" if $id =~ /^OGI/;
    return "MGID" if $id =~ /^(BGL|)(VITA|)(_|-|)\d\d\d(-|_)\d\d\d+$/;

    ### If we get here, then some input contains a sample ID that isn't recognized, no doubt
    ### because it has some extraneous suffix or prefix on it.  In that case, it is necessary to add 
    ### logic above to recognize the type of the sample, and then also logic below in the 
    ### per-type sobroutines so that the ID will get properly canonicalized. 
    die "could not discern ID-type of sample ID: $id\n";
}

###
### Utility Routines to Canonicalize IDs
###
sub canon {
    my $sample = shift @_;
    my $type = idtype($sample);
    my $canon_id;
    if ($type eq 'MGID') {
        $canon_id = MGID_canon($sample);
    } elsif ($type eq 'OGI') {
        $canon_id = OGI_canon($sample);
    } elsif ($type eq 'DNUM') {
        $canon_id = D_canon($sample);
    } else {
        die "Could not convert to canonical ID for $sample / $type\n";
    }
    return $canon_id;
}

sub OGI_canon {
    my $id = shift @_;
    $id =~ s/\s+//g;
    $id =~ s/^Sample_//;
    $id =~ s/_(1|2|3)$//;
    $id =~ s/a$//;
    unless ($id =~ /^OGI(-|_|)0*(\d+)(-|_)(\d+)$/) {
        warn "BAD OGI: $id\n";
        return '';
    }
    my $fam = $2 + 0;
    return sprintf("OGI%d_%06d", $fam, $4);
}


sub D_canon {
    my $id = shift @_;
    $id =~ s/\s+//g;
    $id =~ s/_(c|b|e|R)$//;        # strip off any suffixes
    $id = $id . "-1" if $id =~ /^D\d+$/;
    unless ($id =~ /^D(\d+)(-|_)(\d+)$/) {
        warn "Could not parse Dnum: $id\n";
        return '';
    }
    $id = sprintf("D%05d_%06d", $1, $3);
    return $id;
}

sub MGID_canon {
    my $id = shift @_;
    $id =~ s/\s+//g;
    $id =~ s/BGLVITA_//;
    return '' if $id eq '';
    unless ($id =~ /^(\d+)(-|_)(\d+)$/) {
        warn "Could not parse BGL/MG-ID: $id\n";
        return '';
    }
    $id = sprintf("%03d-%03d", $1, $3);
    return $id;
}

