#!/usr/bin/env python3

import os
import sys
import MendelArgs as MA    ## Arg  parsing
import MendelFile as MF    ## File inhaling

### Configure, parse, validate our command line arguments
args = MA.ParseArgs('SgxiROorUC', 'GeneModel', 'Phenotype', 'AnnotBinding', 'VCFin', 'Pedigree')
args.CacheName = args.VCFin + ".cache"

###
### Step 1:  Build the cache file 
###
BC_switches = MA.switchString(args, 'XS')
cmd = f"BuildOneCache.py {BC_switches} {args.GeneModel} {args.Phenotype} {args.AnnotBinding} {args.VCFin} {args.CacheName}"
status = os.system(cmd)
if status > 0:
    print(f"Error {status} from command: {cmd}", file=sys.stderr)
    sys.exit(0)

###
### Step 2: Do the combined analysis
###
AC_switches = MA.switchString(args, 'XSgxiRrUOoC')
cmd = f"CreateCombinedAnalysis.py {AC_switches} {args.CacheName} {args.Pedigree} -"
status = os.system(cmd)
if status > 0:
    print(f"Error {status} from command: {cmd}", file=sys.stderr)
    sys.exit(0)
