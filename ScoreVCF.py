#!/usr/bin/env python3

import vcfpy
import sys
import re
import MendelArgs as MA    ## Arg parsing
import MendelFile as MF    ## File inhaling
import MendelBase as MB    ## Miscellaneous
import MendelVCF  as MV    ## Miscellaneous

args = MA.ParseArgs('', 'GeneModel', 'Phenotype', 'AnnotBinding', 'VCFin', 'VCFout')
GeneModel = MF.Load_Gene_Model_File(args.GeneModel, args.Phenotype, True)
AB = MF.getAnnotModule(args.AnnotBinding)

### Open input VCF
reader = vcfpy.Reader.from_path(args.VCFin)
ANN_fields = MV.validateBindings(reader, AB)

### Make sure that the VCF really is annotated as expected
if ANN_fields == None:
    sys.exit(f"'{AB.ANN_tag}' INFO Header not found in input VCF")

###
### We need to update the VCF headers to reflect the annotations that we are going to add
### Two additions to the per-transcript annotations provided by VEP:
###   - A score for Loss of Function
###   - A score for Gain of Function
###

### First, update the INFO line that describes the ANN (or whatever) values added by VEP/annovar/etc
for INFOline in reader.header._indices["INFO"]:
    IHLine = reader.header._indices["INFO"][INFOline] 
    if IHLine.id != AB.ANN_tag:
        continue
    ANN_field_strings = re.sub(r"^.*Format:\s*", "", IHLine.mapping['Description'])
    ANN_fields = ANN_field_strings.split('|')      ## This should return the exact same as earlier/above
    if 'LoFscore' in ANN_fields or 'GoFscore' in ANN_fields:
        sys.exit(f"'{AB.ANN_tag}' INFO Header already includes Scoring data fields")

    IHLine.mapping['Description'] += "|LoFscore|GoFscore"

### Then add individual descriptions of the two new values that we're adding to the lists 
reader.header.add_line(vcfpy.HeaderLine('LoFscore','Calculated Loss-of-Function Score'))
reader.header.add_line(vcfpy.HeaderLine('GoFscore','CAlculated Gain-of-Function Score'))

###
### With the headers modified, use them to open a new output stream
###
writer = vcfpy.Writer.from_path(args.VCFout, reader.header)

###
### Process through the VCF file record by record adding our new annotations
###
for record in reader:

    ### convert incoming VCF data to our customized data structure 
    Fields = MV.build_Fields(record, ANN_fields, AB)

    ### Allow the Bindings to gather up any variant-wide information that they want
    SWinfo = AB.getSetWideInfo(Fields['INFO']['ANNLIST'])

    ### Allow the Bindings to make any custom fields desired
    AB.updateFields(Fields)

    ### Now make a pass looking for transcript-based annotation sets to score
    for i in range(len(Fields['INFO']['ANNLIST'])):

        ### promote the current ANN row to be analyzed and get the Reg/Epi info in place
        ANN = Fields['INFO']['ANNLIST'][i]
        Fields['INFO'][AB.ANN_tag] = ANN

        ### Need to call the scoring routines, passing in all the relevant fields
        ### But we're only going to do this for the annotated biotypes which we care about
        ### So set a starting default of nothing
        LoFscore = ''
        GoFscore = ''

        ### Allow the Bindings to apply any of the info collected earlier
        ### and to create any desired derived fields
        AB.updateANNFields(ANN, SWinfo)

        ### is this row relevant to our scoring task?
        if AB.rowApplicable(Fields, GeneModel, args.Phenotype):

            ### The scoring routines that will be called below, normally work off a cache, in which
            ### consequence strings have been encoded (by BuildOneCache.py), and which gets loaded 
            ### a flat 'Fields' data structure, instead of the nested one we have here. So we need
            ### to make both of those conversions here to keep those routines happy

            ### Convert the fields to flat structure such as expected by scoring routines
            Flat_Fields = {}
            for field in AB.cacheColumns:
                val = Fields
                for step in field.split('.'):
                    val = val[step]
                Flat_Fields[field] = str(val)

            ### encode those consequence strings as the scoring routines expect them to be
            Flat_Fields['INFO.ANN.Consequence'] = AB.encodeConsequence(Flat_Fields['INFO.ANN.Consequence'])

            ###
            ### Use information from Gene Model File in order to calculate scores where needed
            ### Calculate scores using appropriate Pop Freq # - AR for LoF, AD for GoF
            ###
            Gene = AB.getGene(Fields, AB)
            if Gene in GeneModel or args.Phenotype == '*':
                GeneKey = Gene if Gene in GeneModel else '*'
                if 'R' in GeneModel[GeneKey]:
                    LoFscore = "%4.2f" % MB.scoreVariant(Flat_Fields, GeneModel, Gene, 'R', AB, '')

                if 'D' in GeneModel[GeneKey]:
                    GoFscore = "%4.2f" % MB.scoreVariant(Flat_Fields, GeneModel, Gene, 'D', AB, '')

                if 'X' in GeneModel[GeneKey]:
                    LoFscore = "%4.2f" % MB.scoreVariant(Flat_Fields, GeneModel, Gene, 'X', AB, '')

        ### Now tack the results onto the end of Annotation value list (matching the header change above)
        record.INFO[AB.ANN_tag][i] += "|" + LoFscore + "|" + GoFscore

    writer.write_record(record)
