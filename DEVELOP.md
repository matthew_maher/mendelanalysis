# The Mendelian Analysis Toolkit ("MATK") Developer Notes

## Prerequisites

This software is all Python3 - most currently tested on 3.7.6, but probably anything 3.6+ (as f-strings are used in the code) is likely to work.

This software was developed on a Linux platform and to date, only tested there. 

This software uses the 3rd-party module [**vcfpy**](https://pypi.org/project/vcfpy/) for parsing VCF files.  You must have it installed and available on your PYTHONPATH.  **TODO (?):  get 'vcfpy' out of BuildOneCache.py and possibly also ScoreVCF.py to eliminate this pre-req for most or all users (?)** 

The toolkit is comprised of a set of command-line tools, many of which call others.  As such, you will need to have the location of the top-level of the directory tree included in your execution "PATH" variable, as well as your PYTHONPATH for locating modules.  i.e.:

```
export PATH=$PATH:<your check-out location>
export PYTHONPATH=<your check-out location>
```

As this toolkit is all directly executable Python3 code, there is no build step required.

----------------

## Executable Software Inventory

The two main workhorse programs (see general descriptions below) are:

|Executable Program| Description  |
|---|---|
| **BuildOneCache.py** | Creates one cache from one VCF |
| **AnalyzeCase.py** | Analyze a single proband or family from a cache for a single disease model |

Several other files intended for direct execution are simply thin higher-level wrappers over the two tools above.  See the discussion and diagram of these 'helpers' in [**The User Guide**](USER.md) for more information.

|Executable Program| Description  |
|---|---|
| **BuildCaches.py**  | Wrapper for multiple calls to **BuildOneCache.py**  |
| **AnalyzeCohort.py**  | Wrapper for multiple calls to **AnalyzeCase.py**   |
| **CreateCombinedAnalysis.py**  | Wrapper for multiple calls to **Analyze(Case/Cohort).py**  |
| **BuildandCombinedAnalysis.py** | Wrapper for **BuildOneCache.py** and **CreateCombinedAnalysis.py** |

Additional executable tools (all described in the [**The User Guide**](USER.md)) provide miscelleneous functionality:

|Executable Program| Description  |
|---|---|
| **prepareGCNV.py** | Preprocessor for GCNV input (multiple per-sample files) |
| **prepareGCNVsummary.py** | Preprocessor for GCNV input (one summary file) |
| **GetVCFs.py** | Helper script to collect a set of VCF files into one directory |
| **AnnotVCF.py** | Helper script to call the local annotation tool (1-n times) |
| **ReCalcCohortFreq.py** | Compute in-cohort frequencies for a set of caches (rewriting them all)  |
| **SplitCache.py** | Split up a single cache in to per-(sample/family) caches, for speed  |
| **ShowGenotypes.py** | Debug/investigation tool to extract specific GTs from a cache |
| **ScoreVCF.py** | Calculate and add LoF and GoF scores to an annotated VCF |


## Supporting Module Inventory
Much of the code that the above programs depend upon is in several sub-modules, each of which is consistently imported into the executable programs under a unique alias when used throughout the codebase:

| Module Name | Alias | Description  |
|---|---|---|
| **MendelArgs.py** | MA | code for validating command line arguments, to ensure consistency from one tool to another.
| **MendelFile.py** | MF | code for parsing/validating/loading critical files like Gene Model, Pedigree, etc.
| **MendelDMs.py** | MD | code testing specific variants/pedigrees/callset for various Disease Inheritance Models
| **MendelCNV.py** | | code for processing CNV calls (generally originating from gCNV)
| **MendelBase.py** | MB | utilities for variant scoring and interfacing with Binding
| **MendelOut.py** | MO |  code for generating final output
| **MendelVCF.py** | MV | code for parsing a VCF file, and confirming it meets the need of the Bindings
| **VEP.py** | | Excapsulates the details of VEP-generated consequences (usable by Annotation Bindings)
| Annotation Binding | **AB** | the site/phenotype-specific annotation binding module

## General Descriptions of the two core programs
Below are very rough descriptions of the general flow of the two main programs.  Hopefully, this is rough description would be helpful to read before trying to dive into the actual code. 

#### BuildOneCache.py ("BOC")

The purpose of **BuildOneCache.py** is make the job of **AnalyzeCase.py** efficient and tractable by reducing the VCF input to a cache file which accomplishes two things:
 - vastly reducing the size of the file, by extracting:
    - only those variants that are deemed of interest according to the supplied gene model
    - only the fields of interest that are needed for the scoring calculation or the final output (both specified in the binding)
 - ensure a cache file that is grouped by gene.  This is as opposed to the VCF, where two genes can overlap/interleave.

The by-gene-grouping of the cache allows **AnalyzeCase.py** to have a minimal memory footprint.  To accomplish this grouped output, BOC collects records in memory, by gene, until the chromosome on the input VCF changes, and only then does it output all genes collected. This works well and has been run even on  WGS input of ~50 samples.  However, this algorithm is clearly memory intensive and may perhaps not work on certain machines with certain inputs.   Therefore a recoding of BOC's main loop may be necessary where it tracks the last seen position of each genes encountered, and once some sufficient distance (> largest known gene size) has passed, that gene can be immediately output and purged from memory, rather than wait for the end of the chromosome.  This should greatly reduce maximum memory demands.

#### AnalyzeCase.py

**AnalyzeCase.py** simply processes chunks of variants for one gene at a time from the cache.  For each gene, it:
   - checks for any supplemental calls from orthogonal "extra" sources (e.g. CNVs) that need to be added to the set of variants. See detailed discussion at end of this document.
   - depending on the requested disease model, it either checks individual variants or PAIRS of variant-alleles against the supplied pedigree.  This pedigree-checking is strictly a filter-OUT operation.
   - Any variant (or variant-pair) that passes pedigree-filtering is scored by the binding
   - if the scored variant (or variant-pair) passes the scoring cutoff, it is save for final output

Once the cache-file input is exhausted, any "extra" sources must be checked for any 'leftover' genes which still be unprocessed, if their gene was not represented in the primary cache input. 

Once all genes are processed, all variants slated for final output are sorted by score and then output is generated.

## Test Suite

For development, In order to execute the test code, you need to have the [**pytest**](https://docs.pytest.org/en/latest/contents.html) and [**pytest-console-scripts**](https://pypi.org/project/pytest-console-scripts/) packages installed and available.  Those can be installed, thusly:

```
pip install pytest
pip install pytest-console-scripts
```

With those installed, just type:

```
pytest
```

The actual test set code is all located in the subdirectory '**tests**'

Currently, this test set covers:

- unit tests of the logic (in **MendelDMs.py**) that does disease-model-specific variant-filtering
- unit tests of critical file loading/parsing operations (in **MendelFile.py**): Gene Model File, Pedigree File, Cache File (headers).
- unit tests of the output code (in **MendelOut.py**) to make sure that the various output sections (which vary depending on invocation switches) and their headers all come out in the expected manner.


- Various canned end-to-end (i.e. command-line based) tests *using the MEE/IRD bindings*: 
  - test of the header-only output of **AnalyzeCase.py**
  - tests of a handful of example (Rec, Dom, X-L) single sample inputs, using the MEE/IRD bindings.
  - test of a recessive family/pedigree-based example, using the MEE/IRD bindings.  
  - test of a dominant family/pedigree-based example, using the MEE/IRD bindings
  - two tests of recessive family with CNV input - in which CNV overlaps a HEMI SNV. 

While the unit tests complete nearly instantly, the end-to-end tests (which do command-line invocations) can take a few (~ 5?) seconds and may highlight portability issues if you want to attempt to run on a non-Linux platform. 



## CNVs and "Extra Sources"

The SNV data in the input cache file is the primary input to **AnalyzeCase.py**.  But CNV calls from an orthogonal call source (gCNV) and having completely different annotations (none, really, beyond gene) have been found to be a vital additional input and MATK allows those calls to be injected into the input stream, where they can be paired with SNV calls in proposed solutions.  It has been surmised that similar treatment will be likely sought for other orthogonal call sources (e.g. Mobile Elements from Mobster, or Structural variants from ....?, etc. ).  To prepare for that eventuality, the CNV code has been generalized into an instance of an "Extra" data source that meets a functional protocol that AnalyzeCase.py expects, and which is represented by the sequence diagram below.  At the code level, you should note that 'CNV' occurs nowhere in AnalyzeCase.py - it is currently aware of the CNV capability due it being imported at bottom of MendelArgs.py, through which it registers as an "extra" source.  This is meant to serve as a model for when/if other "extra" sources come online - if they can meet the same functional protocol as MendelCNV.py (diagram below) then they can be similarly and **safely** added without needing to touch the core executable AnalyzeCase.py.
The essense of the protocl is three phased:
1.  There is an initial exchange of calls that register the extra call source before the main processing begins.
2.  Then, for each gene that AnalyzeCase processes, it will call out to any/all registered extra sources to collect additional calls that they may have for that gene.  Once all calls are collected from all sources, each extra source is given a chance to review ALL the calls together (SNV + extra calls).  It is in this review stage, for instance, that the CNV extra source looks for HOM SNV calls that fall within a -1 Deletion CNV - and if found, it creates an alternative Hemizygous version of the SNV call.
3.  Once the SNV call source (which drives the main processing loop) is exhausted, AnalyzeCase makes a final call to the extra sources for "leftovers" - genes for which there were no SNV calls, but for which there are "extra" calls.  These calls then need to be processed before processing can be considered complete.

```mermaid
sequenceDiagram
    participant E as Extra Source
    participant AC as AnalyzeCase
    E->>AC: MA.RegisterExtra()
    AC->>E: setup(samples, binding)

    loop per Gene
    AC->>E: calls_for_gene()
    AC->>E: review_calls()
    end
    AC->>E: leftover_genes()
    Note over E,AC: repeat the loop above for leftovers
```


