#!/usr/bin/env python3

import os
import sys
import glob
import MendelArgs as MA
import MendelDMs as MD

###
### Parse command-line arguments
###
args = MA.ParseArgs('XgxiIRrUOoC', 'CohortDirOrCache', 'Pedigree', 'Family')
switches = MA.switchString(args, 'XgxiIRrUOoC')

###
### First we just call with -H in order to generate the headers only once
###
cmd = f"AnalyzeCohort.py -m -H {switches} {args.CohortDirOrCache} {args.Pedigree} R"
status = os.system(cmd)
if status > 0:
    print(f"Error {status} from command: {cmd}", file=sys.stderr)
    sys.exit(0)

###
### Now move on to calling AnalyzeCohort for each DiseaseModel (it will call each AnalyzeCase for each sample)
### And this time we include the -N to say 'no headers please'
###
for model in MD.supported_disease_models:
    if args.Family == '-':
        cmd = f"AnalyzeCohort.py -N -m {switches} {args.CohortDirOrCache} {args.Pedigree} {model}"
    else:
        print(f"Performing analysis for disease model '{model}'...", file=sys.stderr)
        if os.path.isdir(args.CohortDirOrCache):
            cache = args.CohortDirOrCache + "/" + args.Family + ".cache"
        else:
            cache = args.CohortDirOrCache
        cmd = f"AnalyzeCase.py {switches} -m -s {cache} {args.Pedigree} {model} {args.Family}"
    status = os.system(cmd)
    if status > 0:
        print(f"Error {status} from command: {cmd}", file=sys.stderr)
        sys.exit(0)

