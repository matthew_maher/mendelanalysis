#!/usr/bin/env python3

import os
import re
import sys
import glob
import MendelArgs as MA
import MendelFile as MF

###
### Validate arguments
###
args = MA.ParseArgs('', 'VCFinDirOrFile', 'VCFoutDirOrFile', 'AnnotBinding')
AB = MF.getAnnotModule(args.AnnotBinding)

if not hasattr(AB, 'annot_cmd'):
    sys.exit(f"The Binding {args.AnnotBinding} does not have a defintion for local annotation command ('annot_cmd')")

def runone(infile, outfile):

    ###
    ###  The specifics of the command(s) are site specific and thus are derived from a custom function in the bindings
    ###

    ## This is the local command for doing the annotation
    jobname = os.path.basename(infile).replace(".vcf", "")
    cmd = AB.annot_cmd(jobname, infile, outfile)
    print(cmd)

    status = os.system(cmd)
    if status > 0:
       sys.exit(f"Error {status} from command: {cmd}")


###
### We may be invoked with just a single file input and output...
###
if not os.path.isdir(args.VCFinDirOrFile):
    if os.path.isdir(args.VCFoutDirOrFile):
        sys.exit(f"Because {args.VCFinDirOrFile} is a file, the output must also specify a file")
    runone(args.VCFinDirOrFile, args.VCFoutDirOrFile)

###
### ...or an input and output DIRECTORIES.   In that case:
### Build a list of samples for which we will attempt to build a cache
###
else:
    cohort = glob.glob(args.VCFinDirOrFile + "/*.vcf")
    print(f"{len(cohort)} VCF files found in '{args.VCFinDirOrFile}'", file=sys.stderr)
    if not os.path.exists(args.VCFoutDirOrFile):
        sys.exit(f"The output target directory '{args.VCFoutDirOrFile}' does not exist")
    if not os.path.isdir(args.VCFoutDirOrFile):
        sys.exit(f"The output target directory '{args.VCFoutDirOrFile}' is not a directory")

    ### Some counters we'll use to report summary stats at the end
    c = 0  # total count
    s = 0  # # skipped
    e = 0  # # executed

    for vcf in cohort:

        c += 1

        basevcfname = os.path.basename(vcf)
        outvcf = args.VCFoutDirOrFile + "/" + basevcfname
        if os.path.exists(outvcf):
            print(f"{basevcfname} is already in the output directory (SKIPPING)", file=sys.stderr)
            s += 1   # already done
            continue

        runone(vcf, outvcf)
        e += 1

    print(f"{c} total VCF files in input directory")
    if s > 0:
        print(f"{s} VCFs were skipped because already in the output directory")
    print(f"{e} Annotation jobs executed")
