#!/usr/bin/env python3

import sys
import copy
import re

###
###  Validate that the incoming VCF contains all the annotations mentioned in the bindings
###
###  Note that this routine is premised upon 'reader' being object returned from 'vcfpy'
###  
def validateBindings(reader, AB):

    ###
    ###  Go get the 'ANN' info header created by the Annotation tool (e.g. VEP, Annovar, SnpEff)
    ###  And inspect it to see if all the required are there and in which positions
    ###
    INFO_fields = [l.id for l in reader.header.get_lines('INFO') ]
    ANNinfo = [l for l in reader.header.get_lines('INFO') if l.id == AB.ANN_tag ]
    if (len(ANNinfo) != 1):
        sys.exit(f"No '{AB.ANN_tag}' Info header found in the VCF input")
    ANNdesc = re.sub('^.*?Format: *','',ANNinfo[0].description)
    ANN_fields = ANNdesc.split('|')

    custom_fields = AB.customFieldList()

    ### Make sure that all the fields mentioned in the configuration, which should be from the 
    ### Annotation tool, are really included, according to the tools' VCF header
    ### Exception are derived fields such as Reg, or Mot; or fields derived by the Bindings
    for field in AB.cacheColumns:

        ### Certain fields are, by rule, always in the cache
        if field in [ 'CHROM',
                      'POS',
                      'REF',
                      'ALT',
                      'COHORT_AN',
                      'COHORT_AC' ]:
            continue 

        ### other fields, our bindings have promised tro populate
        if field in custom_fields:
            continue 

        ### Extra fields (supplied via -A) will be checked in the main program
        if field.startswith('EXTRA.'):
            continue

        ### Make sure that other fields the Bindings request are fdund in the input VCF
        Iprefix = 'INFO.'
        if field.startswith(Iprefix):
            if field.replace(Iprefix,'') in INFO_fields:
                continue

        Aprefix = 'INFO.' + AB.ANN_tag + '.'
        if field.startswith(Aprefix):
            if field.replace(Aprefix,'') in ANN_fields:
                continue

        sys.exit(f"Cache Field '{field}' is not valid")

    return ANN_fields


### 
### Convert VCF record structure supplied by 'vcfpy' package into a dict holding all the field values
### The assumption is that this is a standard VCF record, with Annotations added under the 'ANN' tag
### in the INFO section.  Note (in case of failure): Some people annotate under 'CSQ' instead of 'ANN'. 
###
def build_Fields(record, ANN_fields, AB):
    Fields = {}
    keys = record.INFO.keys()
    Fields["CHROM"] = re.sub('^chr','',record.CHROM, flags=re.IGNORECASE)
    Fields["POS"]   = record.POS
    Fields["REF"]   = record.REF

    ### ALT is trickier as the VCF parsing package returns it as a specific class
    Fields["ALT"]   = [ (a.value if hasattr(a, 'value') else a.sequence) for a in record.ALT ]
    if len(Fields["ALT"]) != 1:
        print(f"Multiple Alleles seen at {record.CHROM} / {record.POS} / {record.REF}", file=sys.stderr)
    Fields["ALT"] = Fields["ALT"][0]

    if Fields["ALT"] == '*':    ## These have no annotations
        return Fields

    Fields['COHORT_AC'] = record.INFO['AC'][0] if 'AC' in record.INFO else 0
    Fields['COHORT_AN'] = record.INFO['AN'] if 'AN' in record.INFO else 0

    for FreqField in AB.pop_Freq_columns:
        Fields[FreqField] = float(Fields[FreqField]) if FreqField in Fields else ''

    Fields["INFO"] = copy.deepcopy(record.INFO)

    ###  Now deal with the multiple rows of annotations information delivered by VEP
    if AB.ANN_tag not in record.INFO:
        sys.exit("The '%s' tag is missing from the record for: chr%s-%s-%s-%s" %
             (AB.ANN_tag, Fields["CHROM"], Fields["POS"], Fields["REF"], Fields["ALT"]))

    ### The top-level INFO annotations are potentially not preset.  So let's go through the 
    ### list of what the user intends to use and ensure at least a blank is present
    for column in AB.cacheColumns:
        parts = column.split('.')
        if parts[0] == "INFO" and parts[1] != AB.ANN_tag:
            if parts[1] not in Fields['INFO']:
                Fields['INFO'][parts[1]] = ''
                ## VCF parsing library may have supplied us with a list and possibly non-string values
            if isinstance(Fields['INFO'][parts[1]], list):
                Fields['INFO'][parts[1]] = ';'.join([str(x) for x in Fields['INFO'][parts[1]]])

    Fields["INFO"]["ANNLIST"] = []
    for ANNstr in record.INFO[AB.ANN_tag]:

        ### Parse apart the 'ANN' fields by their named positions (based on the INFO header)
        ANN = {}
        vals = ANNstr.split("|")
        for field in ANN_fields:
            ANN[field] = vals.pop(0)

        Fields["INFO"]["ANNLIST"].append(ANN)

        ### Loop through the INFO.ANN cache columns and fill in blanks for missing

    return Fields
