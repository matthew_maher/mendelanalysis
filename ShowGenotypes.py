#!/usr/bin/env python3

import sys

import MendelArgs as MA    ## Argument Parsing
import MendelFile as MF    ## File inhaling

### Configure, parse, validate our command line arguments
args = MA.ParseArgs('', 'CACHEin', 'Pedigree', 'Family', 'Chr', 'Pos')

###
###  Start by reading the headers off our cache to retrieve info we need to proceed
###
(cacheInput, CH) = MF.load_CacheHeaders(args.CACHEin)
samples = CH.Samples.split('\t')
AB = MF.getAnnotModule(CH.AnnotBinding)

if args.Pedigree != '-':
    if args.Family == '-':
        sys.exit("A Family must be specified when a pedigree is given")

    Pedigree = MF.load_Pedigree(args.Pedigree)

    ### Make sure we have a PROBAND and remove unresolved parental IDs
    MF.check_and_clean_Pedigree_Family(Pedigree, args.Family, '-')

    for ped_sample in Pedigree[args.Family]['MEMBER']:
        if ped_sample not in samples:
            sys.exit(f"Sample '{ped_sample}' from Pedigree not found in cache file sample list")

    if args.Family not in Pedigree:
        sys.exit(f"{args.Family}' is not a valid family in {args.Pedigree}")

    targets = [ x for x in Pedigree[args.Family]['MEMBER'] ]
    statlist= [ Pedigree[args.Family]['MEMBER'][x]['Status'] for x in targets ]
    tgtstat = { sam:sta for (sam, sta) in zip(targets, statlist) }



for cacheLine in cacheInput:
    row = cacheLine.strip().split(',')

    gene = row.pop(0)

    variant_data = {}
    for column in AB.cacheColumns:
        variant_data[column] = row.pop(0)   ## we want the effect of reducing the list, so that...

    GTs = row                         ## ...all the remaining columns are the per-sample genotypes
    if len(samples) != len(GTs):      ## reality check
        print(f"{args.CACHEin}: Length mismatch {len(samples)} samples <-> {len(GTs)} GTs")
        sys.exit(1)

    ### Is this the variant(s) the user wants GTs for?
    if variant_data['CHROM'] != args.Chr or variant_data['POS'] != args.Pos:
        continue

    ### Provide a description of the variant
    var = "-".join([variant_data[x] for x in ['CHROM','POS','REF','ALT'] ])
    print(var + ":")

    ### Display GTs for only those samples of interest
    for (sample, GT) in zip(samples, GTs):
        if args.Pedigree != '-' and sample not in targets:
            continue
        status = f"({tgtstat[sample]})" if args.Pedigree != '-' else ''
        print("%s  %s %s" % (GT, sample, status))

