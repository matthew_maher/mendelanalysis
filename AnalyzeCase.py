#!/usr/bin/env python3

import sys
import re 
from collections import defaultdict 
from itertools import groupby

import MendelArgs as MA    ## Argument Parsing
import MendelFile as MF    ## File inhaling
import MendelDMs  as MD    ## Disease Model Logic
import MendelBase as MB    ## Miscellaneous
import MendelOut  as MO    ## Output

###  Shorthand routine to print to STDERR
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

### Configure, parse, validate our command line arguments
args = MA.ParseArgs('PZgxidmwsHfCtRrU', 'CACHEin', 'Pedigree', 'DiseaseModel', 'Family')

###
###  Start by reading the headers off our cache to retrieve info we need to proceed
###
(cacheInput, CH) = MF.load_CacheHeaders(args.CACHEin)
samples = CH.Samples.split('\t')
AB = MF.getAnnotModule(CH.AnnotBinding)

### inhale the gene model phenotype definitions
### if a gene symbol was supplied with -g, validate and convert to a gene ID
GeneModel = MF.Load_Gene_Model_File(CH.GeneModel, CH.Phenotype, args.s or args.H)
if args.g:
    args.g = MF.check_and_translate_gene(args.g, GeneModel, CH.GeneModel)

### And now with the Bindings loaded, we can possible get the scoring cutoff from a default list there
if args.Z == None:
    if hasattr(AB, 'DefaultScoreCutoff'):
        args.Z = AB.DefaultScoreCutoff[args.DiseaseModel]
    else:
        args.Z = 0
try:
    args.Z = float(args.Z)
except ValueError:
    sys.exit(f"The supplied -Z <scoring cutoff> value ('{args.Z}') is not numeric")

###
### Load up any Genomic Ranges supplied in a file, which we'll use to filter results
### Add additional column to output to display the matching ranges
###
if args.R != None:
    Ranges = MF.load_RangesFile(args.R)
    AB.outputColumns.append('RANGE')

###
### Figure out what we're solving (family?, person?)
### Whichever way we're invoked, we want to build a Pedigree structure so that
### the rest of the software need not worry about the invocation arguments
###
if args.P == '-':
    args.P = samples[0]
if args.P and (args.Pedigree != '-' or args.Family != '-'):
        sys.exit("<Pedigree> and <Family> must be '-' if using -P")
if args.Pedigree == '-' and args.Family != '-':
        sys.exit("<Family> must be '-' if <Pedigree> is '-'")

### Easy single-sample invocation allows the sample to be extracted from the
### cache or supplied via -P.  Either way, let's construct a skeletal pedigree
### which matches the structure created by MF.load_Pedigree()
if args.Pedigree == '-':
    Proband = args.P if args.P else samples[0]
    Pedigree = MF.skeleton_Pedigree(Proband)
    args.Family = Proband

### More full-blown invocation supplies a Pedigree file and identifies Family within it.
else:
    Proband = None
    ### Note that below, args.Family may go in as '-' but come back as first in file
    Pedigree = MF.load_Pedigree(args.Pedigree)

    ### We've been given a pedigree, not no family. Let's try to find a match from our sample list
    if args.Family == '-':
        for PedFam in Pedigree:
            for PedFamMem in Pedigree[PedFam]['MEMBER']:
                if PedFamMem in samples:
                    args.Family = PedFam

    if args.Family == '-':
        eprint(f"No sample from {args.CACHEin} found in Pedigree {args.Pedigree}")
        sys.exit(0)

    ### Make sure we have a PROBAND and remove unresolved parental IDs
    MF.check_and_clean_Pedigree_Family(Pedigree, args.Family, args.DiseaseModel)

###  If -H is given, that's all we're doing
if args.H:
    print(",".join(MO.headers(args, AB)))
    sys.exit(0)

###
### Output the basics of what we've been asked to analyze
###
FamPed = Pedigree[args.Family]['MEMBER']
if not args.s:    ## unless the user said 'ssshhhh!'
    print(f"Gene Model File in use: {CH.GeneModel}")
    print(f"Phenotype: {CH.Phenotype}")
    print(f"AnnotBinding: {CH.AnnotBinding}")
    if Proband is not None:
        print(f"Proband-Only Analysis for: {Proband}")
    else:
        print(f"Pedigree in effect: {args.Pedigree}")
        print(f"AFFECTED(s) individuals  # (Father:Mother) :")
        unaffecteds = []
        for sample in FamPed:
            if FamPed[sample]['Status'] == 'affected':
                father = FamPed[sample]['Father']
                mother = FamPed[sample]['Mother']
                print(f"   {sample} ( {father} : {mother} )")
            else:
                unaffecteds.append(sample)
        
        print("UNAFFECTEDS (within  family): " + " ".join(unaffecteds))
        print("")

   ### UNRESOLVED: how to handle the OTHER families in the pedigree or cache?
   ### Assuming they're in both, then we know their status and the DiseaseModel
   ### code COULD filter against them as well.  Need to discuss...


    print("Disease Model: " + MD.supported_disease_models[args.DiseaseModel])
    if args.f and args.DiseaseModel == '1':
        print(f"'first-only' filtering in effect: {Proband}")
    print("")

###
### at this point we have read all the cache headers and have collected the sample list.
### So do various checks that the requested operation and pedigree information matches
### the samples seen in the data cache
###
### Note that we only checking the family-of-interest, and see 'UNRESOLVED' note above
###
for ped_sample in FamPed:
    if ped_sample not in samples:
        sys.exit(f"Sample '{ped_sample}' from Pedigree not found in cache file sample list")

affecteds = [s for s in FamPed if FamPed[s]['Status'] == 'affected'  ]

###
### The main input is of course the cache.
### But we want to have a generalized concept of 'extra' sources of calls that can be merged in
### Currently, there is only one 'extra': CNV calls from gCNV
###  But someday, there could be any # of others such as complex SV calls from (your-tool-here)
###  or possibly mobile elements from Mobster, or.... whatever else becomes available. 
### 
### Hopefully, if a new 'extra' source becomes available, the implementation can be put in 
### it's own python file (like MendelCNV.py) and just a few changes to the few lines below
###
for extra in MA.ExtraSources():
    extra.setup(args, samples, AB)

###
###  The main processing loop is essentially a simple loop through the cache/SNV file,
###  but adding in any calls from 'extra' call sources (e.g. CNVs) at each gene.
###  The main cache file MUST BE GROUPED BY GENE, although the collation sequence is unknown
###
counters = defaultdict(lambda: 0 )
def main_loop():

    rows_output = []

    ### A generator which the groupby below will iterate over
    def main_Input():
        for cacheLine in cacheInput:
            yield cacheLine.strip().split(',')

    index_of_geneid = 0   # what column are we grouping on? 
    for gene, calls in groupby(main_Input(), lambda x: x[index_of_geneid]):
       
        ## We want to convert all relevant calls to a more complex data structure (calls_to_consider)
        ## keyed by the variant description, and with vazrious meta-data populated. 
        calls_to_consider = defaultdict( lambda: {} )
    
        ## Our primiary input is the SNV data from the cache - filter and load those (for this one gene)
        for (variant_name, variant_data) in filter_SNVs(gene, calls):
            calls_to_consider[variant_name] = variant_data
    
        ## But now check with any of our 'extra' sources to see if others calls can be added
        for extra in MA.ExtraSources():
            for (variant_name, variant_data) in extra.calls_for_gene(gene, GeneModel):
                calls_to_consider[variant_name] = variant_data

        ## All data for this gene is loaded - send it down the pipeline
        process_one_gene(gene, calls_to_consider, rows_output)

    ###
    ### At this point we've reached the end of the MAIN (SNV) input.  But what if one (or more!)
    ### of the 'extra' sources had calls still pending (i.e. calls for which no SNVs were seen)?
    ### The 'extra' routines have a special method to report the list of 'leftover' genes.
    ###
    ### The code below will make a unified SET of the remaining genes and process them, using only
    ### the extra sources.   Possible case: a recessive gene with no SNVs in the main file (so it
    ### never got processed in the loop above, but it has one call in one extra source (e.g. CNVs) 
    ### and second call in another extra source (e.g. Mobile Element).  This extra pass will catch 
    ### that as a possible solution. 
    ###
    ### You might ask: why is the above code not just a multifile-merge of all input streams at once?
    ### Answer: because we don't know the collation sequence of the SNV data
    ###

    leftover_genes = { gene for extra in MA.ExtraSources() \
                            for gene in extra.leftover_genes() }

    for gene in leftover_genes:
        calls_to_consider = { name: data for extra in MA.ExtraSources() \
                                         for (name, data) in extra.calls_for_gene(gene, GeneModel) }

        process_one_gene(gene, calls_to_consider, rows_output)

    # finally, sort and print out the output 
    for row in MO.final_output(rows_output, AB, args, Proband):
        print(','.join(row))

    final_summary()  # show counters

    ## All Done
    sys.exit(0)


def filter_SNVs(gene, rows_input):

    for row in rows_input:

        ### the first field better darn well be the expected gene ID
        if row.pop(0) != gene:
            sys.exit("INTERNAL ERROR: gene <-> row_gene mismatch")

        counters['rowsread'] += 1
        ### convert it to a dict to avoid coding lots of positional constants
        variant_data = {}
        for column in AB.cacheColumns:
            variant_data[column] = row.pop(0)   ## we want the effect of reducing the list, so that...

        GTs = row[0:len(samples)]         ## ...the remaining columns are the per-sample genotypes
        GQs = row[len(samples):]          ## ...                          and per-sample qualities
        if len(samples) != len(GTs):      ## reality check
            print(f"{args.CACHEin}: Length mismatch {len(samples)} samples <-> {len(GTs)} GTs")
            sys.exit(1)
        if len(samples) != len(GQs):      ## reality check
            print(f"{args.CACHEin}: Length mismatch {len(samples)} samples <-> {len(GQs)} GQs")
            sys.exit(1)
        variant_data['GTs'] = { sample: list(GT) for (sample, GT) in zip(samples, GTs) }
        variant_data['GQ'] = ';'.join(GQs)

        variant_name = '-'.join(variant_data[x] for x in ['CHROM', 'POS', 'REF', 'ALT'])
        variant_data['POS'] = int(variant_data['POS'])

        ### Hard-Filtering by Population Frequency - the bindings define one or more fields to screen against
        ### If any of them are filled in, then if any of them passes our cutoff, then keep it
        ### Net result: if none are filled in, or if they disagree vis-a-vis the cutoff, we keep it
        if args.x:
            PF_vals = [ variant_data[x] for x in AB.pop_Freq_columns if variant_data[x] != '' ]
            if len(PF_vals)>0 and all(float(PF_val) >= args.x for PF_val in PF_vals):
                continue

        ###  Hard-Filtering for Cohort/kit-specific artifacts and noise
        ###  The point is to filter out variants that may be cohort-artifacts and not
        ###  reflected in the general purpose population frequency databases
        if args.i and 'COHORT_AC' in variant_data and 'COHORT_AN' in variant_data:
            AC = int(variant_data['COHORT_AC'])
            AN = int(variant_data['COHORT_AN'])
            AF = AC / AN if AN > 0 else 0
            if AF > args.i:
                continue 

        ### The variant has passed Frequency Filtering (Population References and In-Cohort)
        counters['rowsconsidered'] += 1

        variant_data['RANGE'] = MF.RangeHit(variant_data['CHROM'], variant_data['POS'], Ranges) if args.R else ''

        ### Evaluate this variant for the requested disease model
        variant_data['SCORE'] = MB.scoreVariant(variant_data, GeneModel, gene, args.DiseaseModel, AB, args.d)

        ### the Consequences are encoded in the cache - we need to decode them for display
        cons_path = f"INFO.{AB.ANN_tag}.Consequence"
        variant_data[cons_path] = AB.decodeConsequence(variant_data[cons_path])

        ### A few extra critical housekeeping fields
        variant_data['TYPE']   = 'SNV'
        variant_data['SYMBOL'] = AB.getSymbol(variant_data, AB)

        ### Give the Bindings one last chance to mess with things
        AB.FinalAdjustments(variant_data)

        yield (variant_name, variant_data)
        
    return

###
### Given all filtered and prepped calls (for one gene) we need to find all model-appropriate candidates
### 
def process_one_gene(gene, calls_to_consider, rows_output):

    ## There may be nothing to do
    if len(calls_to_consider) == 0:
        return

    ## Analysis is usually restricted to the Gene Model Set
    if gene not in GeneModel and '*' not in GeneModel:
        return

    ## Did the user want to restrict to a single gene?
    if args.g != None and args.g != gene:
        return

    ## does this gene even support the requested Disease Model?
    if args.DiseaseModel not in GeneModel[gene if gene in GeneModel else '*']:
        return 

    ###
    ### Before proceeding, we have a hook to allow any 'extra' sources to review the call set
    ### The current and only reason is that CNV extra source will look for HOM SNVs within
    ### any HET DEL CNV and guess that the SNV is really HEMI, and it will add a new SNV as such
    ###
    for extra in MA.ExtraSources():
        extra.review_calls(calls_to_consider, samples)

    ## Time for a QC/Reality check - do all variants have the same chromosome? (they better!)
    chroms_seen = { v['CHROM'] for v in calls_to_consider.values() }
    if len(chroms_seen) > 1:
        sys.exit(f"Internal Error: Multiple CHRs ({','.join(chroms_seen)}) seen for gene: {gene}")
    chrom = chroms_seen.pop()  ## turn the set of one into a scalar

    ## one more reality check
    if not re.match(r"^\d+$", chrom) and chrom not in ['X', 'Y', 'M', 'MT']:
        sys.exit(f"Unrecognized Chromosome ('{chrom}') for {gene}")

    ## As Chrom 'X' has special handling, let's make sure only apply XL logic appropriately
    if args.DiseaseModel == 'X' and chrom != 'X':
        return
    if args.DiseaseModel != 'X' and chrom == 'X':
        return

    ## call the detailed logic to identify candidate solutions that fit the requested disease model
    ## But first extract just the GTs from the structure.  The DiseaseModel code tries to be pure and
    ## only knows about GTs - no usage of the other 'field' information
    GTs = { var: calls_to_consider[var]['GTs'] for var in calls_to_consider }
    candidates = MD.find_candidates(args.DiseaseModel, GTs, Pedigree, args.Family, args.f)

    ## For every candidate solution identified, send it on to the next phase (-Z filtering, sorting, output)
    for varlist in candidates:
        record_candidate(rows_output, calls_to_consider, varlist)
        counters['candidatecount'] += 1

    return

###
### Common (to all disease models) routine which:
###    - filters by -Z score cutoff
###    - records variants for subsequent output
### 
def record_candidate(rows_output, data, vlist):
    for v in vlist:
        allele_list = []
        for s in affecteds:
            for a in data[v]['GTs'][s]:
                if a == '1':
                    allele_list.append(s)
        allele_list.sort()
        data[v]['AFF_ALLS'] = ';'.join(allele_list)
    ###
    ### For recessives, we want to list the higher scoring variant first
    ### And we want to set a cap/max on score for single variant to prevent one
    ### super-high scorer from generating two-main combination pairs
    ###
    varlist = sorted(vlist, key=lambda v: data[v]['SCORE'], reverse=True)
    scores = [ data[v]['SCORE'] for v in varlist ]
    if  args.DiseaseModel == 'R':
        ### Bindings have an optional max on one variant's score
        if hasattr(AB, 'MaximumRecessiveVariantScore'):
            max_score = AB.MaximumRecessiveVariantScore
            scores = [ min(data[v]['SCORE'], max_score) for v in varlist ]
    
    ###
    ### Most importantly: see if this row passes our scoring filter 
    ###
    combined_score = sum(scores) / len(scores)
    if combined_score < args.Z:
        return
    
    counters['candidateZcount'] += 1

    ###
    ### if Ranges were supplied, we must ensure the desired relationship to our variants:
    ###     with -R, at least one of the variants must fall within a range
    ###     with -r, when in Recessive mode, the LOWEST scoring variant must fall within a range
    ###
    if args.R:
        if args.r:
            if data[varlist[-1]]['RANGE'] == '':
                return
        else:
            if all([data[v]['RANGE'] == '' for v in varlist]):
                return

    ###
    ### if UTR filtering requested, we must ensure that at least one variant is UTR
    ###
    if args.U:
        if all(['UTR' not in data[v]['CONSEQ'] for v in varlist]):
            return
    
    ### Construct a key that will be used to get all output sorted in descending order of:
    ###   - the combined score
    ###   - the symbol, which we will take from the first row
    sortkey = "%09.3f%-10s%2d%s" % (combined_score, data[varlist[0]]['SYMBOL'], len(varlist), ':'.join(varlist))

    ### Drop the whole bundle in the slot
    rows_output.append(tuple((sortkey, combined_score, scores, data, varlist)))
    return

def final_summary():
    # Output Summary Statistics 
    if not args.s:
        eprint("")
        eprint("     %d rows read from cache" % counters['rowsread'])
        eprint("     %d rows passed gene/frequency filters" % counters['rowsconsidered'])
        eprint("     %d candidate solutions" % counters['candidatecount'])
        eprint("     %d candidate solutions passed -Z filter" % counters['candidateZcount'])
        eprint("")

main_loop()
