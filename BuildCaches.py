#!/usr/bin/env python3

import os
import re
import sys
import glob
import MendelArgs as MA
import MendelFile as MF

###
### Validate arguments
###
args = MA.ParseArgs('I', 'GeneModel', 'Phenotype', 'AnnotBinding', 'VCFdir')
AB = MF.getAnnotModule(args.AnnotBinding)

def cache_command(vcf):
    basename = os.path.basename(vcf).replace('.vcf','').replace('.ANNOT','')
    argI = f"-I {basename}" if args.I == '*' else ''
    cache = vcf + ".cache"
    return f"BuildOneCache.py {argI} {args.GeneModel} {args.Phenotype} {args.AnnotBinding} {vcf} {cache}"

###
### Get our list of VCFs for which we will attempt to build a cache
###
if  os.path.isdir(args.VCFdir):
    cohort = glob.glob(args.VCFdir + "/*.vcf")
    print(f"{len(cohort)} VCFs found in '{args.VCFdir}'", file=sys.stderr)
else:
    cmd = cache_command(args.VCFdir)
    print(cmd, file=sys.stderr)
    status = os.system(cmd)
    if status > 0:
        sys.exit("Error %d from command: %s" % (status, cmd), file=sys.stderr)
    sys.exit(0)

###
### Loop through each VCF, confirm that we have an annotated VCF, and submit annotation job
###
s = 0
for vcf in cohort:

    ### Get the base command - suitable for direct execution
    cmd = cache_command(vcf)

    ### 
    ### We will either execute it as it, OR, if the user supplied a prefix command in the Bindings Module (e.g. to 
    ### submit the job to a queue), then we will add that
    ###
    if hasattr(AB,'submit_cmd'):
        jobname = os.path.basename(vcf).replace(".vcf","")
        cmd = AB.submit_cmd(jobname) + " " + cmd
        s += 1

    print(cmd, file=sys.stderr)

    status = os.system(cmd)
    if status > 0:
        print("Error %d from command: %s" % (status, cmd), file=sys.stderr)
        sys.exit(0)

if s > 0:
    print(f"\n{s} cache-building jobs submitted\n", file=sys.stderr)
else:
    print("All Done.", file=sys.stderr)

