#!/usr/bin/env python3

from collections import defaultdict
import glob
import sys
import os
import re
import MendelArgs as MA
import MendelFile as MF

###
### Construct our command line argument definition/parser
###
args = MA.ParseArgs('', 'AnnotBinding', 'CohortDir')
AB = MF.getAnnotModule(args.AnnotBinding)

###
### Determine the position (col #) in the cache where there in-cohort stats will be rewritten to
###
COHORT_AC_COL = None
COHORT_AN_COL = None
for i in range(len(AB.cacheColumns)):
    if AB.cacheColumns[i] == 'COHORT_AC':           
        COHORT_AC_COL = i + 1
    if AB.cacheColumns[i] == 'COHORT_AN':           
        COHORT_AN_COL = i + 1

if COHORT_AC_COL == None:
    print("COHORT_AC_COL not seen in column list")
    sys.exit(1)
if COHORT_AN_COL == None:
    print("COHORT_AN_COL not seen in column list")
    sys.exit(1)

###
### Data structures where we'll gather data
###
callcnts = defaultdict(lambda: 0)          
counted = defaultdict(lambda: defaultdict(lambda: False )) # will be: [<variant>][<sample>] = <Boolean>

###
### First, let's get the list of files to count up
###
AN_total = 0
samples_seen = set()
WCpath = args.CohortDir + "/*.cache"
files = glob.glob(WCpath)
print("%d cache files seen under '%s'" % (len(files), WCpath), file=sys.stderr)

for infile in files:

    print("Processing: %s..." % infile, file=sys.stderr)

    samples = []
    cacheInput = open(infile,"r")


    for cacheLine in cacheInput:
        cacheLine = cacheLine.strip()

        ###
        ### deal with any headers
        ###
        if cacheLine.startswith('#'):

            ### Make sure the cache version agrees with our code/definitions
            m = re.match(r'^#AnnotBinding\s+(.*)', cacheLine)
            if m:
                if m.group(1) != args.AnnotBinding:
                    print("Cache is wrong AnnotBinding (%s seen; %s expected) in %s" % (m.group(1), args.AnnotBinding, file))
                    sys.exit(1)
                continue

            ### Get the list of samples
            m = re.match(r'^#Samples\t(.*)$',cacheLine)
            if m:
                samples = m.group(1).split("\t")
                for sample in samples:
                    if sample in samples_seen:
                        print("sample '%s' seen multiple times" % sample)
                        sys.exit(1)
                    samples_seen.add(sample)

                AN_total += len(samples)*2
                continue

            ## and skip any other sort of header we see
            continue
           

        ###
        ### At this point, we know it's data row (of comma-separated columns defined in meta-data)
        ###
        vals = cacheLine.split(',')
        Gene = vals.pop(0)

        ### convert it to a dict to avoid coding lots of positional constants
        fields = {}
        for column in AB.cacheColumns:
            val = vals.pop(0)
            fields[column] = val

        GTs = vals[0:len(samples)]         ## ...the remaining columns are the per-sample genotypes
        GQs = vals[len(samples):]          ## ...                          and per-sample qualities

        if len(samples) != len(GQs):      ## reality check
            print(f"{args.CACHEin}: Length mismatch {len(samples)} samples <-> {len(GQs)} GQs")

        variant_name = '-'.join([fields['CHROM'],fields['POS'],fields['REF'],fields['ALT']])

        ### walk through GTs and samples
        for sample in samples:
            GT = GTs.pop()

            if counted[variant_name][sample] == True:
                continue
            counted[variant_name][sample] = True

            AC = None
            if GT == '11':
                AC = 2 
            if GT == '10' or GT == '01' or GT == '1.' or GT == '.1':
                AC = 1 
            if GT == '00' or GT == '..':
                AC = 0
            if AC == None:
                print("unrecognized GT: %s" % GT)
                sys.exit(1)

            callcnts[variant_name] += AC
    

        if len(GTs) != 0:
            print("Too many GTs in list")
            sys.exit(0)

    cacheInput.close()


print("All Input read and counts calculated. Now rewriting Cache files", file=sys.stderr)

for infile in files:

    print("Rewriting: %s..." % infile, file=sys.stderr)

    samples = []
    cacheInput = open(infile, "r")

    outfile = infile + ".new"
    cacheOutput = open(outfile, "w")

    for cacheLine in cacheInput:
        cacheLine = cacheLine.strip()

        ## any headers seen, just pass them through
        if cacheLine.startswith('#'):
            print(cacheLine, file=cacheOutput)
            continue

        orig_vals = cacheLine.split(',')
        vals = cacheLine.split(',')
        Gene = vals.pop(0)

        ### convert it to a dict to avoid coding lots of positional constants
        fields = {}
        for column in AB.cacheColumns:
            val = vals.pop(0)
            fields[column] = val

        variant_name = '-'.join([fields['CHROM'],fields['POS'],fields['REF'],fields['ALT' ]])

        if variant_name not in callcnts:
            print("FATAL ERROR: variant '%s' not seen in calculated content list?" % variant_name)
            sys.exit(1)

        orig_vals[COHORT_AC_COL] = str(callcnts[variant_name])
        orig_vals[COHORT_AN_COL] = str(AN_total)

        print("%s" % ','.join(orig_vals), file=cacheOutput)

    cacheInput.close()
    cacheOutput.close()

    ### Now swap the new file into place of the original 
    os.remove(infile)
    os.rename(outfile, infile)

print("All Done.", file=sys.stderr)
