#!/usr/bin/env python3
import os
import re
import sys
import glob
import argparse
import MendelArgs as MA    ## Arg parsing
import MendelFile as MF    ## File inhaling
from collections import defaultdict

###
### Validate arguments
###
args = MA.ParseArgs('', 'GeneModel', 'Phenotype', 'GCNVdir', 'gencodeGTF', 'Padding')

### test padding to be numeric
try:
    args.Padding = int(args.Padding)
except ValueError:
    print("Padding value not numeric: %s" % args.Padding)
    sys.exit(1)

###
### Get our list of samples for which we will attempt to build a cache
###
infiles = [ os.path.basename(x) for x in glob.glob(args.GCNVdir + "/*segments_gCNV.vcf")]
print("%d input files found in '%s'" % (len(infiles), args.GCNVdir), file=sys.stderr)

### inhale the gene model phenotype definitions
print("Reading in Phenotype-specific Gene Model File...", file=sys.stderr)
GeneModel = MF.Load_Gene_Model_File(args.GeneModel, args.Phenotype, True)
print("%d genes for the phenotype" % len(GeneModel), file=sys.stderr)

###
### Process the gencode GTF looking for our genes of interest
###
print("Reading in GENCODE GTF (%s)..." % args.gencodeGTF, file=sys.stderr)
genes_by_chr = defaultdict(lambda: [])
c = 0
lines = open(args.gencodeGTF, "r").read().splitlines()
for line in lines:
    if line.startswith('#'):
        continue

    chrom, src, type, start, stop, extra, strand, frame, rest = line.split('\t')
    if type != 'gene':
        continue
    chrom = chrom.replace('chr', '')
    restvals = re.split(r";\s*", rest)
    flds = {}
    for ival in restvals:
       if ival == '':
          continue
       m = re.match(r"^(.*?) (.*)$", ival)
       if m:
           key = m.group(1)
           val = m.group(2)
       else:
           print("could not parse key and val: %s" % ival, file=sys.stderr)
           sys.exit(1)
       val = val.replace('"', '')
       flds[key] = val

    ENS = flds['gene_id']
    SYM = flds['gene_name']
    ENS = re.sub(r"\.\d+$", '', ENS)
    if ENS not in GeneModel:
        continue
    del GeneModel[ENS]
    c += 1
    try:
        start = int(start)
    except ValueError:
        print("Start value not numeric in GTF file at %s: %s" % (ENS, start))
        sys.exit(1)
    try:
        stop = int(stop)
    except ValueError:
        print("Stop value not numeric in GTF file at %s: %s" % (ENS, stop))
        sys.exit(1)

  # if chrom not in genes_by_chr:
  #     genes_by_chr[chrom] = []
    genes_by_chr[chrom].append((start, stop, ENS, SYM))

print("%d genes found in GTF file" % c, file=sys.stderr)
leftover = GeneModel.keys()
if len(leftover) > 0:
    print("%d not found: %s" % (len(leftover), leftover.join(', ')), file=sys.stderr)

    
print("Processing GCNV input files...", file=sys.stderr)
for infile in infiles:
    print("Reading in %s..." % (args.GCNVdir + '/' + infile), file=sys.stderr)
    samples = []
    lines = open(args.GCNVdir + '/' + infile, "r").read().splitlines()
    for line in lines:
        if line.startswith('#CHROM'):
            (a, b, c, d, e, f, g, h, i, *samples) = line.split('\t')
            continue
        if line.startswith('#'):
            continue
        (CNV_chr, CNV_start, CNV_id, CNV_ref, CNV_alt, CNV_qual, CNV_filt, CNV_info, CNV_format, *CNV_calls) = line.split('\t')
        m = re.match("END=(\d+)", CNV_info)
        if m:
            CNV_stop = m.group(1)
        else:
            print("Could not get END position from INFO field in %s:\n%s\n" % (infile, line), file=sys.stderr)
            sys.exit(1)
        CNV_id = 'CNV:' + CNV_chr + ':' + CNV_start + '-' + CNV_stop
        CNV_chr = CNV_chr.replace("chr", '')
        try:
            CNV_start = int(CNV_start)
        except ValueError:
            print("Start value not numeric in GCNV file (%s): %s" % (infile, CNV_start))
            sys.exit(1)
        try:
            CNV_stop = int(CNV_stop)
        except ValueError:
            print("Stop value not numeric in GCNV file (%s): %s" % (infile, CNV_stop))
            sys.exit(1)

        matching_genes = []
        for gene in genes_by_chr[CNV_chr]:
            (gene_start, gene_stop, ENS, SYM) = gene
            if ((CNV_start >= (gene_start - args.Padding) and CNV_start <= (gene_stop + args.Padding)) or
                (CNV_stop  >= (gene_start - args.Padding) and CNV_stop  <= (gene_stop + args.Padding)) or
                (CNV_start <  (gene_start - args.Padding) and CNV_stop  >  (gene_stop + args.Padding))):
                    matching_genes.append(( ENS, SYM ))
            
        ###
        ### Now process the individual calls
        ###
        fields = CNV_format.split(':')
        for sample, call in zip(samples, CNV_calls):
            callvals = {}
            values = call.split(':')
            for field, value in zip(fields, values):
                callvals[field] = value
        
            GT = int(callvals['GT'])
            CN = int(callvals['CN'])
            NP = int(callvals['NP'])
            QA = int(callvals['QA'])
            QS = int(callvals['QS'])
             
            ## skip it if its 'REF'
            if GT == 0:
                continue

            for gene in matching_genes:
                (ENS, SYM) = gene
                print ("%s, %s, %s, %d, %d, %s, %s, %d, %d, %d, %d" % \
                  (sample, CNV_id, CNV_chr, CNV_start, CNV_stop, ENS, SYM, CN, NP, QA, QS))

print("All Done.", file=sys.stderr)
