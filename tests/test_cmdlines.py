import os
import glob
import sys
import subprocess
import logging
import pytest

### We're here to test the OGI/GEDi Bindings and Gene Models for IRDs
Gene_Model =  'GEDI.20200210.csv'
Phenotype  =  'GEDIR'
Bindings   =  'MBC1_v1'


###
### Header only output
###
def test_header(script_runner):
    ret = script_runner.run('AnalyzeCase.py', '-H', 'tests/stub.cache', '-', 'R', '-')
    assert ret.success
    assert ret.stdout == 'Score,Gene,Chr,POS,REF,ALT,GQ,HH,EX_AF,GN_AF,COHORT_AC,Consequence,HGVSc,HGVSp,CADD,HGMD,ClinVar,Reg,Mot,LoFTEE\n'
    assert ret.stderr == ''

###
### 'Machine-friendly' output header
###
def test_m_header(script_runner):
    ret = script_runner.run('AnalyzeCase.py', '-m', '-H', 'tests/stub.cache', '-', 'R', '-')
    assert ret.success
    assert ret.stdout == 'SampFam,Binding,Model,Rank,Score,Gene,Chr,POS,REF,ALT,GQ,HH,EX_AF,GN_AF,COHORT_AC,Consequence,HGVSc,HGVSp,CADD,HGMD,ClinVar,Reg,Mot,LoFTEE\n'
    assert ret.stderr == ''

###
### Test some canned families - check for recessives and dominants that we have saved
###
family_dir_list = glob.glob('tests/rec_families/*')
@pytest.mark.parametrize("family_dir", family_dir_list)
def test_recessive_family(script_runner, family_dir, tmpdir):
     do_one_family_model(script_runner, family_dir, 'R', tmpdir)

family_dir_list = glob.glob('tests/dom_families/*')
@pytest.mark.parametrize("family_dir", family_dir_list)
def test_dominant_family(script_runner, family_dir, tmpdir):
     do_one_family_model(script_runner, family_dir, 'D', tmpdir)

###
### Either of the above sets gets here for a single family and disease model
###
def do_one_family_model(script_runner, family_dir, mode, tmpdir):
    ### The input and expected output of this test
    family_vcf = family_dir + "/family.vcf"
    family_ped = family_dir + "/family.ped"
    family_out = family_dir + "/family.out"
    expected_output = open(family_out).read()

    family = os.path.basename(family_dir)
    cache = tmpdir / (family + ".cache")
    do_cache_build(script_runner, family_vcf, cache)
    do_AnalyzeCase(script_runner, cache, family_ped, mode, family, expected_output)

###
### Test some canned samples, one at a time: Build the cache, and then the combined results and confirm they match what's on file
###
sample_dir_list = glob.glob('tests/probandonly/*')
@pytest.mark.parametrize("sample_dir", sample_dir_list)
def test_one_proband(script_runner, sample_dir, tmpdir):

    ### The input and expected output of this test
    sample_vcf = sample_dir + "/sample.vcf"
    sample_out = sample_dir + "/sample.csv"
    expected_output = open(sample_out).read()

    sample = os.path.basename(sample_dir)
    cache = tmpdir / (sample + ".cache")
    do_cache_build(script_runner, sample_vcf, cache)
    do_combined_file(script_runner, cache, expected_output)

###
### Build a cache
###
def do_cache_build(script_runner, sample_vcf, cache):
    ret = script_runner.run('BuildOneCache.py', Gene_Model, Phenotype, Bindings, sample_vcf, cache)

    # did the process exit cleanly? 
    assert ret.success

    # make sure we recognize all the stderr messages
    se_rows = ret.stdout.split('\n')
    se_rows = list(filter((lambda x: ('Dumping' not in x 
                                  and 'rows read' not in x
                                  and 'rows written' not in x
                                  and 'rows were skipped' not in x
                                  and ('Ignoring unrecognized disease model' not in x and x != '')
                                  and x != ''
                          )
                         ), se_rows))
    # there should be nothing left (that is unrecognized)
    assert len(se_rows) == 0

    # make sure we recognize all the stdout messages (there should be none!)
    assert(len(ret.stdout)) == 0

    # and do we actually have a cache with content?
    assert os.stat(cache).st_size > 0


###
### Build a single sample combined file
### Note that this call to CreateCombinedAnalysis.py then calls AnalyzeCohort.py and AnalyzeCase.py
###
def do_combined_file(script_runner, cache, expected_output):
    ret = script_runner.run('CreateCombinedAnalysis.py', cache, '-', '-')

    # did the process exit cleanly? 
    assert ret.success

    # make sure we recognize all the stderr messages
    se_rows = ret.stderr.split('\n')
    se_rows = list(filter((lambda x: ('Found '     not in x 
                                  and 'Analyzing ' not in x
                                  and 'All Done.'  not in x
                                  and x != ''
                          ) ), se_rows))
    # there should be nothing left (that is unrecognized)
    assert len(se_rows) == 0

    # Now let's compare the output generated to the expected
    assert ret.stdout == expected_output

###
### Do a family-level recessive analysis
### will use silent mode:  NOT testing all the info messages
###
def do_AnalyzeCase(script_runner, cache, ped, mode, family, expected_output):
    ret = script_runner.run('AnalyzeCase.py', '-s', str(cache), ped, mode, family)
    

    # did the process exit cleanly? 
    assert ret.success

    # make sure we recognize all the stderr messages
    se_rows = ret.stderr.split('\n')
    se_rows = list(filter((lambda x: (x != '') ), se_rows))
    # there should be nothing left (that is unrecognized)
    assert len(se_rows) == 0

    # Now let's compare the output generated to the expected
    assert ret.stdout == expected_output

###
### Do a targeted test of the CNV HEMI logic - in a proband
###
sample_dir_list = glob.glob('tests/pro_with_CNV/*')
@pytest.mark.parametrize("sample_dir", sample_dir_list)
def test_CNV_HEMI_proband(script_runner, sample_dir):

    ### The input and expected output of this test
    sample_cache = sample_dir + "/sample.cache"
    sample_CNV = sample_dir + "/sample.CNV"
    sample_out = sample_dir + "/sample.csv"
    expected_output = open(sample_out).read()

    ret = script_runner.run('AnalyzeCase.py', '-C', sample_CNV, '-s', '-m', sample_cache, '-', 'R', '-')

    # did the process exit cleanly? 
    assert ret.success

    # make sure we recognize all the stderr messages
    se_rows = ret.stderr.split('\n')
    se_rows = list(filter((lambda x: (x != '') ), se_rows))
    # there should be nothing left (that is unrecognized)
    assert len(se_rows) == 0

    # Now let's compare the output generated to the expected
    assert ret.stdout == expected_output

###
### Do a targeted test of the CNV HEMI logic - in a parent of recessive case
###
family_dir_list = glob.glob('tests/fam_with_CNV/*')
@pytest.mark.parametrize("family_dir", family_dir_list)
def test_CNV_HEMI_family(script_runner, family_dir):

    ### The input and expected output of this test
    family_cache = family_dir + "/family.cache"
    family_ped = family_dir + "/family.ped"
    family_CNV = family_dir + "/family.CNV"
    family_out = family_dir + "/family.out"
    expected_output = open(family_out).read()

    ret = script_runner.run('AnalyzeCase.py', '-C', family_CNV, '-s', '-m', family_cache, family_ped, 'R', '-')

    # did the process exit cleanly? 
    assert ret.success

    # make sure we recognize all the stderr messages
    se_rows = ret.stderr.split('\n')
    se_rows = list(filter((lambda x: (x != '') ), se_rows))
    # there should be nothing left (that is unrecognized)
    assert len(se_rows) == 0

    # Now let's compare the output generated to the expected
    assert ret.stdout == expected_output

