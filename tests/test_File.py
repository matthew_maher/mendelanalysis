#!/usr/bin/env python3

import sys
from time import gmtime, strftime
import MendelFile as MF
import pytest

###
### Various tests of the Gene_Model_Processing routine
###

###
### A sample mini gene-model file to test with
###
GMcontent = [ 
        "Ensembl,Symbol,Disease Models,AR Pop Freq,AD Pop Freq,XL Pop Freq,HI-Dom,Confidence,GEDIR,GEDIO,GEDIS",
        "defaults,,,0.002,0.000010,0.002,,,,,",
        "*,,AR;AD;XL,0.001,0.000010,0.001,,0.5,,,",
        "ENSG00000141385,AFG3L2,AR;AD,,,,,,,O,",             ## Make sure different phenotypes excluded ('O')
        "ENSG00000198691,ABCA4,AR,0.004,0.004000,,,,R,,",
        "ENSG00000091262,ABCC6,AR;AD,,,,0,,R,,",
        "ENSG00000100997,ABHD12,AR;BAD,,,,,,R,,",
        "ENSG00000107897,ACBD5,AR,,,,,,R,,",
        "ENSG00000102001,CACNA1F,XL,,,,No,,R,,",
        "ENSG00000105618,PRPF31,AD,,,,Yes,,R,,",
    ]

GM_expected = {
        'ENSG00000198691': { 'Symbol': 'ABCA4',   'Confidence': 1.0, 'HI-Dom': False, 'R': -2.3979400086720375, '1': -2.3979400086720375},
        'ENSG00000091262': { 'Symbol': 'ABCC6',   'Confidence': 1.0, 'HI-Dom': False, 'R': -2.6989700043360187, '1': -2.6989700043360187, 'D': -5.0},
        'ENSG00000100997': { 'Symbol': 'ABHD12',  'Confidence': 1.0, 'HI-Dom': False, 'R': -2.6989700043360187, '1': -2.6989700043360187},
        'ENSG00000107897': { 'Symbol': 'ACBD5',   'Confidence': 1.0, 'HI-Dom': False, 'R': -2.6989700043360187, '1': -2.6989700043360187},
        'ENSG00000102001': { 'Symbol': 'CACNA1F', 'Confidence': 1.0, 'HI-Dom': False, 'X': -2.6989700043360187},
        'ENSG00000105618': { 'Symbol': 'PRPF31',  'Confidence': 1.0, 'HI-Dom': True,  'D': -5.0, },
       }

### Let's test a basic/good case
def test_Process_Gene_Model_Content_basic(script_runner):
    GMC = GMcontent.copy()
    GM = MF.Process_Gene_Model_Content(GMC, 'GEDIR', True)

    ### This confirms that:
    ###     Required fields present
    ###     the frequency values get populated from the default and converted to log10
    ###     Rows unrelated to phenotype are dropped
    assert GM == GM_expected


### Check that an invalid phenotype raises exception
def test_Process_Gene_Model_Content_bad_phenotype(script_runner):
    GMC = GMcontent.copy()
    with pytest.raises(SystemExit) as e_info:
        GM = MF.Process_Gene_Model_Content(GMC, 'CANCER', True)
    assert e_info.type == SystemExit

### Check that required column check is working
def test_Process_Gene_Model_Content_missing_required_field(script_runner):
    GMC = GMcontent.copy()
    GMC[0] = GMC[0].replace('Symbol', 'Nonsense')
    with pytest.raises(SystemExit) as e_info:
        GM = MF.Process_Gene_Model_Content(GMC, 'GEDIR', True)
    assert e_info.type == SystemExit

### Check that non-numeric pop freq creates a problem
def test_Process_Gene_Model_Content_invalid_popfreq(script_runner):
    GMC = GMcontent.copy()
    GMC[4] = GMC[4].replace(',0.004,', ',Nonsense,')   ## mess up the row for ABCA4 - note the magic '4' based on the fixed input above
    with pytest.raises(SystemExit) as e_info:
        GM = MF.Process_Gene_Model_Content(GMC, 'GEDIR', True)
    assert e_info.type == SystemExit

### Check that invalid HI-dom value creates a problem
def test_Process_Gene_Model_Content_invalid_HIdom(script_runner):
    GMC = GMcontent.copy()
    GMC[5] = GMC[5].replace(',0,', ',X,')   ## mess up one of the HI-dom values- note the magic '5' based on the fixed input above
    with pytest.raises(SystemExit) as e_info:
        GM = MF.Process_Gene_Model_Content(GMC, 'GEDIR', True)
    assert e_info.type == SystemExit

### Check that invalid HI-dom value creates a problem
def test_Process_Gene_Model_Content_no_HIdom(script_runner):
    GMC = GMcontent.copy()
    ###Remove the HI-Dom column from all rows
    new_GMC = []
    for row in GMC:
        vals = row.split(',')
        del vals[6]
        new_GMC.append(','.join(vals))
    GM = MF.Process_Gene_Model_Content(new_GMC, 'GEDIR', True)
    ### Also make a modifled expected output that turns off the one True value in our original example
    GM_expected2 = GM_expected.copy()
    GM_expected2['ENSG00000105618']['HI-Dom'] = False

    assert GM == GM_expected2

### Check that empty file fails
def test_Process_Gene_Model_Content_empty(script_runner):
    with pytest.raises(SystemExit) as e_info:
        GM = MF.Process_Gene_Model_Content([], 'GEDIR', True)
    assert e_info.type == SystemExit

### Check that gene symbol -> ID translation works
def test_check_and_translate_gene(script_runner):
    GMC = GMcontent.copy()
    GM = MF.Process_Gene_Model_Content(GMC, 'GEDIR', True)
    ABCA4 = MF.check_and_translate_gene('ABCA4', GM, 'Just Testing')
    assert ABCA4 == 'ENSG00000198691'

### Check that gene symbol -> ID translation properly fails
def test_check_and_translate_gene_badgene(script_runner):
    GMC = GMcontent.copy()
    GM = MF.Process_Gene_Model_Content(GMC, 'GEDIR', True)
    with pytest.raises(SystemExit) as e_info:
        ID = MF.check_and_translate_gene('BADGENE', GM, 'Just Testing')
    assert e_info.type == SystemExit

##################################################################################

### Various tests of the Pedigree FIle processing

###
### A sample mini gene-model file to test with
###
PEDcontent = [ 
    'FAM1,FAM1_PERS1,,,Male,unaffected',
    'FAM1,FAM1_PERS2,,,Female,1',
    'FAM1,FAM1_PERS3,FAM1_PERS1,FAM1_PERS2,1,affected',
    'FAM1,FAM1_PERS4,FAM1_PERS1,FAM1_PERS2,Female,2',
    'FAM1,FAM1_PERS5,FAM1_PERS1,FAM1_PERS2,Female,unaffected',
    'FAM2,FAM2_PERS1,,,male,unaffected',
    'FAM2,FAM2_PERS2,,,female,unaffected',
]

### Let's test a the basic case
def test_process_Pedigree_basic(script_runner):
    PEDC = PEDcontent.copy()
    PED = MF.process_Pedigree(PEDC)

    ### This confirms that:
    ###     Proper structure built
    ###     Numeric codes accepted and translated
    ###       1=unaffected, 2=affected
    ###       1=male,2=female
    ###     case differences handled
    ###     Proband determined and saved
    assert PED == {
        'FAM1': {
             'PROBAND': 'FAM1_PERS3', 
             'MEMBER': {
                  'FAM1_PERS1': {'Father': '', 'Mother': '', 'Gender': 'male', 'Status': 'unaffected'}, 
                  'FAM1_PERS2': {'Father': '', 'Mother': '', 'Gender': 'female', 'Status': 'unaffected'}, 
                  'FAM1_PERS3': {'Father': 'FAM1_PERS1', 'Mother': 'FAM1_PERS2', 'Gender': 'male', 'Status': 'affected'}, 
                  'FAM1_PERS4': {'Father': 'FAM1_PERS1', 'Mother': 'FAM1_PERS2', 'Gender': 'female', 'Status': 'affected'}, 
                  'FAM1_PERS5': {'Father': 'FAM1_PERS1', 'Mother': 'FAM1_PERS2', 'Gender': 'female', 'Status': 'unaffected'}, 
              }
        },
        'FAM2': {
             'MEMBER': { 
                  'FAM2_PERS1': {'Father': '', 'Mother': '', 'Gender': 'male', 'Status': 'unaffected'}, 
                  'FAM2_PERS2': {'Father': '', 'Mother': '', 'Gender': 'female', 'Status': 'unaffected'}
              }
        }
    }

### Check that an invalid status fails
def test_process_Pedigree_bad_status(script_runner):
    PEDC = PEDcontent.copy()
    PEDC[1] = PEDC[1].replace('1', '3')   ## mess up status of second row- note the magic '[1]' based on the fixed input above
    with pytest.raises(SystemExit) as e_info:
        PED = MF.process_Pedigree(PEDC)
    assert e_info.type == SystemExit

### Check that an invalid status fails
def test_process_Pedigree_bad_gender(script_runner):
    PEDC = PEDcontent.copy()
    inFAM = 'FAM1'
    PEDC[1] = PEDC[1].replace('Female', 'Cat')   ## mess up gender of second row- note the magic '[1]' based on the fixed input above
    with pytest.raises(SystemExit) as e_info:
        PED = MF.process_Pedigree(PEDC)
    assert e_info.type == SystemExit

### test empty pedigree
def test_process_Pedigree_empty(script_runner):
    inFAM = 'FAM1'
    with pytest.raises(SystemExit) as e_info:
        PED = MF.process_Pedigree([])
    assert e_info.type == SystemExit

### test no proband for family of interest
def test_process_Pedigree_no_proband(script_runner):
    PEDC = PEDcontent.copy()
    inFAM = 'FAM2'
    PED = MF.process_Pedigree(PEDC)
    with pytest.raises(SystemExit) as e_info:
        MF.check_and_clean_Pedigree_Family(PED, inFAM, 'R')
    assert e_info.type == SystemExit

### test family not found
def test_process_Pedigree_missing_family(script_runner):
    PEDC = PEDcontent.copy()
    inFAM = 'FAMX'
    PED = MF.process_Pedigree(PEDC)
    with pytest.raises(SystemExit) as e_info:
        MF.check_and_clean_Pedigree_Family(PED, inFAM, 'R')
    assert e_info.type == SystemExit

### test bad recessive ped
def test_process_Pedigree_bad_rec_family(script_runner):
    PEDC = PEDcontent.copy()
    inFAM = 'FAM1'
    PED = MF.process_Pedigree(PEDC)
    PED['FAM1']['MEMBER']['FAM1_PERS4']['Father'] =  'SOMEONEELSE'
    with pytest.raises(SystemExit) as e_info:
        MF.check_and_clean_Pedigree_Family(PED, inFAM, 'R')
    assert e_info.type == SystemExit

### test the skeltal pedigree creation
def test_process_Pedigree_skeleton(script_runner):
    PEDC = [ 'PROBAND,PROBAND,,,Unknown,affected' ]
    PED = MF.process_Pedigree(PEDC)
    skel_ped = MF.skeleton_Pedigree('PROBAND')
    assert PED == skel_ped

##################################################################################

### Cache Headers

def test_cacheHeaders(script_runner):
    SourceVCF = 'MY.vcf'
    GeneModel = 'MyMOdel'
    AnnotBinding = 'MyBinding'
    Phenotype = 'MyPhenotype'
    BuiltWhen = gmtime()
    Samples = ['SAMPLEA', 'SAMPLEB' ]

    CH = MF.parseCacheHeaders(MF.formatCacheHeaders(SourceVCF, GeneModel,
                          AnnotBinding, Phenotype, BuiltWhen, Samples))
   #assert CH.__dict__ == 1
    assert CH.SourceVCF == SourceVCF
    assert CH.GeneModel == GeneModel
    assert CH.AnnotBinding == AnnotBinding
    assert CH.Phenotype == Phenotype
    assert CH.BuiltWhen == strftime("%Y-%m-%d %H:%M:%S", BuiltWhen)
    assert CH.Samples == '\t'.join(Samples)
