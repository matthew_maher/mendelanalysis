#!/usr/bin/env python3

import sys
import MendelOut as MO

class dummyobj:
    pass

args = dummyobj()
args.Family = 'FAM1'
args.DiseaseModel = 'R'
score = '20.0'
datavar = { 'SYMBOL': 'ABCA4', 'CHROM': '1', 'POS': '1234', 'REF': 'A', 'ALT': 'C', 'MYCOL': 'DATA', 'GQ': '99' }
AnnotBinding = dummyobj()
AnnotBinding.outputColumns = [ 'MYCOL' ]
AnnotBinding.Name = 'TEST'

### Set up for testing the full output routine
data = {}
data['VAR1'] = {}
data['VAR1']['SYMBOL'] = 'ABCA4'
data['VAR1']['CHROM'] = '1'
data['VAR1']['POS'] = '1234'
data['VAR1']['REF'] = 'A'
data['VAR1']['ALT'] = 'C'
data['VAR1']['MYCOL'] = 'DATA'
data['VAR1']['GQ'] = '99'
data['VAR1']['AFF_ALLS'] = 'SAMPLE1'
D_rows_output = [
  ("2-RHO", 10, [10], data, ['VAR1'] ),
  ("1-XYZ", 10, [10], data, ['VAR1'] ),
]
R_rows_output = [
  ("2-RHO", 10, [10,0], data, ['VAR1', 'VAR1'] ),
  ("1-XYZ", 10, [10,0], data, ['VAR1', 'VAR1'] ),
]

### Make sure that headers and data logic return the same # of values
def test_sectionA(script_runner):
    hdrs = MO.sectionA(True,  None, 'Binding', 1)
    assert len(hdrs) == 4
    data = MO.sectionA(False, args, 'Binding', 1)
    assert len(data) == 4

### Make sure that headers and data logic return the same # of values
def test_sectionB(script_runner):
    hdrs = MO.sectionB(True,  None, None)
    assert len(hdrs) == 3
    data = MO.sectionB(False, score, datavar)
    assert len(data) == 3

### Make sure that headers and data logic return the same # of values
def test_sectionC(script_runner):
    hdrs = MO.sectionC(True, None)
    assert len(hdrs) == 4
    data = MO.sectionC(False, datavar)
    assert len(data) == 4

### Make sure that headers and data logic return the same # of values
def test_sectionD(script_runner):
    hdrs = MO.sectionD(True, None,  AnnotBinding)
    assert len(hdrs) == len(AnnotBinding.outputColumns)
    data = MO.sectionD(False, datavar, AnnotBinding)
    assert len(data) == len(AnnotBinding.outputColumns)


### Test various combinations of headers
### expected length is the sum of the expected header lengths above (+1 for HOM)
def test_m_headers(script_runner):
    args.m = True
    args.w = False
    hdrs = MO.headers(args, AnnotBinding)
    assert len(hdrs) == 4 + 3 + 4 + len(AnnotBinding.outputColumns) + 1

def test_m_w_headers(script_runner):
    args.m = True
    args.w = True
    hdrs = MO.headers(args, AnnotBinding)
    assert len(hdrs) == 4 + 3 + (4 + len(AnnotBinding.outputColumns) + 1) * 2

def test_headers(script_runner):
    args.m = False  ## this should drop section A
    args.w = False
    hdrs = MO.headers(args, AnnotBinding)
    assert len(hdrs) == 0 + 3 + 4  + len(AnnotBinding.outputColumns) + 1



### Test various combinations of final output
### We really just want to ensure that headers and data rows have same legnth

def test_m_D_output(script_runner):
    args.DiseaseModel = 'D'
    args.s = False
    args.m = True
    args.w = False
    out = list(MO.final_output(D_rows_output, AnnotBinding, args, None))
    outlens = [ len(r) for r in out ]
    ### Check that all the rows are the same length.  If not, the header is probably different
    assert len(set(outlens)) == 1

def test_w_R_output(script_runner):
    args.DiseaseModel = 'R'
    args.m = False
    args.w = True
    out = list(MO.final_output(R_rows_output, AnnotBinding, args, None))
    outlens = [ len(r) for r in out ]
    ### Check that all the rows are the same length.  If not, the header is probably different
    assert len(set(outlens)) == 1

def test_R_output(script_runner):
    args.DiseaseModel = 'R'
    args.m = False
    args.w = False
    out = list(MO.final_output(R_rows_output, AnnotBinding, args, None))
    ## capture the data rows
    outlens = [ len(r) for r in out if len(r) != 1 ]
    ## capture the blank rows that appear in R non-w mode
    blanks  = [ len(r) for r in out if len(r) == 1 ]

    ### check that we have blank lines
    assert len(set(blanks)) == 1
    ### Check that all the data rows are the same length.  If not, the header is probably different
    assert len(set(outlens)) == 1
