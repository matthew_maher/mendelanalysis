#!/usr/bin/env python3

import sys
import MendelDMs as DM

###
### Test the three most basic routines that summerize a single genotype
###
def test_ishom(script_runner):
    assert     DM.ishom(['1', '1'])
    assert not DM.ishom(['1', '0'])
    assert not DM.ishom(['0', '1'])
    assert not DM.ishom(['0', '0'])

def test_ishet(script_runner):
    assert not DM.ishet(['1', '1'])
    assert     DM.ishet(['1', '0'])
    assert     DM.ishet(['0', '1'])
    assert not DM.ishet(['0', '0'])

def test_isalh(script_runner):    ## At least het
    assert     DM.isalh(['1', '1'])
    assert     DM.isalh(['1', '0'])
    assert     DM.isalh(['0', '1'])
    assert not DM.isalh(['0', '0'])

def test_dosage(script_runner):    ## How many visible alleles?
    assert     DM.dosage(['1', '1']) == 2
    assert     DM.dosage(['1', '0']) == 1
    assert     DM.dosage(['0', '1']) == 1
    assert     DM.dosage(['0', '0']) == 0
    assert     DM.dosage(['1', '.']) == 1
    assert     DM.dosage(['.', '0']) == 0
    assert     DM.dosage(['.', '1']) == 1
    assert     DM.dosage(['0', '.']) == 0

def test_max_dosage(script_runner):    ## how many possible alleles?
    assert     DM.max_dosage(['1', '1']) == 2
    assert     DM.max_dosage(['1', '0']) == 1
    assert     DM.max_dosage(['0', '1']) == 1
    assert     DM.max_dosage(['0', '0']) == 0
    assert     DM.max_dosage(['1', '.']) == 2
    assert     DM.max_dosage(['.', '0']) == 1
    assert     DM.max_dosage(['.', '1']) == 2
    assert     DM.max_dosage(['0', '.']) == 1

def test_is_cis(script_runner):    
    ### A HOM call for the sample in question
    assert not DM.is_cis({'A'},     {'A'}, {'A'})
    assert     DM.is_cis({'A'},     {'A'}, set()) ## denovo?
    assert     DM.is_cis({'A'},     set(), {'A'}) ## denovo?
    assert     DM.is_cis({'A'},     set(), set()) ## Double denovo?
    ### A Comp-Het call for the sample in question
    assert not DM.is_cis({'A','B'}, {'A'}, {'B'}) ## Def Trans
    assert     DM.is_cis({'A','B'}, {'A','B'}, set()) ## Def Cis
    assert     DM.is_cis({'A','B'}, set(), {'A','B'}) ## Def Cis
    assert     DM.is_cis({'A','B'}, {'A'}, set()) ## Denovo?
    assert     DM.is_cis({'A','B'}, set(), {'B'}) ## Denovo?
    assert     DM.is_cis({'A','B'}, set(), set()) ## Double denovo?

###
### Test routine that summarizes across all the affecteds
###
def test_summarize_genotypes(script_runner):

    ### Set up a basic starting test case, which we will incrementally change
    ### and retest.  Each of the output flags is both T or F at least once
    callinfo = {
        'SAMPLEA': ['1', '1'],
        'SAMPLEB': ['1', '1'],
        'SAMPLEC': ['1', '1'],
        'SAMPLED': ['0', '0'],
        'SAMPLEE': ['1', '1'],
        'SAMPLEF': ['1', '0'],
    }
    Proband = None
    affecteds = [ 'SAMPLEA', 'SAMPLEB', 'SAMPLEC' ]
    unaffecteds = []
    FirstOnly = False

    assert DM.summarize_genotypes(callinfo, affecteds, unaffecteds, FirstOnly, Proband) == {
        'ALL_AFF_HET': False,
        'ALL_AFF_HOM': True,
        'ALL_AFF_ALH': True,
        'ANY_AFF_ALH': True,
        '1ST_AFF_HET': False,
        'ANY_UNA_ALH': False,
        'ANY_UNA_HOM': False,
    }

    callinfo['SAMPLEB'] = ['1', '0']
    assert DM.summarize_genotypes(callinfo, affecteds, unaffecteds, FirstOnly, Proband) == {
        'ALL_AFF_HET': False,
        'ALL_AFF_HOM': False,
        'ALL_AFF_ALH': True,
        'ANY_AFF_ALH': True,
        '1ST_AFF_HET': False,
        'ANY_UNA_ALH': False,
        'ANY_UNA_HOM': False,
    }

    FirstOnly = True
    Proband = 'SAMPLEA'
    assert DM.summarize_genotypes(callinfo, affecteds, unaffecteds, FirstOnly, Proband) == {
        'ALL_AFF_HET': False,
        'ALL_AFF_HOM': False,
        'ALL_AFF_ALH': True,
        'ANY_AFF_ALH': True,
        '1ST_AFF_HET': False,
        'ANY_UNA_ALH': False,
        'ANY_UNA_HOM': False,
    }

    callinfo['SAMPLEC'] = ['0', '0']
    assert DM.summarize_genotypes(callinfo, affecteds, unaffecteds, FirstOnly, Proband) == {
        'ALL_AFF_HET': False,
        'ALL_AFF_HOM': False,
        'ALL_AFF_ALH': False,
        'ANY_AFF_ALH': True,
        '1ST_AFF_HET': False,
        'ANY_UNA_ALH': False,
        'ANY_UNA_HOM': False,
    }

    callinfo['SAMPLEA'] = ['1', '0']
    callinfo['SAMPLEC'] = ['1', '0']
    assert DM.summarize_genotypes(callinfo, affecteds, unaffecteds, FirstOnly, Proband) == {
        'ALL_AFF_HET': True,
        'ALL_AFF_HOM': False,
        'ALL_AFF_ALH': True,
        'ANY_AFF_ALH': True,
        '1ST_AFF_HET': True,
        'ANY_UNA_ALH': False,
        'ANY_UNA_HOM': False,
    }

####################   Recessive ###########################

###
### Helper Routine that we'll use for testing recessive cases
###
def rec_update_and_run(basecase, GT_mods, new_solution):

    ### Get the initial starting pedigree / callset 
    (Pedigree, Family, affecteds, unaffecteds, calls) = basecase()

    ### Apply incremental mods to the callset
    for (var, samp, GT) in GT_mods:
        calls[var][samp] = GT

    ### Perform the required summarization
    for var in calls:
        calls[var]['SUMMARY'] = DM.summarize_genotypes(calls[var], affecteds, unaffecteds, False, None)

    ### Run the actual recessive analyzer and recheck the answer
    solutions = DM.find_recessive(calls, Pedigree, Family, affecteds, unaffecteds)
    assert set(solutions) == new_solution


###
### Family of 3 children, two affected (S1, S2)
###
def base_recessive_family():
    Family = 'FAM1'
    Pedigree = {
        'FAM1':  {
            'PROBAND': 'S1',
            'MEMBER': {
                    'S1': { 'Mother': 'S4', 'Father': 'S5', 'Gender':  'male', 'Status':  'affected' },
                    'S2': { 'Mother': 'S4', 'Father': 'S5', 'Gender':'female', 'Status':'unaffected' },
                    'S3': { 'Mother': 'S4', 'Father': 'S5', 'Gender':  'male', 'Status':  'affected' },
                    'S4': { 'Mother': '',   'Father': '',   'Gender':'female', 'Status':'unaffected' },
                    'S5': { 'Mother': '',   'Father': '',   'Gender':  'male', 'Status':'unaffected' },
                      }
                 },
        'FAM2': 'Nonsense that should not break anything'
               }

    (Proband, affecteds, unaffecteds) = DM.find_aff_unaff(Pedigree, Family)
    assert affecteds == [ 'S1', 'S3' ]

    ###
    ### Initial set up: V1 and V3 are comp-het pair solution
    ###
    calls = {
        'V1': {
            'S1': ['1','0'],
            'S2': ['1','0'],
            'S3': ['1','0'],
            'S4': ['0','0'],
            'S5': ['1','0'],
              },
        'V2': {
            'S1': ['1','0'],
            'S2': ['1','0'],
            'S3': ['1','0'],
            'S4': ['0','0'],
            'S5': ['0','0'],
              },
        'V3': {
            'S1': ['1','0'],
            'S2': ['0','0'],
            'S3': ['1','0'],
            'S4': ['1','0'],
            'S5': ['0','0'],
              },
        'V4': {
            'S1': ['1','1'],
            'S2': ['1','0'],
            'S3': ['1','0'],
            'S4': ['1','0'],
            'S5': ['0','1'],
              },
        'V5': {
            'S1': ['1','0'],
            'S2': ['1','0'],
            'S3': ['1','0'],
            'S4': ['1','0'],
            'S5': ['1','0'],
              },
            }

    return (Pedigree, Family, affecteds, unaffecteds, calls) 

### Test the base case unchanged
def test_recessive_base(script_runner):
    rec_update_and_run(base_recessive_family, [], { ('V1', 'V3') })

### Try adding a HOM solution
def test_recessive_addHOM(script_runner):
    rec_update_and_run(base_recessive_family, [('V4', 'S3', ['1','1'])],  { ('V1', 'V3'), ('V4',) })

### Give the UNAFFECTED sib the original comphet pair - should disallow it
def test_recessive_unaffected_also_matches(script_runner):
    rec_update_and_run(base_recessive_family, [('V3', 'S2', ['1','0'])],  set())

### Give the UNAFFECTED sib HOM for V1 - should also disallow base solution
def test_recessive_HOM_unaffected(script_runner):
    rec_update_and_run(base_recessive_family, [('V1', 'S2', ['1','1'])],  set())

### Undo that last HOM and make one of the parents HOM for V3 - should again disallow original solution
def test_recessive_HOM_parent(script_runner):
    rec_update_and_run(base_recessive_family, [('V3', 'S4', ['1','1'])],  set())

#################  Dominant  #############################################

###
### Helper Routine that we'll use for testing dominant cases
###
def dom_update_and_run(basecase, GT_mods, new_solution):

    ### Get the initial starting pedigree / callset 
    (Pedigree, Family, affecteds, unaffecteds, calls) = basecase()

    ### Apply incremental mods to the callset
    for (var, samp, GT) in GT_mods:
        calls[var][samp] = GT

    ### Perform the required summarization
    for var in calls:
        calls[var]['SUMMARY'] = DM.summarize_genotypes(calls[var], affecteds, unaffecteds, False, None)

    ### Run the actual recessive analyzer and recheck the answer
    solutions = DM.find_dominants(calls)
    assert set(solutions) == new_solution

###
### Multigeneration Family 
###
def base_dominant_family():
    Family = 'FAM1'
    Pedigree = {
        'FAM1':  {
            'PROBAND': 'S1',
            'MEMBER': {
                    'S1': { 'Mother': 'S4', 'Father': 'S5', 'Gender':  'male', 'Status':  'affected' },
                    'S2': { 'Mother': 'S4', 'Father': 'S5', 'Gender':'female', 'Status':'unaffected' },
                    'S3': { 'Mother': 'S4', 'Father': 'S5', 'Gender':  'male', 'Status':  'affected' },
                    'S4': { 'Mother': 'S6', 'Father': 'S7', 'Gender':'female', 'Status':  'affected' },
                    'S5': { 'Mother': '',   'Father': '',   'Gender':  'male', 'Status':'unaffected' },
                    'S6': { 'Mother': '',   'Father': '',   'Gender':  'male', 'Status':'unaffected' },
                    'S7': { 'Mother': '',   'Father': '',   'Gender':  'male', 'Status':  'affected' },
                      }
                 },
        'FAM2': 'Nonsense that should not break anything'
               }

    (Proband, affecteds, unaffecteds) = DM.find_aff_unaff(Pedigree, Family)
    assert affecteds == [ 'S1', 'S3', 'S4', 'S7' ]

    ###
    ### Initial set up: V3 is it
    ###
    calls = {
        'V1': {
            'S1': ['1','1'],
            'S2': ['0','0'],
            'S3': ['1','0'],
            'S4': ['0','0'],
            'S5': ['1','0'],
            'S6': ['1','0'],
            'S7': ['1','0'],
              },
        'V2': {
            'S1': ['1','0'],
            'S2': ['1','0'],
            'S3': ['1','1'],
            'S4': ['0','0'],
            'S5': ['1','0'],
            'S6': ['1','1'],
            'S7': ['1','1'],
              },
        'V3': {
            'S1': ['1','0'],
            'S2': ['0','0'],
            'S3': ['0','1'],
            'S4': ['0','1'],
            'S5': ['0','0'],
            'S6': ['0','0'],
            'S7': ['1','0'],
              },
        'V4': {
            'S1': ['1','1'],
            'S2': ['1','0'],
            'S3': ['1','0'],
            'S4': ['1','0'],
            'S5': ['0','0'],
            'S6': ['1','0'],
            'S7': ['1','0'],
              },
        'V5': {
            'S1': ['1','0'],
            'S2': ['0','0'],
            'S3': ['1','0'],
            'S4': ['0','0'],
            'S5': ['0','0'],
            'S6': ['0','0'],
            'S7': ['1','0'],
              },
        'V6': {
            'S1': ['1','1'],
            'S2': ['1','1'],
            'S3': ['1','0'],
            'S4': ['1','0'],
            'S5': ['0','1'],
            'S6': ['1','0'],
            'S7': ['1','0'],
              },
            }
    return (Pedigree, Family, affecteds, unaffecteds, calls) 

### Test the base case unchanged
def test_dominant_base(script_runner):
    dom_update_and_run(base_dominant_family, [], { ('V3',) })

### Take it away from one affected 
def test_dominant_lose_aff(script_runner):
    dom_update_and_run(base_dominant_family, [('V3', 'S1', ['0','0'])], set() )

### Make one affected HOM (it was agreed that is disqualifying as the variant is too unlikely)
def test_dominant_HOM_aff(script_runner):
    dom_update_and_run(base_dominant_family, [('V3', 'S3', ['1','1'])], set() )

### Make one affected HOM (it was agreed that is disqualifying as the variant is too unlikely)
def test_dominant_add_UNaff(script_runner):
    dom_update_and_run(base_dominant_family, [('V3', 'S2', ['1','0'])], set() )
