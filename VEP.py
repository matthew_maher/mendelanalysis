#!/usr/bin/env python3

import re
import MendelBase

### This file contains all the 'consequence' and 'serverity' information from VEP.  

###
### VEP's documentation assigns an 'Impact Severity' to each possible consequence.
### VEP's values appear to be their estimates of the Impact vis a vis Loss-of-Function,
### and thus appear in the 'LoF Sev' column (2nd) below.  
###
### Assessing variants for Gain-of-Function is a completely different ballgame,
### with missense variants apparently being the most interesting.  The 3rd column, 
### Gain-of-Function, is a somewhat arbitrary list (NOT from VEP!) of values reflecting 
### this belief.

### For which 'BIOTYPE's will we actually calculate scores?
biotypes_of_interest = [ 'protein_coding',
                         'processed_transcript'   # PLK1S1+NR2E3 are both this biotype (per VEP)
                       ]

### Symbol and Chromosome are required/primary output fields
### Where are they in our VCF file?
def getSymbol(fields, AB):
    return fields[f'INFO.{AB.ANN_tag}.SYMBOL']

def getGene(fields, AB):
    return fields['INFO'][AB.ANN_tag]['Gene']


###  VEP has a list of Severity Terms - here they ordered from least to most severe
Impact_ORDER = [ 'MODIFIER',
                 'LOW',
                 'MODERATE',
                 'SKIP',   # kludge to simply make 'HIGH' two notches higher than 'MODERATE'
                 'HIGH'
               ]


### All the Sequence Variation Terms and their severity rankings for both LoF and GoF
### The LoF values were taken from VEP website.  GoF values made up in local consultation
Consequences = [
    #  Consequence                             LoF Sev     GoF Sev
    [ 'transcript_ablation',                   'HIGH',     'LOW'      ],
    [ 'splice_acceptor_variant',               'HIGH',     'MODERATE' ],
    [ 'splice_donor_variant',                  'HIGH',     'MODERATE' ],
    [ 'stop_gained',                           'HIGH',     'LOW'      ],
    [ 'frameshift_variant',                    'HIGH',     'LOW'      ],
    [ 'stop_lost',                             'HIGH',     'HIGH'     ],
    [ 'start_lost',                            'HIGH',     'LOW'      ],
    [ 'transcript_amplification',              'HIGH',     'LOW'      ],
    [ 'inframe_insertion',                     'MODERATE', 'MODERATE' ],
    [ 'inframe_deletion',                      'MODERATE', 'MODERATE' ],
    [ 'missense_variant',                      'MODERATE', 'HIGH'     ],
    [ 'protein_altering_variant',              'MODERATE', 'HIGH'     ],
    [ 'splice_region_variant',                 'LOW',      'LOW'      ],
    [ 'incomplete_terminal_codon_variant',     'LOW',      'LOW'      ],
    [ 'stop_retained_variant',                 'LOW',      'LOW'      ],
    [ 'synonymous_variant',                    'LOW',      'LOW'      ],
    [ 'coding_sequence_variant',               'MODIFIER', 'MODERATE' ],
    [ 'mature_miRNA_variant',                  'MODIFIER', 'MODIFIER' ],
    [ '5_prime_UTR_variant',                   'MODIFIER', 'MODIFIER' ],
    [ '3_prime_UTR_variant',                   'MODIFIER', 'MODIFIER' ],
    [ 'non_coding_transcript_exon_variant',    'MODIFIER', 'MODIFIER' ],
    [ 'intron_variant',                        'MODIFIER', 'MODIFIER' ],
    [ 'NMD_transcript_variant',                'MODIFIER', 'MODIFIER' ],
    [ 'non_coding_transcript_variant',         'MODIFIER', 'MODIFIER' ],
    [ 'upstream_gene_variant',                 'MODIFIER', 'MODIFIER' ],
    [ 'downstream_gene_variant',               'MODIFIER', 'MODIFIER' ],
    [ 'TFBS_ablation',                         'MODIFIER', 'MODIFIER' ],
    [ 'TFBS_amplification',                    'MODIFIER', 'MODIFIER' ],
    [ 'TF_binding_site_variant',               'MODIFIER', 'MODIFIER' ],
    [ 'regulatory_region_ablation',            'MODERATE', 'MODIFIER' ],
    [ 'regulatory_region_amplification',       'MODIFIER', 'MODIFIER' ],
    [ 'feature_elongation',                    'MODIFIER', 'MODIFIER' ],
    [ 'regulatory_region_variant',             'MODIFIER', 'MODIFIER' ],
    [ 'feature_truncation',                    'MODIFIER', 'MODIFIER' ],
    [ 'intergenic_variant',                    'MODIFIER', 'MODIFIER' ],
    [ 'start_retained_variant',                'MODIFIER', 'MODIFIER' ],

    # CNV is not a VEP value, but an extra that this app will use
    [ 'CNV',                                   'HIGH',     'MODIFIER' ]   
]

def decodeConsequence(ConsequenceCodes):
    consdesc = [ Consequences[int(x)][0]  for x in ConsequenceCodes.split('-') ]
    return '&'.join(consdesc)

def encodeConsequence(ConsequenceDescs):
    try:
       conscode = [ str(Consequence_Code[x])  for x in ConsequenceDescs.split('&') ]
    except KeyError:
       sys.exit(f"FATAL ERROR: '{ConsequenceDescs}' is not recognized in the internal Consequence Table")
    return '-'.join(conscode)

(Consequence_Code, Impact_Rank) = MendelBase.InitializeConsequenceTable(Consequences, Impact_ORDER)

###
### This hook is called by BuildOneCache or ScoreVCF just when a VCF record is read in.
### This is our chance to capture any record-wide information that we may want later,
### when we are called back for each ANN row.  Note that the result returned here
### will be resupplied on multiple subsequent calls to updateANNFields below
###
### In our case, we're interested in the Epigenetic annotation rows.  We will gather up
### information from all of them, which we apply to all Transcript rows later.
###
def getSetWideInfo(ANNFields):

    Reg = set()
    Mot = set()
    RegMotCons = set()

    for A in ANNFields:

        if A['Feature_type'] == 'RegulatoryFeature':
            Reg.add(A['BIOTYPE'])
            RegMotCons.add(A['Consequence'])

        if A['Feature_type'] == 'MotifFeature':
            Motif_Details = ':'.join([A['MOTIF_NAME'],
                                      A['MOTIF_POS'],
                                      A['HIGH_INF_POS'],
                                      A['MOTIF_SCORE_CHANGE']
                                     ])
            Mot.add(Motif_Details)
            RegMotCons.add(A['Consequence'])

    SWinfo = [ '&'.join(sorted(list(Reg))), '&'.join(sorted(list(Mot))), '&'.join(sorted(list(RegMotCons))) ]

    return SWinfo


def updateANNFields(ANN, SWinfo):

    ###
    ### force the epigenetic annotations (captured above) onto a given transcript row
    ###
    ### NOTE: This is a bit sketchy as the epigenetic annotations do NOT actually claim
    ### to be related to the same gene/transcript.  But since it's falling on the same spot
    ### we will assume there's a connection.
    ###
    Reg, Mot, RegMotCons = SWinfo
    ANN['Reg'] = Reg
    ANN['Mot'] = Mot
    if RegMotCons != '':
        ANN["Consequence"] += '&' + RegMotCons

###
### Is this specific annotation row one that we are actually interested it?
###
def rowApplicable(Fields, GeneModel, Phenotype, AB):
        ANN = Fields['INFO'][AB.ANN_tag]

        ### don't bother, if we're lost in space
        if ANN['Consequence'] == 'intergenic_variant':
            return False

        ### Ignore all but the desired rows of annotations (HGNC transcripts of relevant Biotypes)
        if ANN['BIOTYPE'] not in biotypes_of_interest:
            return False

        ### Because our Cache/AnalyzeCase workflow is (to-date) Ensembl only?  i.e. avoid RefSeq/EntrezGene
        if ANN['SYMBOL_SOURCE'] != 'HGNC':
            return False

        ## We only want a selected set of genes
        Gene = getGene(Fields, AB)
        if Gene not in GeneModel and '*' not in GeneModel:
            return False

        ## Sanity check that we're understanding the annotations properly
        if ANN['Feature_type'] != 'Transcript' or not re.search('ENST', ANN['Feature']):
            sys.exit("%s variant has unknown Feature Type/Feature: (%s) at %s %d %s %s"
                      % ( Consequence, Feature_type, record.CHROM, record.pos, record.REF, record.ALT ))

        return True

