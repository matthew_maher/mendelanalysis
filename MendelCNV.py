#!/usr/bin/env python3

import re
import sys
import copy
from collections import defaultdict
import MendelBase as MB
import MendelArgs as MA
import MendelFile as MF

class CNV_source:
    ###
    ### What command line switches do we use/support? 
    ### They can either be new, or their description must match exactly the existing
    ###
    bin_switches = { 't': 'Treat CNVs >= 4 as tandem repeat (HET, not HOM)',
                   }
    val_switches = { 'C': '<CNV call file>',
                   }

    

    def setup(self, *statics):          ## will be called by AnalyzeCase.py
        (self.args, 
         self.samples, 
         self.AB) = statics

        self._load_data(self.args.C)
    
    
    def leftover_genes(self):           ## will be called by AnalyzeCase.py
        leftover_genes = sorted(self.CNVcalls_by_gene)
        for leftover_gene in leftover_genes:
            yield leftover_gene
        return
    
    
    def calls_for_gene(self, gene, GeneModel):     ## will be called by AnalyzeCase.py
    
        for CNVid in sorted(self.CNVcalls_by_gene[gene]):
    
            variant_name = f"CNV-CNV-WT-{CNVid}"
    
            ## We need to get all genotypes for this CNV.  Start by assuming everyone is WT
            GTs = {}
            for sample in self.samples:
                GTs[sample] = ['0','0']
    
            ## The CNV data has entries for each sample that has a/any call for this CNV
            for cnvcall in self.CNVcalls_by_gene[gene][CNVid]:
                (cnvsample, cn, cnvchr, start, stop, cnvsymbol) = cnvcall
    
                ## But what sort of call is it?
                ## Should we treat as HET or HOM CNV?  
                ## Default:  Copy Number of 0 or 4+ is HOM   and 1 or 3 is HET
                ## if user supplied -t then treat 4+ as HET on theory that it is a tandem multi-repeat
                if cn == 0 or (cn >= 4 and not self.args.t):
                    GTs[cnvsample] = ['1','1']
                else:
                    GTs[cnvsample] = ['1','0']
    
            ###
            ### How to score a CNV?   A difficult question
            ### We'll use the DUP/DEL nature and whether we're scoring for LoF/GoF to get a starting point
            ### And then make several adjustments 
            ###
    
            ### Determine score for this CNV call
            if gene in GeneModel:
                LoFGoF = 'LoF' if MB.LoF(gene, self.args.DiseaseModel, GeneModel[gene]['HI-Dom']) else 'GoF'
            else:
                LoFGoF = 'GoF'
            (CNV_score, CNV_start_score, affected_factor, CNV_factor) = \
                 self._score_one_CNV(LoFGoF, CNVid, cn)
    
            ###
            ### Store everything in the variant_data dict which the disease-model-specific routines with use as input
            ###
            variant_data = {}
            variant_data['POS'] = ''
            variant_data['REF'] = 'WT'
            variant_data['ALT'] = CNVid + ":" + str(cn)
            variant_data['GQ'] = '99'
            variant_data['TYPE'] = 'CNV'
            variant_data['SCORE'] = CNV_score
            variant_data['SYMBOL'] = cnvsymbol
            variant_data['CHROM'] = cnvchr
            variant_data['RANGE'] = ''                              ## CNV do not support the -R ranges logic
            variant_data['CONSEQ'] = ''                             ## CNV do not support the -U UTR logic
            variant_data['CNV_info'] = (cnvchr, start, stop, cn)    ## this is used in review_calls below

            variant_data['GTs'] = GTs
    
            yield (variant_name, variant_data)
    
        ### Clear this data from the CNV stash because when SNVs are exhausted, we'll take a final look to see 
        ### what was missed (see end of code's main line) - possibly a CNV in a gene with no SNV calls
        del self.CNVcalls_by_gene[gene]
        return
    
    ###
    ### The method below be will called by AnalyzeCase once all the calls are loaded from all sources.
    ### This give us an opportunity to do something perhaps a tad experimental:
    ###
    ###   See if a HOM SNV is spanned by a HET CNV deletion, meaning SNV may really be HEMI
    ###   if we find one, re-add the SNV variant into the callset again, but as a HET/HEMI.
    ###   It should then get paired with the spanning CNV as a compound HET call.
    ###   This prevents the hiding of what is likely a highly relevant/true CNV call
    ###
    def review_calls(self, calls, samples):

        ### First build lists of all pending CNV and SNV calls
        CNV_calls = [x for x in calls if calls[x]['TYPE'] == 'CNV']
        SNV_calls = [x for x in calls if calls[x]['TYPE'] == 'SNV']

        ### And now we need to examine all pairwise conbinations of the two lists
        for SNV_call in SNV_calls:
            SNV_chr = calls[SNV_call]['CHROM']
            SNV_pos = calls[SNV_call]['POS']

            ### We want to prepare for the possibility that some HOM calls are really HEMI
            new_call = copy.deepcopy(calls[SNV_call])     ## Make a copy of the HOM SNV call
            new_call['HEMI'] = 1                          ## flag it as HEMI
            new_call['HEMI_SAMPS'] = []                   ## We will track the samples, if any

            for CNV_call in CNV_calls:
                (CNV_chr, CNV_start, CNV_stop, CN) = calls[CNV_call]['CNV_info']

                ### Pause for a reality check
                if CNV_chr != SNV_chr:
                    sys.exit(f"Huh? Mismatched Chrs ({SNV_chr}<->{CNV_chr}) between SNV and CNV?")

                ### For this SNV / CNV combination: does the CNV span the SNV and is the CNV a -1 DEL?
                if CNV_start <= SNV_pos and CNV_stop >= SNV_pos and CN == 1:

                    ### If so, we need to inspect each sample in our data
                    for sample in samples:

                        ### if a sample is HOM for the SNV and has the -1 DEL CNV...
                        ### then we suspect, the HOM could really be HEMI
                        if calls[SNV_call]['GTs'][sample] == ['1', '1'] and calls[CNV_call]['GTs'][sample] == ['1', '0']:
                            new_call['GTs'][sample] = [ '1', '0' ]        ## change the genotype to HET
                            new_call['HEMI_SAMPS'].append(sample)


            ### Finally, IF we found anything during that analysis, then we want to
            ### add this new 'HEMI' call to our set for subsequent analysis/
            ### In a recessive disease model, this should allow the HEMI SNV to
            ### get paired with the -1 DEL CNV.
            ### Or, in Dominant, this should allow the SNV to be called (it would not if HOM)
            if len(new_call['HEMI_SAMPS']) > 0:
                calls[SNV_call + "-HEMI"] = new_call          ## needs a new key



    ###
    ### Load up any supplied CNV data
    ### Annoyingly we need to explicitly manage/track the symbol and chromosome
    ### Since there's no guarantee that the gene in question will be seen in the SNV input
    ###
    def _load_data(self, CNVfilename):
    
        global CNVcalls_by_gene
        global CNVcalls_by_sample
        global CNVcalls_by_CNVid
    
        self.CNVcalls_by_gene      = defaultdict(lambda: defaultdict(lambda: []))
        self.CNVcalls_by_sample    = defaultdict(lambda: [])
        self.CNVcalls_by_CNVid     = defaultdict(lambda: [])
    
        if CNVfilename == None:
            return
    
        with open(CNVfilename, "r") as cf:
            for line in cf:
                (sample, CNV_id, CNV_chr, CNV_start, CNV_stop, gene, SYMBOL, CN, *extras) = re.split(r',\s*', line)
                try:
                    CN = int(CN)
                except ValueError:
                    sys.exit(f"Copy # value ('{CN}') not numeric in CNV file: {line}")
                try:
                    CNV_start = int(CNV_start)
                except ValueError:
                    sys.exit(f"Start position ('{CNV_start}') not numeric in CNV file: {line}")
                try:
                    CNV_stop = int(CNV_stop)
                except ValueError:
                    sys.exit(f"Stop position ('{CNV_stop}') not numeric in CNV file: {line}")
    
                self.CNVcalls_by_gene[gene][CNV_id].append((sample, CN, CNV_chr, CNV_start, CNV_stop, SYMBOL))
                self.CNVcalls_by_sample[sample].append((CNV_chr, CNV_start, CNV_stop, CN))
                self.CNVcalls_by_CNVid[CNV_id].append(sample)
    
        ### Now calculate a couple critical statistics that we'll use later to adjust the scores
        ###  - average # of CNVs per sample seen IN THE INPUI FILE?
        ###  - average # of samples for a CNV called?
    
        CNV_counts_per_sample = [len(self.CNVcalls_by_sample[x]) for x in self.CNVcalls_by_sample]
        if len(CNV_counts_per_sample) > 0:
            self.avg_CNVs_per_sample = sum(CNV_counts_per_sample) / len(CNV_counts_per_sample)
    
        CNV_occurance_cnts = [len(self.CNVcalls_by_CNVid[x]) for x in self.CNVcalls_by_CNVid]
        if len(CNV_occurance_cnts) > 0:
            self.avg_CNV_occurance = sum(CNV_occurance_cnts) / len(CNV_occurance_cnts)
    
        return
    
    
    ###
    ### The Bindings module establishes a starting score for all CNVs
    ### The we will adjust them for two things:
    ###    - Does this sample have more or fewer CNV calls than the average sample? 
    ###    - Was this CNV called more or fewer times than the average CNV?
    ###
    ###    - not currently using any incoming data, but many we should? 
    ###
    def _score_one_CNV(self, LoFGoF, CNV_id, CN):
        CNV_start_score = self.AB.CNV_basescore[LoFGoF]['DEL' if CN < 2 else 'DUP']
        CNV_score_factor = self.AB.CNV_ratio_factor
     
        ### Give or take points if this sample hava fewer or more CNVs than the average (in CNV input file)
        sample_factor = 0.0
        for sample in self.samples:
            ratio = len(self.CNVcalls_by_sample[sample]) / self.avg_CNVs_per_sample
            if ratio < 1.0:
                sample_factor += CNV_score_factor * ((1 / ratio) - 1) if ratio > 0 else 0
            else:
                sample_factor -= CNV_score_factor * (ratio - 1)
    
        ### Give or take points if this CNV is rarer or more common than the average CNV (in CNV input file)
        ratio = len(self.CNVcalls_by_CNVid[CNV_id]) / self.avg_CNV_occurance
        if ratio < 1.0:
            CNV_factor =  +CNV_score_factor * ((1 / ratio) - 1)
        else:
            CNV_factor =  -CNV_score_factor * (ratio - 1)
    
        ### Perhaps we should also adjust based on some incoming quality score?

        CNV_tot_score = CNV_start_score + sample_factor + CNV_factor
    
        return (CNV_tot_score, CNV_start_score, sample_factor, CNV_factor)


    ###
    ### The "score_all" method below, plus the "__main__" block are not used as
    ### part of normal operation of AnalyzeCase.py.  They are only used when
    ### this source file is directly invoked, and allow the user to see directly
    ### how the CNVs in their call file are getting scored, which could otherwise
    ### be difficult to ascertain since many may not appear in normal output.  
    ###
    ### Close examiniation of this scoring (and in particulat the adjustments for 
    ### per-sample and per-CNV uniqueness) is warranted because different CNV inputs
    ### may have very different charactistics.  It may well be that additional 
    ### parameterization of the adjustment factors may be needed in order to get 
    ### best results with various CNV input files. 
    ###
 
    ###
    ### Just read through the CNV file and call the 'score_one...' on each 
    ### and then produced complete, ranked output
    ###
    def _score_all_CNVs(self, CNVfilename, LoFGoF):
        CNV_score_factor = AB.CNV_ratio_factor
        results = []
        with open(CNVfilename, "r") as cf:
            for line in cf:
                (sample, CNV_id, CNV_chr, CNV_start, CNV_stop, gene, SYMBOL, CN, *extras) = \
                     re.split(r',\s*', line)
                self.samples = [ sample ]
                (CNV_tot_score, CNV_start_score, sample_factor, CNV_factor) = \
                     self._score_one_CNV(LoFGoF, CNV_id, int(CN))
                results.append((CNV_tot_score, CNV_start_score, sample_factor, \
                                CNV_factor, sample, CNV_id, CN, SYMBOL))

        ordered_result = sorted(results, key=lambda tup: tup[0], reverse=True )

        print("FinalScore,StartScore,SampleFactor,CNVfactor,sample,CNV_id,CN,SYMBOL")
        for row in ordered_result:
            print("%f,%f,%f,%f,%s,%s,%s,%s" % row)


if __name__ == "__main__": 
    args = MA.ParseArgs('', 'CNVfile', 'AnnotBinding', 'LoFGoF')
    args.C = args.CNVfile

    AB = MF.getAnnotModule(args.AnnotBinding)

    extra = CNV_source()
    ### Initialize this module with our Bindings.  
    ### This scoring exercise will treat all as proband onlies
    extra.setup( args, [], AB )
    
    ### Read in the actual CNV file, which will capture some average stats
#    extra.load_data(args.CNVfile)

    ### Now reread the file, and call the scoring routine
    extra._score_all_CNVs(args.CNVfile, args.LoFGoF)

    sys.exit(0)

###
### Whenever this module gets imported, it will register with the MendelArgs module 
### as an "extra" source of variant calls.  Registration can cause additional 
### command-line switches to become available (see very top of this file) and 
### thereafter AnalyzeCase.y will interact with this "extra" source, via 
### the first three methods of this module (at top)
###
extra = CNV_source()
MA.RegisterExtra(extra)
