#!/usr/bin/env python3

import re
import sys
import copy
from collections import defaultdict
import MendelBase as MB
import MendelArgs as MA
import MendelFile as MF

class SV_source:
    ###
    ### What command line switches do we use/support? 
    ### They can either be new, or their description must match exactly the existing
    ###
    bin_switches = {
                   }
    val_switches = { 'S': '<SV call file>',
                   }

    

    def setup(self, *statics):          ## will be called by AnalyzeCase.py
        (self.args, 
         self.samples, 
         self.AB) = statics

        self._load_data(self.args.S)
    
    
    def leftover_genes(self):           ## will be called by AnalyzeCase.py
        leftover_genes = sorted(self.SVcalls_by_gene)
        for leftover_gene in leftover_genes:
            yield leftover_gene
        return
    
    
    def calls_for_gene(self, gene, GeneModel):     ## will be called by AnalyzeCase.py
    
        for SVid in sorted(self.SVcalls_by_gene[gene]):
    
            variant_name = f"SV-SV-WT-{SVid}"
    
            ## We need to get all genotypes for this SV.  Start by assuming everyone is WT
            GTs = {}
            for sample in self.samples:
                GTs[sample] = ['0','0']
    
            ## The SV data has entries for each sample that has a/any call for this SV
            for svcall in self.SVcalls_by_gene[gene][SVid]:
                (gene, SYMBOL, SV_chr, SV_start, SV_stop, SV_id, Annot, LoF_factor, GoF_factor, svsample, GT, GQ, CN, EV) = svcall
                SV_start = int(SV_start)
                SV_stop  = int(SV_stop)
                dosage = GT.count('1')
    
                if dosage == 0 and 'CNV' in SV_id:
                    call = 'HOM' if CN in ['0', '4'] else 'HET'
                if dosage == 2:
                    call = 'HOM'
                    GTs[svsample] = ['1','1']
                elif dosage == 1:
                    call = 'HET'
                    GTs[svsample] = ['1','0']
                elif dosage > 2:
                    sys.exit(f"Bad Genotype in SV call: '{GT}' in SV file: {line}")
    
            ###
            ### How to score a SV?   A difficult question
            ### We'll use the DUP/DEL nature and whether we're scoring for LoF/GoF to get a starting point
            ### And then make several adjustments 
            ###
    
            ### Determine score for this SV call
            HiDomFlag = GeneModel[gene]['HI-Dom'] if gene in GeneModel else False
            LoFGoF = 'LoF' if MB.LoF(gene, self.args.DiseaseModel, HiDomFlag) else 'GoF'
            LoFGoF_factor = float(LoF_factor) if LoFGoF == 'LoF' else float(GoF_factor)
            (SV_score, SV_start_score, SV_GQ_score) = self._score_one_SV(SV_id, LoFGoF, LoFGoF_factor, GQ, EV)
    
            ###
            ### Store everything in the variant_data dict which the disease-model-specific routines with use as input
            ###
            variant_data = {}
            variant_data['POS'] = ''
            variant_data['REF'] = 'WT'
            variant_data['ALT'] = SV_id + (":" + CN if 'CNV' in SV_id else '') + "(" + EV + ")"
            variant_data['GQ'] = GQ
            variant_data['TYPE'] = 'SV'
            variant_data['SCORE'] = SV_score
            variant_data['SYMBOL'] = SYMBOL
            variant_data['CHROM'] = SV_chr
            variant_data['RANGE'] = ''                                    ## SV do not support the -R ranges logic
            variant_data['CONSEQ'] = ''                                   ## SV do not support the -U UTR logic
            variant_data['SV_info'] = (SV_chr, SV_start, SV_stop, call, SV_id, CN)    ## this is used in review_calls below

            variant_data['GTs'] = GTs
    
            yield (variant_name, variant_data)
    
        ### Clear this data from the SV stash because when SNVs are exhausted, we'll take a final look to see 
        ### what was missed (see end of code's main line) - possibly a SV in a gene with no SNV calls
        del self.SVcalls_by_gene[gene]
        return
    
    ###
    ### The method below be will called by AnalyzeCase once all the calls are loaded from all sources.
    ### This give us an opportunity to do something perhaps a tad experimental:
    ###
    ###   See if a HOM SNV is spanned by a HET SV deletion, meaning SNV may really be HEMI
    ###   if we find one, re-add the SNV variant into the callset again, but as a HET/HEMI.
    ###   It should then get paired with the spanning SV as a compound HET call.
    ###   This prevents the hiding of what is likely a highly relevant/true SV call
    ###
    def review_calls(self, calls, samples):

        ### First build lists of all pending SV and SNV calls
        SV_calls = [x for x in calls if calls[x]['TYPE'] == 'SV']
        SNV_calls = [x for x in calls if calls[x]['TYPE'] == 'SNV']

        ### And now we need to examine all pairwise conbinations of the two lists
        for SNV_call in SNV_calls:
            SNV_chr = calls[SNV_call]['CHROM']
            SNV_pos = calls[SNV_call]['POS']

            ### We want to prepare for the possibility that some HOM calls are really HEMI
            new_call = copy.deepcopy(calls[SNV_call])     ## Make a copy of the HOM SNV call
            new_call['HEMI'] = 1                          ## flag it as HEMI
            new_call['HEMI_SAMPS'] = []                   ## We will track the samples, if any

            for SV_call in SV_calls:
                (SV_chr, SV_start, SV_stop, call, SVid, CN) = calls[SV_call]['SV_info']

                ### Pause for a reality check
                if SV_chr != SNV_chr:
                    sys.exit(f"Huh? Mismatched Chrs ({SNV_chr}<->{SV_chr}) between SNV and SV?")

                ### For this SNV / SV combination: does the SV span the SNV and is the SV a -1 DEL?
                if SV_start <= SNV_pos and SV_stop >= SNV_pos and 'DEL' in SVid and call == 'HET':

                    ### If so, we need to inspect each sample in our data
                    for sample in samples:

                        ### if a sample is HOM for the SNV and has the -1 DEL SV...
                        ### then we suspect, the HOM could really be HEMI
                        if calls[SNV_call]['GTs'][sample] == ['1', '1'] and calls[SV_call]['GTs'][sample] == ['1', '0']:
                            new_call['GTs'][sample] = [ '1', '0' ]        ## change the genotype to HET
                            new_call['HEMI_SAMPS'].append(sample)


            ### Finally, IF we found anything during that analysis, then we want to
            ### add this new 'HEMI' call to our set for subsequent analysis/
            ### In a recessive disease model, this should allow the HEMI SNV to
            ### get paired with the -1 DEL SV.
            ### Or, in Dominant, this should allow the SNV to be called (it would not if HOM)
            if len(new_call['HEMI_SAMPS']) > 0:
                calls[SNV_call + "-HEMI"] = new_call          ## needs a new key



    ###
    ### Load up any supplied SV data
    ### Annoyingly we need to explicitly manage/track the symbol and chromosome
    ### Since there's no guarantee that the gene in question will be seen in the SNV input.
    ### In that case, they will get picked up at the end during the 'leftover' process
    ###
    def _load_data(self, SVfilename):
    
        global SVcalls_by_gene
        global SVcalls_by_sample
        global SVcalls_by_SVid
    
        self.SVcalls_by_gene      = defaultdict(lambda: defaultdict(lambda: []))
        self.SVcalls_by_sample    = defaultdict(lambda: [])
        self.SVcalls_by_SVid     = defaultdict(lambda: [])
    
        if SVfilename == None:
            return
    
        with open(SVfilename, "r") as sf:
            for line in sf:
                svcall = re.split(r',\s*', line.strip())
                (gene, SYMBOL, SV_chr, SV_start, SV_stop, SV_id, Annot, LoF_score, GoF_score, svsample, GT, GQ, CN, EV) = svcall

                if svsample == 'sample':
                    continue

                self.SVcalls_by_gene[gene][SV_id].append(svcall)
                self.SVcalls_by_sample[svsample].append(SV_id)
                self.SVcalls_by_SVid[SV_id].append(svsample)
    
        ### Now calculate a couple critical statistics that we'll use later to adjust the scores
        ###  - average # of SVs per sample seen IN THE INPUI FILE?
        ###  - average # of samples for a SV called?
    
        SV_counts_per_sample = [len(self.SVcalls_by_sample[x]) for x in self.SVcalls_by_sample]
        if len(SV_counts_per_sample) > 0:
            self.avg_SVs_per_sample = sum(SV_counts_per_sample) / len(SV_counts_per_sample)
    
        SV_occurance_cnts = [len(self.SVcalls_by_SVid[x]) for x in self.SVcalls_by_SVid]
        if len(SV_occurance_cnts) > 0:
            self.avg_SV_occurance = sum(SV_occurance_cnts) / len(SV_occurance_cnts)
    
        return
    
    
    ###
    ### The Bindings module establishes a starting score for all SVs
    ### The we will adjust them for two things:
    ###    - Does this sample have more or fewer SV calls than the average sample? 
    ###    - Was this SV called more or fewer times than the average SV?
    ###
    ###    - not currently using any incoming data, but many we should? 
    ###
    def _score_one_SV(self, SVid, LoFGoF, LoFGoF_factor, GQ, EV):
        SV_start_score = self.AB.SV_basescore[LoFGoF] 
        SV_GQ_score = float(GQ) / 100.0


       #SV_score_factor = self.AB.SV_ratio_factor
     
        ### Give or take points if this sample hava fewer or more SVs than the average (in SV input file)
       #sample_factor = 0.0
       #for sample in self.samples:
       #    ratio = len(self.SVcalls_by_sample[sample]) / self.avg_SVs_per_sample
       #    if ratio < 1.0:
       #        sample_factor += SV_score_factor * ((1 / ratio) - 1) if ratio > 0 else 0
       #    else:
       #        sample_factor -= SV_score_factor * (ratio - 1)
    
        ### Give or take points if this SV is rarer or more common than the average CSV(in SV input file)
       #ratio = len(self.SVcalls_by_SVid[SV_id]) / self.avg_SV_occurance
       #if 0.0 < ratio < 1.0:
       #    SV_factor =  +SV_score_factor * ((1 / ratio) - 1)
       #else:
       #    SV_factor =  -SV_score_factor * (ratio - 1)
    
        ### Perhaps we should also adjust based on some incoming quality score?

       #SV_tot_score = SV_start_score + sample_factor + SV_factor

        SV_tot_score = SV_start_score * LoFGoF_factor + SV_GQ_score
    
       #return (SV_tot_score, SV_start_score, sample_factor, SV_factor)
        return (SV_tot_score, SV_start_score, SV_GQ_score)


    ###
    ### The "score_all" method below, plus the "__main__" block are not used as
    ### part of normal operation of AnalyzeCase.py.  They are only used when
    ### this source file is directly invoked, and allow the user to see directly
    ### how the SVs in their call file are getting scored, which could otherwise
    ### be difficult to ascertain since many may not appear in normal output.  
    ###
    ### Close examiniation of this scoring (and in particulat the adjustments for 
    ### per-sample and per-SV uniqueness) is warranted because different SV inputs
    ### may have very different charactistics.  It may well be that additional 
    ### parameterization of the adjustment factors may be needed in order to get 
    ### best results with various SV input files. 
    ###
 
    ###
    ### Just read through the SV file and call the 'score_one...' on each 
    ### and then produced complete, ranked output
    ###
    def _score_all_SVs(self, SVfilename, LoFGoF):
       #SV_score_factor = AB.SV_ratio_factor
        results = []
        with open(SVfilename, "r") as cf:
            for line in cf:
                (gene, SYMBOL, SV_chr, SV_start, SV_stop, SV_id, Annot, LoF_factor, GoF_factor, svsample, GT, GQ, CN, EV) = \
                     re.split(r',\s*', line.strip())
                if svsample == 'sample':
                    continue
                self.samples = [ svsample ]
                LoFGoF_factor = float(LoF_factor) if LoFGoF == 'LoF' else float(GoF_factor)
                (SV_tot_score, SV_start_score, SV_GQ_score) = self._score_one_SV(SV_id, LoFGoF, LoFGoF_factor, GQ, EV)
                results.append((SV_tot_score, SV_start_score, SV_GQ_score, svsample, SV_id, GQ, EV, SYMBOL, Annot))

        ordered_result = sorted(results, key=lambda tup: tup[0], reverse=True )

        print("FinalScore,StartScore,sample,SV_id,GQ,EV,SYMBOL,Annot")
        for row in ordered_result:
            print("%f,%f,%f,%s,%s,%s,%s,%s,%s" % row)


if __name__ == "__main__": 
    args = MA.ParseArgs('', 'SVfile', 'AnnotBinding', 'LoFGoF')
    args.C = args.SVfile

    AB = MF.getAnnotModule(args.AnnotBinding)

    extra = SV_source()
    ### Initialize this module with our Bindings.  
    ### This scoring exercise will treat all as proband onlies
    extra.setup( args, [], AB )
    
    ### Read in the actual SV file, which will capture some average stats
    extra._load_data(args.SVfile)

    ### Now reread the file, and call the scoring routine
    extra._score_all_SVs(args.SVfile, args.LoFGoF)

    sys.exit(0)

###
### Whenever this module gets imported, it will register with the MendelArgs module 
### as an "extra" source of variant calls.  Registration can cause additional 
### command-line switches to become available (see very top of this file) and 
### thereafter AnalyzeCase.y will interact with this "extra" source, via 
### the first three methods of this module (at top)
###
extra = SV_source()
MA.RegisterExtra(extra)
