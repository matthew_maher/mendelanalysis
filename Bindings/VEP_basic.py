#!/usr/bin/env python

###
### This file contains the details of how to score variants, based on annotations created by
###     MEEI's MBC_annotation pipeline (version 3) according to the scoring methodology
###     of the reseearchers/analysts there (version 1). 
###

import subprocess
import re
import sys
import MendelBase
import VEP
import os

###
### This module must present some items which are generic info about the annotation engine in use.
### At MEEI/MBC, that annotation engine is VEP
### In the interest of separation of VEP-specific information and MEEI/MBC-specific info and logic
### the VEP information is factored out to VEP.py, imported above and referenced at 
### appropriate spots throughout below. 
###


### All definitions below are called by some portion of the core MATK code and are required


### MATK likes to save space and make caches slightly compact, and so it encodes consequences in
### the cache file.  So the bindings must provide encode/decode mechanisms
decodeConsequence = VEP.decodeConsequence
encodeConsequence = VEP.encodeConsequence

### MATK assumes nothing about the annotation scheme and so we need to supply routines 
### to call that will pull out the Gene ID (ENSG...) or Symbol from the annotation scheme
### the cache file.  So the bindings must provide encode/decode mechanisms
getSymbol         = VEP.getSymbol
getGene           = VEP.getGene

### Under what tag in the INFO field will the per-transcript annotations be found?
ANN_tag = 'CSQ'       # historically, 'CSQ' is also used, or sometimes 'VEP'

### What are the columns that will comprise the data Cache?  Emsembl Gene is first column automatically
cacheColumns = [ 'CHROM',                               # From the primary VCF columns
                 'POS',                                 #           ditto
                 'REF',                                 #           ditto
                 'ALT',                                 #           ditto
                 'COHORT_AN',                           # Within-Cohort Allele Number
                 'COHORT_AC',                           # Within-Cohort Allele Count
                 'INFO.CSQ.gnomADg_AF',                 # GnomAD's Genome Pop Freq
                 'INFO.CSQ.SYMBOL',                     # Gene Symbol 
                 'INFO.CSQ.Feature',                    # ENST####
                 'INFO.CSQ.CANONICAL',                  # YES or blank 
                 'INFO.CSQ.HGVSc',                      # Note that this modified in callback hook above
                 'INFO.CSQ.HGVSp',                      # Note that this modified in callback hook above
                 'INFO.CSQ.Consequence',                # list of consequence codes (encoded/decoded automatrically)
                 'INFO.CSQ.BIOTYPE',                    # Annotation Engine's Biotype
               ]                                        ## Plus all the per-sample Genotypes, of course

### Of the fields listed above, which will be custom creations (below) (and thus are not really in the VCF)?
### This is so that the Bindings' supplied column list can be validated
def customFieldList():
    return [ ]


### Against which fields should AnalyzeCase.py apply -x filtering?
pop_Freq_columns = [ 'INFO.CSQ.gnomADg_AF' ]


### What are the columns that will be presented in the final output?
### There are always a set of fixed columns at far left e.g.: score,chr,pos,alt,ref
### Here we define the optional columns that this Binding will produce
outputColumns = [ 'COHORT_AC',       
                  'INFO.CSQ.gnomADg_AF',
                  'INFO.CSQ.Consequence',
                  'INFO.CSQ.Feature',
                  'INFO.CSQ.HGVSc',
                  'INFO.CSQ.HGVSp',
                ]

###########################################################################################

###
### MATK provides multiple hooks in the processing flow that allow a Binding to customize
### or alter the data along the way.  Below are the customizations desired for MEEI 
### annotation data. 
###

###
### BuildOneCache.py next calls here for each row in the Annotation table
### i.e. this is multiple times per variant.  And the set-wide information collected above is
### re-supplied so that it can be included.
###
rowApplicable = VEP.rowApplicable


###
### When BuildOneCache.py reads in a VCF record, it calls here to provide our first opportunity 
### to modify or customize the data.
###
def updateFields(Fields):
    return


###
### BuildOneCache.py next calls here for each row in the Annotation table 
### i.e. this is multiple times per variant.  And the set-wide information collected above is
### re-supplied so that it can be included.
###
def updateANNFields(ANN, SWinfo):

    # remove transcript info from the HGVS nomenclature (to save space in cache and output)
    ANN['HGVSc'] = re.sub(r'^.*:','', ANN['HGVSc'])
    ANN['HGVSp'] = re.sub(r'^.*:','', ANN['HGVSp'])
    return


###
### AnalyzeCase will call here to give us one last chance to modify values prior to output
###
def FinalAdjustments(variant_data):
    return


######################################################################################

###
### The most important routine of all.  
###
### Given a set of annotation values, stored in a dict, and a LoF/GoF indicator, score a variant
###
### Our score is simply a sum of a set of user-approved components, which have been 
### iteratively tuned.  Each component is a piece of evidence, and can be positive or negative
###
def scoreVariant(LoF, LogMaxPlausibleAF, fields, debug):

    ###
    ### Component 1: maximal consequence severity (with respect to LoF/GoF!)
    ###              The 'rank' can go up to 4 (HIGH) and impact_factor of 4 for recessive means
    ###              that obvious NULL alleles get enough points right here to make it
    ###              into most reports
    ###
    cons_codes = fields['INFO.CSQ.Consequence'].split('-')
    max_impact_rank = MendelBase.maxImpact(VEP.Consequences, cons_codes, LoF)
    impact_factor = 4 if LoF else 1.5
    max_cons_impact = max_impact_rank * impact_factor

    gnomAD = fields['INFO.CSQ.gnomADg_AF']
    freq_score = MendelBase.scorePopFreq(gnomAD, LogMaxPlausibleAF, False)

    ###
    ### Combine all of our scoring components into a single value
    ###
    score = max_cons_impact + freq_score
    
    ###
    ### Debugging
    ###
    if debug == str(fields['POS']):
        print("Score Breakdown for %s:%s:%s:%s" % (fields['CHROM'], fields['POS'], fields['REF'], fields['ALT']))
        print("   max_cons = %d  (impact_factor = %d)" % (max_cons_impact, impact_factor))
        print("   MaxPlausibleAF = %f" % 10**LogMaxPlausibleAF)
        print("cons score = %f" % score)
        print("freq score = %f" % freq_score)
        print("")
    
    return score

###
### An optional maximal score for single variant when doing Recessive (pairs) scoring
### This might enforce a bit of balance on pairs, and not allow a super-high scorer to
### be paired with everything
###
MaximumRecessiveVariantScore = 20

###
### What score cutoff values should be used by default?
### Can be overidden by -Z on Analyze(Case|Cohort)
### Note that scores for recessive solutions are AVG of the variant scores, not SUM
###
DefaultScoreCutoff = { 'R': 15, 'D': 8, 'X': 15, '1': 15, 'M': 5}

###########################################################################################

###
### This binding is used with the GCNV 'extra' module, which needs various config settings:
###

### 
### Some starting arbitrary values for how CNVs will be scored
### 
CNV_basescore = { 'LoF':  { 'DEL': 15,   'DUP': 15 },
                  'GoF':  { 'DEL':  4,   'DUP': 7  } }

### And what is the magnifier for the ratio adjustments? 
CNV_ratio_factor = 2

SV_basescore = { 'LoF':  15,
                 'GoF':  4, }

### And what is the magnifier for the ratio adjustments?
SV_ratio_factor = 2

###########################################################################################

###
### Below are 'Helper' routines that exist to facilitate invocations of annotation jobs
### on the MEEI compute cluster, using the MEEI in-house annotation pipeline.
### i.e. these routines have nothing to do with the core tasks of MATK
###
def annot_cmd(jobname, invcf, outvcf):

    ### Do a check to ensure the user has the correct module loaded.  Save some confusion.
    try:
        output = subprocess.check_output("which vep",
                                 stderr=subprocess.STDOUT, shell=True).decode('ascii')
    except subprocess.CalledProcessError as e:
        sys.exit("You have no 'vep' in your command PATH.")


    ### The specific locations of certain data sources must be set in Environment Variables
    if 'VEP_CACHE' not in os.environ:
        sys.exit("You must set the VEP 'cache' location in ENV variable: VEP_CACHE")
    if 'VEP_ASSEM' not in os.environ:
        sys.exit("You must set the VEP 'assembly' value in ENV variable: VEP_ASSEM")
    if 'VEP_FASTA' not in os.environ:
        sys.exit("You must set the VEP 'fasta' value (path to reference) in ENV variable: VEP_FASTA")
    if 'GNOMAD_GZ' not in os.environ:
        sys.exit("You must set the path to the GnomAD Genome GZ-ed content in ENV variable: GNOMAD_GZ")

    vep_cache = os.environ['VEP_CACHE']
    vep_assem = os.environ['VEP_ASSEM']
    vep_fasta = os.environ['VEP_FASTA']
    gnomad_gz = os.environ['GNOMAD_GZ']

    ## A most basic 'off-line' VEP with GnomAD Frequencies added
    base_cmd = f"vep --cache --dir_cache {vep_cache} " + \
                "    --assembly GRCh38" + \
                "    --offline" + \
               f"    --fasta {vep_fasta}" + \
                "    --hgvs" + \
                "    --canonical" + \
               f"    --input_file {invcf}" + \
                "    --vcf" + \
                "    --force_overwrite" + \
               f"    --output_file {outvcf}" + \
               f"    --custom {gnomad_gz},gnomADg,vcf,exact,0,AF"

    return base_cmd

def submit_cmd(sample):
    ## Local syntax for submitting to the job queue can go here
    return ""
