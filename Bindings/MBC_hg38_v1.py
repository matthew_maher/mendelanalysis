#!/usr/bin/env python

###
### This file contains the details of how to score variants, based on annotations created by
###     MEEI's MBC_annotation pipeline (version 3) according to the scoring methodology
###     of the reseearchers/analysts there (version 1). 
###

import subprocess
import re
import sys
import MendelBase
import VEP

###
### This module must present some items which are generic info about the annotation engine in use.
### At MEEI/MBC, that annotation engine is VEP
### In the interest of separation of VEP-specific information and MEEI/MBC-specific info and logic
### the VEP information is factored out to VEP.py, imported above and referenced at 
### appropriate spots throughout below. 
###


### All definitions below are called by some portion of the core MATK code and are required


### MATK likes to save space and make caches slightly compact, and so it encodes consequences in
### the cache file.  So the bindings must provide encode/decode mechanisms
decodeConsequence = VEP.decodeConsequence
encodeConsequence = VEP.encodeConsequence

### MATK assumes nothing about the annotation scheme and so we need to supply routines 
### to call that will pull out the Gene ID (ENSG...) or Symbol from the annotation scheme
### the cache file.  So the bindings must provide encode/decode mechanisms
getSymbol         = VEP.getSymbol
getGene           = VEP.getGene

### Under what tag in the INFO field will the per-transcript annotations be found?
ANN_tag = 'ANN'       # historically, 'CSQ' is also used, or sometimes 'VEP'

### What are the columns that will comprise the data Cache?  Emsembl Gene is first column automatically
cacheColumns = [ 'CHROM',                               # From the primary VCF columns
                 'POS',                                 #           ditto
                 'REF',                                 #           ditto
                 'ALT',                                 #           ditto
                 'COHORT_AN',                           # Within-Cohort Allele Number
                 'COHORT_AC',                           # Within-Cohort Allele Count
                 'INFO.CADD',                           # CADD score
                 'INFO.GN_AF',                          # GnomAD's Genome Pop Freq
                 'INFO.EX_AF',                          # GnomAD's Exome Pop Freq
                 'INFO.ClinVar',                        # ClinVars ClinSig:Description (CUSTOMIZED)
                 'INFO.HGMD',                           # HGMD's VariantClass:Phenotype (CUSTOMIZED)
                 'INFO.ANN.SYMBOL',                     # Gene Symbol 
                 'INFO.ANN.Transcript',                 # ENST####
                 'INFO.ANN.Protein_position',           # Numerical AA position
                 'INFO.ANN.CANONICAL',                  # YES or blank 
                 'INFO.ANN.HGVSc',                      # Note that this modified in callback hook above
                 'INFO.ANN.HGVSp',                      # Note that this modified in callback hook above
                 'INFO.ANN.Consequence',                # list of consequence codes (encoded/decoded automatrically)
                 'INFO.ANN.BIOTYPE',                    # Annotation Engine's Biotype
                 'INFO.ANN.Reg',                        # Regulatory annotations
                 'INFO.ANN.Mot',                        # Motif annotations
                 'INFO.ANN.LoFTEE'                      # LoF predictor (from MacArthur lab)
               ]                                        ## Plus all the per-sample Genotypes, of course

### Of the fields listed above, which will be custom creations (below) (and thus are not really in the VCF)?
### This is so that the Bindings' supplied column list can be validated
def customFieldList():
    return [ 'INFO.ClinVar', 'INFO.HGMD', 'INFO.ANN.Reg', 'INFO.ANN.Mot', 'INFO.ANN.LoFTEE', 'INFO.ANN.Transcript' ]


### Against which fields should AnalyzeCase.py apply -x filtering?
pop_Freq_columns = [ 'INFO.GN_AF', 'INFO.EX_AF' ]


### What are the columns that will be presented in the final output?
### There are always a set of fixed columns at far left e.g.: score,chr,pos,alt,ref
### Here we define the optional columns that this Binding will produce
outputColumns = [ 'INFO.EX_AF',
                  'INFO.GN_AF',
                  'COHORT_AC',       
                  'INFO.ANN.Consequence',
                  'INFO.ANN.Transcript',
                  'INFO.ANN.HGVSc',
                  'INFO.ANN.HGVSp',
                  'INFO.CADD',
                  'INFO.HGMD',
                  'INFO.ClinVar',
                  'INFO.ANN.Reg',
                  'INFO.ANN.Mot',
                  'INFO.ANN.LoFTEE'
                ]

###########################################################################################

###
### MATK provides multiple hooks in the processing flow that allow a Binding to customize
### or alter the data along the way.  Below are the customizations desired for MEEI 
### annotation data. 
###


###
### When BuildOneCache.py reads in a VCF record, it calls here to provide our first opportunity 
### to modify or customize the data.
###
def updateFields(Fields):

    if Fields['INFO']['EX_AF'] != '':
        Fields['INFO']['EX_AF'] = float(Fields['INFO']['EX_AF'])
    if Fields['INFO']['GN_AF'] != '':
        Fields['INFO']['GN_AF'] = float(Fields['INFO']['GN_AF'])

    def bnc(s):   #"blank no comma"
        if isinstance(s, list) and len(s) == 0:
            return ''
        if isinstance(s, list):
            s = s[0]
        if s is None:
            return ''
        else:
            return s.replace(',',';')

    ### To reduce output  columns, we're going to combine two ClinVar annotations into one
    INFO = Fields['INFO']
    if 'CV_CLNSIG' in INFO or 'INFO.CV_CLNDN' in INFO:
        INFO['ClinVar'] = "%s:%s" % (bnc(INFO['CV_CLNSIG']), bnc(INFO['CV_CLNDN']))
    else:
        INFO['ClinVar'] =  ''

    ### To reduce columns, we're going to combine two HGMD annotations into one
    if 'HGMD_CLASS' in INFO or 'HGMD_PHEN' in INFO:
        INFO['HGMD'] = "%s:%s" % (bnc(INFO['HGMD_CLASS']), bnc(INFO['HGMD_PHEN']))
    else:
        INFO['HGMD'] = ''

### At the same point, BuildOneCache.py also calls this routine to provide an opportunity 
### to gather "set-wide" information from the multiple annotation rows.  In our case, 
### this is ENCODE/regulatory annotation rows, which do not correspond to any specific
### gene/transcript (but we're going to pretend that they do!)
getSetWideInfo = VEP.getSetWideInfo


###
### BuildOneCache.py next calls here for each row in the Annotation table 
### i.e. this is multiple times per variant.  And the set-wide information collected above is
### re-supplied so that it can be included.
###
rowApplicable = VEP.rowApplicable
def updateANNFields(ANN, SWinfo):

    # this is just a move to more meaningful column name
    ANN['Transcript'] = ANN['Feature']

    # remove transcript info from the HGVS nomenclature (to save space in cache and output)
    ANN['HGVSc'] = re.sub(r'^.*:','', ANN['HGVSc'])
    ANN['HGVSp'] = re.sub(r'^.*:','', ANN['HGVSp'])

    ### LoFtee creates three fields - let's combine them into one
    ANN['LoFTEE'] = ':'.join( (ANN['LoF'], ANN['LoF_filter'], ANN['LoF_flags']) )
    if ANN['LoFTEE'] == '::':
        ANN['LoFTEE'] = ''

    ### force the epigenetic annotations onto a given transcript row
    VEP.updateANNFields(ANN, SWinfo)


###
### AnalyzeCase will call here to give us one last chance to modify values prior to output
###
def FinalAdjustments(variant_data):
    ### In the rare event that a variant is called on a non-canonical transcript, let's just add 
    ### a tag to the consequence string, so as to avoid creating a new column in the output (99% blank)
    if variant_data['INFO.ANN.CANONICAL'] == 'NON':
        variant_data['INFO.ANN.Consequence'] = 'NON:' + variant_data['INFO.ANN.Consequence']


######################################################################################

###
### The most important routine of all.  
###
### Given a set of annotation values, stored in a dict, and a LoF/GoF indicator, score a variant
###
### Our score is simply a sum of a set of user-approved components, which have been 
### iteratively tuned.  Each component is a piece of evidence, and can be positive or negative
###
def scoreVariant(LoF, LogMaxPlausibleAF, fields, debug):

    ###
    ### Component 1: maximal consequence severity (with respect to LoF/GoF!)
    ###              The 'rank' can go up to 4 (HIGH) and impact_factor of 4 for recessive means
    ###              that obvious NULL alleles get enough points right here to make it
    ###              into most reports
    ###
    cons_codes = fields['INFO.ANN.Consequence'].split('-')
    max_impact_rank = MendelBase.maxImpact(VEP.Consequences, cons_codes, LoF)
    impact_factor = 4 if LoF else 1.5
    max_cons_impact = max_impact_rank * impact_factor

    ### SPECIAL CASE:
    ### If we're scoring for Dominant within RP1, then we need to look for NULL alleles starting a known position
    if not LoF and fields['INFO.ANN.SYMBOL'] == 'RP1':
        max_impact_rank = MendelBase.maxImpact(VEP.Consequences, cons_codes, True)
        if max_impact_rank == 4:    ## 4 is maximum severity. i.e. a NULL allele
            ProtPos = re.sub('\-.*$','',fields['INFO.ANN.Protein_position'])  ## this could be a range so need to check and edit
            if ProtPos != '' and ProtPos != '?' and int(ProtPos)>= 312:
                max_cons_impact = 10

    ###
    ### Component 2: Population Frequency values scored against Maximum Plausible value
    ###
    ### Score each of the Allele Frequency Values that we have available (GnomAD Exomes and Genomes)
    ###
    indel = False if len(fields['REF']) == len(fields['ALT']) else True
    gnomAD_E = fields['INFO.EX_AF']
    G_E_score = MendelBase.scorePopFreq(gnomAD_E, LogMaxPlausibleAF, indel)
    gnomAD_G = fields['INFO.GN_AF']
    G_G_score = MendelBase.scorePopFreq(gnomAD_G, LogMaxPlausibleAF, indel)

    ###
    ### Component 3: ClinVar Reports
    ###
    ClinVar_score = 0
    CV_clnsig = fields['INFO.ClinVar']
    if CV_clnsig.find('Pathogenic/Likely_pathogenic') != -1:
       ClinVar_score =   5
    elif CV_clnsig.find('Likely_pathogenic') != -1:
       ClinVar_score =   4
    elif CV_clnsig.find('Pathogenic') != -1:
       ClinVar_score =   6
    elif CV_clnsig.find('Uncertain_significance') != -1:
       ClinVar_score =   2
    elif CV_clnsig.find('Conflicting_interpretations_of_pathogenicity') != -1:
       ClinVar_score =   2
    elif CV_clnsig.find('risk_factor') != -1:
       ClinVar_score =   1
    elif CV_clnsig.find('association') != -1:
       ClinVar_score =   1
    elif CV_clnsig.find('Affects') != -1:
       ClinVar_score =   1
    elif CV_clnsig.find('drug_response') != -1:
       ClinVar_score =   1
    elif CV_clnsig.find('other') != -1:
       ClinVar_score =   1
    elif CV_clnsig.find('not_provided') != -1:
       ClinVar_score =   1
    elif CV_clnsig.find('Benign') != -1:
       ClinVar_score =  -4
    elif CV_clnsig.find('Likely_benign') != -1:
       ClinVar_score =  -2
    elif CV_clnsig.find('Benign/Likely_benign') != -1:
       ClinVar_score =  -3
    elif CV_clnsig.find('protective') != -1:
       ClinVar_score =  -3

    if ClinVar_score == 0 and len(CV_clnsig)>1:
        print("Unrecognized ClinVar String (%s)" % CV_clnsig)

    ###
    ### Component 4: HGMD Reports
    ###
    HGMD_score = 0
    if fields['INFO.HGMD'] != '':
        HGMD_Class, *rest = re.split(':', fields['INFO.HGMD'])
        HGMD_Classes = re.split(',|;', HGMD_Class)
        for Class in HGMD_Classes:
            if len(Class)>3:
                print("HGMD value (%s) too long at %s:%s" % (Class, fields['CHROM'], fields['POS']))
        if 'DM?' in HGMD_Classes:
            HGMD_score = 2
        if 'DM' in HGMD_Classes:
            HGMD_score = 4

    ###
    ### Component 5: Epigenetic Markers falling on this location
    ###
    Reg_impact = 0
    if re.match(r'promoter|CTCF', fields['INFO.ANN.Reg']):
        Reg_impact = 1 

    ###
    ### Component 6: CADD score:  every point over 20 is worth 0.1
    ###
    CADD = fields['INFO.CADD']
    CADD_impact = (float(CADD)-10) / 5 if CADD != '' else 0

    ###
    ### Combine all of our scoring components into a single value
    ###
    score = max_cons_impact + \
            G_E_score + G_G_score + \
            ClinVar_score + \
            HGMD_score + \
            Reg_impact +  \
            CADD_impact
    
    ###
    ### Debugging
    ###
    if debug == str(fields['POS']):
        print("Score Breakdown for %s:%s:%s:%s" % (fields['CHROM'], fields['POS'], fields['REF'], fields['ALT']))
        print("   max_cons = %d  (impact_factor = %d)" % (max_cons_impact, impact_factor))
        print("   MaxPlausibleAF = %f" % 10**LogMaxPlausibleAF)
        print("       GN_E_score = %f  (AF=%s)" % (G_E_score, str(-1 if gnomAD_E == None else gnomAD_E)))
        print("       GN_G_score = %f  (AF=%s)" % (G_G_score, str(-1 if gnomAD_G == None else gnomAD_G)))
        print("   ClinVar_score = %d  (%s)" % (ClinVar_score, CV_clnsig))
        print("   HGMD_score = %d" % HGMD_score)
        print("   Reg_score = %f" % Reg_impact)
        print("   CADD_score = %f" % CADD_impact)
        print("score = %f" % score)
        print("")
    
    return score

###
### An optional maximal score for single variant when doing Recessive (pairs) scoring
### This might enforce a bit of balance on pairs, and not allow a super-high scorer to
### be paired with everything
###
MaximumRecessiveVariantScore = 20

###
### What score cutoff values should be used by default?
### Can be overidden by -Z on Analyze(Case|Cohort)
### Note that scores for recessive solutions are AVG of the variant scores, not SUM
###
DefaultScoreCutoff = { 'R': 10, 'D': 5, 'X': 8, '1': 10, 'M': 5}

###########################################################################################

###
### This binding is used with the GCNV 'extra' module, which needs various config settings:
###

### 
### Some starting arbitrary values for how CNVs will be scored
### 
CNV_basescore = { 'LoF':  { 'DEL': 15,   'DUP': 15 },
                  'GoF':  { 'DEL':  4,   'DUP': 7  } }

### And what is the magnifier for the ratio adjustments? 
CNV_ratio_factor = 2

SV_basescore = { 'LoF':  15,
                 'GoF':  4, }

### And what is the magnifier for the ratio adjustments?
SV_ratio_factor = 2

###########################################################################################

###
### Below are 'Helper' routines that exist to facilitate invocations of annotation jobs
### on the MEEI compute cluster, using the MEEI in-house annotation pipeline.
### i.e. these routines have nothing to do with the core tasks of MATK
###
def annot_cmd(jobname, invcf, outvcf):

    ### Do a check to ensure the user has the correct module loaded.  Save some confusion.
    try:
        output = subprocess.check_output("which annotate_VCF",
                                 stderr=subprocess.STDOUT, shell=True).decode('ascii')
        if 'hg38_0.1/bin/annotate_VCF' not in str(output):
           sys.exit("You have wrong version - please: 'module load MBC_annotation_pipeline/hg38_0.1'")
    except subprocess.CalledProcessError as e:
        sys.exit("please: 'module load MBC_annotation_pipeline/hg38_0.1'")

    ## Local annotation command
    base_cmd = f"annotate_VCF --input_file {invcf} --output_file {outvcf}"

    ## Local syntax for submitting to the job queue
    sub_cmd = f"qsub -pe multi-thread 4 -l virtual_free=7G -l h_vmem=7G -cwd -V -b y -N ANN{jobname} {base_cmd}"

    return sub_cmd

def submit_cmd(sample):
    ## Local syntax for submitting to the job queue
    return "qsub -pe multi-thread 4 -l virtual_free=7G -l h_vmem=7G -cwd -V -b y -N JOB" + sample


