#!/usr/bin/env python

###
### This file contains a skeletal example of an Annotation Binding file as 
### needed by the MendelAnalysis software package.  The details of an actual
### Annotation Binding will be highly specific to:
###
###     1.  The actual annotation fields and formats in the incoming VCFs
###
###     2.  The desired methodology for interpreting those annotations and 
###         using them to score/rank variants for LoF or GoF
### 
### 
###

### The incoming Annotations MUST have a 'Consequence' field in the per-transcript annotations

### The actual values of those fields, and their presumed severity may vary depending
### on which annotation tool (e.g. VEP, Annovar, Snpeff, etc.) you are using.  Also note 
### that the severity of a given consequence depends greatly on whether we are evaluating for 
### loss-of-cuntion or gain-of-function.

### This example table below was derived from VEP

###
###  The various levels of consequence severity, as defined by VEP, 
###  and seen in table below. This list is ordered from least to most severe
###
Impact_ORDER = [ 'MODIFIER',
                 'LOW',
                 'MODERATE',
                 'HIGH'
               ]

Consequences = [
    #  Consequence                             LoF Impact  GoF Impact
    [ 'transcript_ablation',                   'HIGH',     'LOW'      ],
    [ 'splice_acceptor_variant',               'HIGH',     'MODERATE' ],
    [ 'splice_donor_variant',                  'HIGH',     'MODERATE' ],
    [ 'stop_gained',                           'HIGH',     'LOW'      ],
    [ 'frameshift_variant',                    'HIGH',     'LOW'      ],
    [ 'stop_lost',                             'HIGH',     'HIGH'     ],
    [ 'start_lost',                            'HIGH',     'LOW'      ],
    [ 'transcript_amplification',              'HIGH',     'LOW'      ],
    [ 'inframe_insertion',                     'MODERATE', 'MODERATE' ],
    [ 'inframe_deletion',                      'MODERATE', 'MODERATE' ],
    [ 'missense_variant',                      'MODERATE', 'HIGH'     ],
    [ 'protein_altering_variant',              'MODERATE', 'HIGH'     ],
    [ 'splice_region_variant',                 'LOW',      'LOW'      ],
    [ 'incomplete_terminal_codon_variant',     'LOW',      'LOW'      ],
    [ 'stop_retained_variant',                 'LOW',      'LOW'      ],
    [ 'synonymous_variant',                    'LOW',      'LOW'      ],
    [ 'coding_sequence_variant',               'MODIFIER', 'MODERATE' ],
    [ 'mature_miRNA_variant',                  'MODIFIER', 'MODIFIER' ],
    [ '5_prime_UTR_variant',                   'MODIFIER', 'MODIFIER' ],
    [ '3_prime_UTR_variant',                   'MODIFIER', 'MODIFIER' ],
    [ 'non_coding_transcript_exon_variant',    'MODIFIER', 'MODIFIER' ],
    [ 'intron_variant',                        'MODIFIER', 'MODIFIER' ],
    [ 'NMD_transcript_variant',                'MODIFIER', 'MODIFIER' ],
    [ 'non_coding_transcript_variant',         'MODIFIER', 'MODIFIER' ],
    [ 'upstream_gene_variant',                 'MODIFIER', 'MODIFIER' ],
    [ 'downstream_gene_variant',               'MODIFIER', 'MODIFIER' ],
    [ 'TFBS_ablation',                         'MODIFIER', 'MODIFIER' ],
    [ 'TFBS_amplification',                    'MODIFIER', 'MODIFIER' ],
    [ 'TF_binding_site_variant',               'MODIFIER', 'MODIFIER' ],
    [ 'regulatory_region_ablation',            'MODERATE', 'MODIFIER' ],
    [ 'regulatory_region_amplification',       'MODIFIER', 'MODIFIER' ],
    [ 'feature_elongation',                    'MODIFIER', 'MODIFIER' ],
    [ 'regulatory_region_variant',             'MODIFIER', 'MODIFIER' ],
    [ 'feature_truncation',                    'MODIFIER', 'MODIFIER' ],
    [ 'intergenic_variant',                    'MODIFIER', 'MODIFIER' ],
    [ 'start_retained_variant',                'MODIFIER', 'MODIFIER' ],

    [ 'CNV',                                   'HIGH',     'MODIFIER' ]
]

###
###   The above table is handy for source code maintenance.  
###   But we want to let the base code rearrange into separate dicts for actual use
###
(Consequence_Code, Impact_Rank) = MendelBase.InitializeConsequenceTable(Consequences, Impact_ORDER)


### Under what tag in the INFO field will the per-transcript annotations be found?
ANN_tag = 'ANN'       # historically, 'CSQ' is also used


### For which 'BIOTYPE's will we actually calculate scores? 
biotypes_of_interest = [ 'protein_coding', 
                         'processed_transcript'   # PLK1S1+NR2E3 are both this biotype (per VEP)
                       ] 


### What are the columns that will comprise the data Cache?
cacheColumns = [ 'CHROM',                               # From the primary VCF columns
                 'POS',                                 #           ditto
                 'REF',                                 #           ditto
                 'ALT',                                 #           ditto
                 'COHORT_AN',                           # Within-Cohort Allele Number
                 'COHORT_AC',                           # Within-Cohort Allele Count
                 'INFO.ANN.Gene',                       # Ensembl identifier (required for matching against gene model file)
                 'INFO.ANN.SYMBOL',                     # Gene Symbol 
                 'INFO.ANN.BIOTYPE',                    # Annotation Engine's Biotype
                 'INFO.ANN.CALC_cons_codes',            # list of consequence codes


                 'INFO.ANN.Feature',                    # Annotation Engine's Feature Type (e.g. transcript)
                 'INFO.ANN.CANONICAL',                  # YES or blank 
                 'INFO.CADD',                           # CADD score
                 'INFO.HGMD_CLASS',                     # HGMD's variant class
                 'INFO.HGMD_PHEN',                      # HGMD's Phenotype
                 'INFO.ANN.Protein_position',           # Numerical AA position
                 'INFO.ANN.HGVSc',                      # Local Description
                 'INFO.ANN.HGVSp',                      # Local Description (Protein)
                 'INFO.ANN.CALC_Reg',                   # Regulatory annotations
                 'INFO.ANN.CALC_Mot'                    # Motif annotations
               ]                                        ## Plus all the per-sample Genotypes, of course

### Symbol and Chromosome are required/primary output fields
### Where are they in our VCF file?
def getSymbChr(fields):
    return fields['INFO.ANN.SYMBOL'], fields['CHROM']


### Against which fields should FindCandidates.py apply -x filtering?
pop_Freq_columns = [  ]    # fields listed need to also be in above list


### What are the columns that will be presented in the final output from FindCandidates? 
### These are in addition to 'score', 'gene symbol' and 'chr' which are always the first columns.
### This column list below will repeat if FindCandidates is outputing in 'wide' mode
outputColumns = [ 'POS',
                  'REF',
                  'ALT',
                  'INFO.EX_AF',
                  'INFO.GN_AF',
                  'INFO.ANN.Consequence',
                  'INFO.ANN.HGVSc',
                  'INFO.ANN.HGVSp',
                  'INFO.CADD',
                  'INFO.HGMD',               ## Note: not in cache - added in customize_derived_fields()
                  'INFO.ClinVar',            ## Note: not in cache - added in customize_derived_fields()
                  'INFO.ANN.CALC_Reg',   
                  'INFO.ANN.CALC_Mot'
                ]

### 
### Some starting arbitrary values for how CNVs will be scored
### 
CNV_basescore = { 'LoF':  { 'DEL': 15,   'DUP': 15 },
                  'GoF':  { 'DEL':  4,   'DUP': 7  } }
### And what is the magnifier for the ratio adjustments? 
CNV_ratio_factor = 2

########################  Various subroutines to include optional functionality ###############

### modifications/additions to the fields prior to storing in the cache
### This hook is called from BuildCache
def customize_cache_Fields(fields):
    # make any desired modifications to the data in 'fields' before it is written to the cache


### modifications/additions to fields on their way to the output 
### This hook is called from FindCandidates
def customize_derived_Fields(fields):
    # make any desired modifications to the data in 'fields' before final output is written
    # new 'derived' fields can be inserted, and then included in the list of 'outputColumns' (below)


### Last, but not least, THE key subroutine:

###
### SCORE THE VARIANT, given the following parameters:
###    - are we scoring for LoF or GoF? 
###    - What is the maximal plausible pathogenic allele freq (from the gene model file)?
###    - the data structure full of the annotation/cache field contents
###
### A score is simply a user-approved computation, somewhat arbitrary
### i.e. an accumulation of evidence: each component can be positive or negative
###
### The scores have no precise meaning (e.g. probability) - they are only for relative ranking purposes
###
def scoreVariant(LoF, LogMaxPlausibleAF, fields, debug):

    ### Suggested Component 1: The annotation's consequence severity
    component1 = 0
    
    ### Suggested Component 2: Population Frequency values scored against Maximum Plausible value
    component2 = 0

    ### Suggested Component 3: Prior reports (e.g. HGMD, ClinVar, internal...)
    component3 = 0

    ### Suggested Component X: CADD?
    ### Suggested Component Y: Epigenetic Markers falling on this location
    ### and so on and so forth....
    

    ###
    ### Combine all of our scoring components into a single value
    ###
    score = component1 + \
            component2 + \
            component3        ## and so on as needed
    
    ###
    ### Debugging
    ###
    if debug == fields['POS']:
        ### Do some debugging output for this variant (e.g. the various score components)
    
    return score
