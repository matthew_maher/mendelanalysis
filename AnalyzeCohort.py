#!/usr/bin/env python3

import os
import re
import subprocess
import sys
import glob
from collections import defaultdict

import MendelArgs as MA
import MendelFile as MF

### Configure, parse, validate our command line arguments
args = MA.ParseArgs('ZXgCxitmRrUHN', 'CohortDirOrCache', 'Pedigree', 'DiseaseModel')

### Default the cutoff if not supplied
switches = MA.switchString(args, 'ZXgCxitmRrU')


### This is where we'll accumulate all the output for the runs
output_rows = defaultdict(lambda: [])

def runone(label, labelval, cache, pedigree, DM, family):
    global score_index

    if pedigree == '-' and family != '-':
        cmd = f"AnalyzeCase.py {switches} -P {family} -m -s {cache} {pedigree} {DM} -"
    else:
        cmd = f"AnalyzeCase.py {switches}             -m -s {cache} {pedigree} {DM} {family}"
    print(f"Analyzing {label} {labelval} for DiseaseModel '{DM}'...", file=sys.stderr)
    output = subprocess.check_output(cmd, shell=True).decode('utf-8')

    for line in output.splitlines():
        ## Without -m, AnalyzeCase put in whitespace for readability.  We'll redo that below
        if 'Family' in line:
           print(line)
           continue
        if line == '':
           continue
        vals = re.split(",\s*", line)
        if vals[score_index] != '':         ## if it's blank, it carries over from prior row
            score = re.sub(r'=.*$', '', vals[score_index])
        output_rows[labelval].append([ score, vals ])

###
### Get our column headers.  Not only to print them out, but we need to know where 'Score' is
###
if os.path.isdir(args.CohortDirOrCache):
    oneCache = [ x for x in glob.glob(args.CohortDirOrCache + "/*.cache")][0]
else:
    oneCache = args.CohortDirOrCache
cmd = f"AnalyzeCase.py -m -s -H {switches} {oneCache} - {args.DiseaseModel} -"
hdrs = subprocess.check_output(cmd, shell=True).decode('utf-8').strip().split(',')
score_index = hdrs.index('Score')

### output those headers we captured earlier
if not args.N:
    print(','.join(hdrs), flush=True)
    if args.H:
        sys.exit(0)

###
### We're going to loop over something - but what?
###

### Choice 1: if a directory name was supplied, we're going to loop over contents
if os.path.isdir(args.CohortDirOrCache):
    Caches = [ x for x in glob.glob(args.CohortDirOrCache + "/*.cache")]
    print(f"Found {len(Caches)} Caches to run in {args.CohortDirOrCache}", file=sys.stderr)
    for Cache in Caches:
        label = 'SampFam'
        CacheName = os.path.basename(Cache).replace('.cache','').replace('.vcf','').replace('.ANNOT','')
        runone(label, CacheName, Cache, args.Pedigree, args.DiseaseModel, '-')

### Choice 2: if a pedigree supplied, we will loop over the families in it
elif args.Pedigree != '-':
    Pedigree = MF.load_Pedigree(args.Pedigree)
    Families = Pedigree.keys()
    print(f"Found {len(Families)} Familiies to run in {args.Pedigree}", file=sys.stderr)
    for Family in Families:
        label = 'Family'
        runone(label, Family, args.CohortDirOrCache, args.Pedigree, args.DiseaseModel, Family)
    
### Choice 3: if all we have is a single cache file, lets loop over the samples within
else:
    (cacheInput, CH) = MF.load_CacheHeaders(args.CohortDirOrCache)
    samples = CH.Samples.split('\t')

    cacheInput.close()   ## don't need it - we only wanted the sample header
    print(f"Found {len(samples)} Samples to run in {args.CohortDirOrCache}", file=sys.stderr)
    for sample in samples:
        label = 'Sample'
        runone(label, sample, args.CohortDirOrCache, args.Pedigree, args.DiseaseModel, sample)

###
### We've run all the analyses and captured the output
### Now sort by each run's high score and then output everything
###

### Now rank all the runs in order of each's highest score
ordered_runs = sorted(output_rows.keys(), key=lambda x: -float(output_rows[x][0][0]))

### produce the ranked output
for run in ordered_runs:

    calls = [ x[1] for x in output_rows[run] ]

    if not args.m:
        print(f"{label}: {run}", flush=True)

    ## stuff we may need to limit output below
    c = 0
    max = float(re.sub(r'=.*$', '', calls[0][score_index]))

    for call in calls:
        print(','.join(call), flush=True)

    if not args.m:
        print("", flush=True)

print("All Done.\n", file=sys.stderr)
