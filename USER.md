# The Mendelian Analysis Toolkit ("MATK") User Guide


### Basic Operational Overview

Assuming someone did the configuration and you now have access to:

-  a Gene Model File ("**GMF**" in commands below) which has a column for at least one Phenotype ("**Phen**")
- an Annotation Bindings ("**Binding**") which defines mappings from VCF fields to a scoring formula

you are then ready to analyze **annotated** VCF files, with or without a pedigree file ("**Pedigree**") against various DiseaseModels ("**DM**"), either R, D, X, or 1.

#### Two Phases = Two Primary Commands 

As mentioned in the main [**README**](README.md), due to needs of efficiency, and to facilitate multiple repeated analyses, MATK usage proceeds in two phases:

- The annotated VCF file is reduced and summarized into a "**cache**" file
- The cache file is then used as the basis for subsequent analyses, yielding candidate variants or solutions

In the intro demo example in the main [**README**](README.md), you may have noted that a file named **proband.vcf.cache** was produced and left behind.   In the case of that demo, the cache was produced once, and then used in four subsequent analyses for different inheritance models (R, D, X, 1).   The command invoked in the demo, **BuildandCombinedAnalysis.py** is a convenience tool that bundled all of these these steps together.  Now we will unbundle those steps and look at executing each phase separately:

##### Phase One

Here's how you build a Cache from an annotated VCF with any number of samples within it:

```
BuildOneCache.py <GMF> <Phen> <Binding> <VCF Input File> <cache file to create>
```

##### Phase Two

The most basic analysis is for a single disease model (**DM**) of a cache - for either:

- a single sample cache (as in the demo in the main [**README**](README.md) )  Supply '-' for both **pedigree** and **Family**
- one family in a multi-sample cache along with a **pedigree** file (in standard six-column format)

```
AnalyzeCase.py <cache> <Pedigree> <DM> <Family>
```
The **AnalyzeCase.py** command (above) has a great many command-line options that can modify its behavior - see the 'Analysis Options' section below for more details.


### Samples, Families, Cohorts and Combined Analyses

The two basic commands shown above are all that is needed to perform a single analysis for a single proband or family.  But most users will want to do much more - a key value of MATK is its ability to automate repetitive analyses over whatever set of cases you have and whatever disease model analyses you seek to perform.  But different users' wishes and input VCFs create different needs:
- some users will want to perform multiple analyses (e.g. Recessive, Dominant, X-Linked...) on the same case(s) if the disease inheritance model is unclear.
- some users may have a large # of VCF files, each containing a single proband or family (described in a pedigree)
- some users may have a single multisample VCF file in which the samples are:
   - all independent probands
   - belong to various distinct families, all described in a pedigree file


In order to support users in each of the above situations, MATK provides flexibility in the command-line arguments in the AnalyzeCase.py command (the pedigree and family are optional) and also supports various higher-level wrapper commands that perform multiple processing steps:

##### Multiple Caches
If you have a collection of annotated VCF files, gathered in a single directory, all of which you wish to convert to cache files, you can do this:

```
BuildCaches.py <GMF> <Phen> <Binding> <VCF Directory>
```
Note the slightly different command name ('BuildCache**s**' instead of 'Build**One**Cache'), and the loss of the last command line arg (output filename) - all cache files will automatically be created directly adjacent to the VCF files found in the directory, with a '.cache' filename extension.

##### Analyzing a Cohort (collection of cases)

As noted earlier, if you have multiple cases, they could be:
- all proband-only, either in separate caches or all in a single multi-sample cache.  Set **pedigree** = '-'
- a series of families, either in per-family caches or all in a single cache, with matching pedigree file

To analyze each cohort member, use a slightly different command with one less parameter (note that the first can now be a directory):

```
AnalyzeCohort.py <cache or DIR> <pedigree> <DM>
```
Note the slightly different command name ('Analyze**Cohort**' instead of 'Analyze**Case**') and the lack of the **Family** arguments.  If a Pedigree file is supplied, all families found within it will be analyzed.  Note that the input can specify either a specific cache file, or a directory that contains multiple caches - MATK will determine the structure of your inputs and process accordingly.

##### Combined Analysis
Finally, if your case or cohort are of unknown disease model and you wish to create a unified analysis showing results for Dominant, Recessive, X-Linked and 1-hit recessives, then use this command:

```
CreateCombinedAnalysis.py <cache or DIR> <pedigree>
```
Here again, the input can specify either a specific cache file, or a directory that contains multiple caches - MATK will figure it out.  Set **pedigree** = '-' if your samples are all proband-only.

### Helpers - summary
Each of three commands described in the prior section, along with the single command (**BuildandCombinedAnalysis**) used in the demo in the main [**README**](README.md) are simply convenience/helper wrappers around the two primary commands described in the opening section of this document.  These helpers provide the ability to invoke complex analysis operations (e.g. across multiple samples, families or disease models) with a single command.  The following diagram shows the relationships between these various tools:

```mermaid
graph TD;
  BC[BuildCaches.py] ==> BOC[BuildOneCache.py]
  ACC[AnalyzeCohort.py] ==> AC[AnalyzeCase.py]
  CCA[CreateCombinedAnalysis.py] ==> ACC
  BCA[BuildandCombinedAnalysis.py] ==> BC
  BCA ==> CCA

  style AC fill:#9ff
  style BOC fill:#9ff
```



### Referencing GeneModelFiles and Annotation Bindings
The first MATK phase - building a cache file from a VCF - requires the specification of:
 - Gene Model File (**GMF**) which is a CSV file with specific columns (described in [**The Configuration Guide**](CONFIG.md))
 - Binding Module - A Python3 module that implements a variant scoring methodology based upon a particular annotation scheme, as well as the output column arrangement.

See [**The Configuration Guide**](CONFIG.md) for all the details of those two components.  This document assumes you already know the names of specific instances of these components that you wish to use.

MATK comes with locations for "built-in" Gene Models and Bindings.  Currrently the only built-ins are the '**GEDI.latest.csv**' Gene Model and the '**MBC1_v1**' Binding, both specific to Mass Eye and Ear and Inherited Retinal Degenerations.  So those values could be specified as-is in the command-line parameters to **BuildOneCache.py**.

If, on the other hand, you have built or been supplied with your own Gene Model File and Bindings specific to your phenotype and annotation scheme, and likely located in some non-MATK directory, then you should specify the *full file paths* to each in the command-line parameters to **BuildOneCache.py**.


** ENV variable support? **



### Adding in GCNV calls

MATK has the ability to integrate CNV call data with the SNV call data from your VCF files.  And more specifically, it has tools to convert the output of the GCNV tool (a component of Broad Institute's GATK) into the format required by MATK for CNV call data.  Depending on the version and manner in which you use GATK, the outputs will either be per-sample files named “\***segments_gCNV.vcf**” or a single combined "summary" file in TSV format.  In either case, MATK has a tool that will transform GCNV’s output into the form needed by MATK:
```
prepareGCNV.py <GMF> <Phenotype> <GCNVdir> <gencodeGTF> <padding>
```
```
prepareGCNVsummary.py <GMF> <Phenotype> <GCNV summary file> <gencodeGTF> <padding>
```
The Gene Model File **(GMF)** and **Phenotype** parameters are the same as described earlier for BuildOneCache.py and are used to filter which CNV calls will be passed through to the output. 

In the first form, the **GCNVdir** parameter specifies the directory containing multiple GCNV output files, which are expected to be named in the form “\*segments_gCNV.vcf”, whereas in the second form, "**GCNV summary file**" specifies a single summary result file.

Bear in mind that GCNV only makes CNV calls against the genome reference locations and does not annotate them against a gene model. Therefore MATK needs to perform this annotation and the **gencodeGTF** parameter is required to specify the GENCODE gene model file to use (e.g. “gencode.v19.annotation.gtf”).  Obviously, the specified GTF must correspond to the same genome reference that GCNV was using when it was run as well as the genome reference that is the basis of the VCF file inputs that you intend to use.

The **padding** parameter specifies a buffer around each gene in the GENCODE model that each CNV call should be assessed with respect to. i.e. if a padding value of 1000 is specified, then any CNV call that overlaps with a gene’s region padded on both sides with 1000 base pairs, will be included in the output, annotated to that gene. Note that the a single input CNV call can potentially be output multiple times if it falls within the padding region for multiple genes.

Whichever of the two command forms above you use, the output (CSV-formatted annotated CNV calls) should be captured to a filename of your choice.  You can then supply this as an extra input to **AnalyzeCase.py** or any of it's helper/wrapper scripts (see diagram above) by adding the command-line argument:  ``-C <CNV file>``

### Invoking the local (site-specific) Annotation process

As previously stated, MATK requires that VCF files are already annotated.  i.e. MATK is NOT an annotation tool.  However, it does support the ability to configure in the Bindings module the details of how to invoke the local annotation command.  This is provided as a convenience that can be helpful when working with large numbers of VCF files (as discussed in the sections below).  See [**The Configuration Guide**](CONFIG.md)) for details on how this is specified.   

IF your bindings have such a configuration, then you can invoke the local annotation command like so:

```
AnnotVCFs.py: <VCFin> <VCFout> <AnnotBinding>
```

### Preparing a cohort of single-sample VCFs

Some users may want to treat a set of samples as a single cohort for analysis, despite all the samples having individual VCF files which may be located in arbitrary file locations.  In order to assist with managing the sample set as a cohort, MATK includes a helper tool that will gather (and name) your samples into a single directory.  To use it, first create a 'manifest' file which is a simple two-column CSV of: a sample ID, a path to the sample's VCF. Then, invoke the **getVCFs.py** script, supplying the
name of your manifest file and the name of the directory where you want the VCF files collected:
```
GetVCFs.py <manifest file> <Cohort Directory>
```
The script should copy each VCF into the specified directory, named with just your supplied sample ID and '.vcf'.

If the VCF files are in need of annotation and your bindings module is configured with the details of the local annotation method (see above), then you could initiate all the annotations via **AnnotVCFs.py** (described above), but substituting input and output *directories* names for the input/output filenames demostrated above.  **AnnotVCFs.py** will recognize that your specified input is a directory and it will attempt to annotate all files named with '*.vcf' found within it, creating a matching *.cache in your specified output directory.

If the VCF files are already annotated, you could then proceed to cache-building with **BuildCaches.py**, described earlier.

When working with a cohort of many individual VCFs, filtering by in-cohort frequency (to avoid false positives from artifactual SNV calls) can be problematic.  See the **NOTE** in the next section on how to overcome this.

Once all VCFs are annotated and all caches built, actual analyses could be invoked via either **AnalyzeCohort.py** or **CreateCombinedAnalysis.py**, both described above.

### In-Cohort Frequencies

When analyzing any cohort in rare disease analysis, general population frequency (e.g. from GnomAD) of the variants being assessed is one of the strongest and most valuable filters available - too common and the variant can be trivially excluded. But it is also important, especially in target-capture sequencing projects, to consider the purely in-cohort frequency of the variants, as some variants may actually be artifacts of the sequencing platform or capture kit. Obviously, when analyzing a population of people with disease, it would be expected that some variants (e.g. the disease-causing one’s we’re seeking!) could appear more common than in the general population. But not TOO far out of proportion. So some reasonable cutoff needs to be selected for in-cohort filtering, separate from and higher than, the general population scoring threshold in use, otherwise the analysis results may contain an excess of false positives.

When using the MendelAnalysis toolkit starting with a single joint-called VCF, the in-cohort cutoff frequency can be specified as a command-line argument (-i) to AnalyzeCase.py. Then for each variant, an in-cohort frequency can be calculated (and filtering applied) since the cache file contains all the samples’ genotypes explicitly, as well as summary allele counts for the entire cohort.

**NOTE:**  in the special case of a cohort comprised of many single sample VCFs, there is no cohort-level data naturally available in the VCFs (the information is dispersed across many VCFs). As a result, you may see numerous noisey candidate solutions appearing the final output - e.g. 10% of samples all appear to be "solved" with the same “rare” variant - which are really experimental artifacts. To address this, the script **ReCalcCohortFreq.py** is designed to scan a collection of individual cache files (your ‘cohort’), determine allele counts for each variant seen in ANY and then rewrite ALL of the cache files with full cohort data about each variant. To run it, you would type:
```
ReCalcCohortFreq.py <Annot Bindings> <Cohort Directory>
```

The output will track as the script first makes a pass through each matching VCF, collecting all genotype information, and then a second pass where it will rewrite all caches in their entirety with the updated pan-cohort information.  Thereafter, use of ``-i``, described above should be possible to effectively filter artifactual calls.



### Splitting up a Cache for Performance
When your cache was built from a large joint-called sample set, comprised either of many families, or many individual probands, running many analyses can be slow due to the fact that each analysis must make a pass through the entire cache, while the cache is much larger than is actually needed for that one analysis - this is true for two reasons:
 - the cache contains rows for any variant seen in ANY sample, not just the samples that are the subject of your current analysis
 - each row in the cache contains genotypes for ALL samples, not just the ones that are the subject of your current analysis

Therefore, as a purely performance-enhancing helper, MATK supplies a tool that will split a single cache (of many samples) into a series of per-family (or per-sample) caches, all located in a single directory.  You can invoke it like this:
```
SplitCache.py <CACHEin> <Pedigree> <OutDir>
```
Once complete, the output directory should include one cache per family (or sample if you supply '-' as the pedigree).
And critically, you should then be able, when calling **AnalyzeCohort.py** or **CreateCombinedAnalysis.py** to simply substitute the newly populated directory for the name of the cache.  Both of those tools will recognize and work with either cache files or directories.  But with the cache content now split up, your analyses (whose output should be unchanged!) will complete MUCH faster.

### Debugging/investigating aid: ShowGenotypes.py
At times, you may wonder why a certain variant did not appear in your analysis results and you would like to confirm what the genotype calls were for that variant for your sample/family of interest. Extracting the genotype calls from the VCF can be somewhat tedious, so MATK provides a tool to extract the sample information from a cache file:

```
ShowGenotypes.py <CACHEin> <Pedigree> <Family> <Chr> <Pos>
```

Genotypes will be displayed for all members of the specified family, for all variants at the specified chromosomal location.  If you want to see genotypes for all samples in the cache (e.g. you don't have a pedigree at all), just specify '-' for both the pedigree and family. 