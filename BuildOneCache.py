#!/usr/bin/env python3

from time import gmtime, strftime
from collections import defaultdict, deque
from itertools import groupby
import sys
import re
import MendelArgs as MA    ## Arg  parsing
import MendelFile as MF    ## File inhaling

### Configure, parse, validate our command line arguments
args = MA.ParseArgs('IA', 'GeneModel', 'Phenotype', 'AnnotBinding', 'VCFin', 'CACHEout')

### inhale the gene model phenotype definitions
GeneModel = MF.Load_Gene_Model_File(args.GeneModel, args.Phenotype, False)
AB = MF.getAnnotModule(args.AnnotBinding)

per_gene_annots = defaultdict(lambda: defaultdict(lambda: {}))
if args.A != None:
    for extra_annot in args.A:
        (AnnotFile, AnnotLabel, *AnnotFields) = extra_annot.split(':')
        with open(AnnotFile, "r") as af:
            hdrs = af.readline().strip().split(',')
            hdrpos = {h:i for i,h in enumerate(hdrs)}
            if 'Gene' not in hdrs:
                print("When using -A, a column headed 'Gene' is required. %s does not have one." % AnnotFile)
                sys.exit(1)
            if len(AnnotFields) == 0:
                print("When using -A, you must supply at least one field to capture [-A file:label:field1:field2.. ]")
                sys.exit(1)
            for fld in AnnotFields:
                if fld not in hdrs:
                    print("The field '%s' is not seen in the column headings for %s" % (fld, AnnotFile))
                    sys.exit(1)

            for line in af:
                vals = line.strip().split(',')
                gene = vals[hdrpos['Gene']]
                for fld in AnnotFields:
                    val = vals[hdrpos[fld]]
                    per_gene_annots[AnnotLabel][fld][gene] = val

for field in AB.cacheColumns:
    if field.startswith('EXTRA.'):
        extra, source, fldname = field.split('.')
        if fldname not in per_gene_annots[source]:
            print("The binding requires an extra source named '%s' with field '%s' supplied via -A" % (source, fldname))
            sys.exit(1)

###  Open up the VCF input
VCFfile = open(args.VCFin,"r")
INFO_fields = []
for line in VCFfile:

    ### we want to note what annotation fields are available in the VCF file
    if line[0] == '#':

        ### get the Feature-based annotation block
        if line.startswith(f'##INFO=<ID={AB.ANN_tag},'):
            ANNdesc = re.sub('^.*?Format:\s*','',line)
            ANNdesc = re.sub('\">','',ANNdesc)
            ANN_fields = ANNdesc.split('|')
            continue

        ### Note any other annotations in the INFO section
        match = re.search(r"^##INFO=<ID=(.*?),",line)
        if match:
            INFO_fields.append(match.group(1))

        ### be sure to grap the sample list when it goes by
        if line.startswith('#CHROM'):
            (_, _, _, _, _, _, _, _, _, *samples) = line.strip().split("\t")
            break

### Make sure that all the fields mentioned in the configuration, which should be from the
### Annotation tool, are really included, according to the tools' VCF header
### Exception are derived fields such as Reg, or Mot; or fields derived by the Bindings
for field in AB.cacheColumns:

    ### Certain fields are, by rule, always in the cache
    if field in [ 'CHROM',
                  'POS',
                  'REF',
                  'ALT',
                  'COHORT_AN',
                  'COHORT_AC' ]:
        continue

    ### other fields, our bindings have promised tro populate
    if field in AB.customFieldList():
        continue

    ### Extra fields (supplied via -A) will be checked in the main program
    if field.startswith('EXTRA.'):
        continue

    ### Make sure that other fields the Bindings request are fdund in the input VCF
    Iprefix = 'INFO.'
    if field.startswith(Iprefix):
        if field.replace(Iprefix,'') in INFO_fields:
            continue

    Aprefix = 'INFO.' + AB.ANN_tag + '.'
    if field.startswith(Aprefix):
        if field.replace(Aprefix,'') in ANN_fields:
            continue

    sys.exit(f"Cache Field '{field}' is not valid")


def VCFreader():
    for line in VCFfile:
        (chr, pos, id, ref, alt, qual, filter, info, format, *calls) = line.strip().split("\t")
        row = {}
        row["CHROM"]  = chr
        row["POS"]    = pos
        row["REF"]    = ref
        row["ALT"]    = alt
        row["QUAL"]   = qual
        row["FILTER"] = filter
        row["INFO"]   = info
        row["FORMAT"] = format
        row["CALLS"]  = calls
        yield row

### Get the list of samples and force the single sample name if the user wants to
if args.I:
    if len(samples)==1:
        samples[0] = args.I
    else:
        sys.exit("Multiple Samples seen in VCF. Cannot force sample ID with -I.")

###
### Start getting our output file ready
###
outfile = open(args.CACHEout,"w")

### Get the headers constructed precisely
hdrs = MF.formatCacheHeaders(args.VCFin,
                             args.GeneModel,
                             args.AnnotBinding,
                             args.Phenotype,
                             gmtime(),
                             samples)
### And write them out
for hdr in hdrs:
    print(hdr, file=outfile)


###
### routine that will be called each time a chromosome is completed.
### Needs to produce output:
###  - in gene order (this simplifies the task of AnalyzeCase)
###  - reduced to one annotation per variant/gene (usually canonical)
###
def dumpdata(rows, counts, chr):
    print("%s    Dumping data for %s" % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), chr), file=sys.stderr)

    for gene in sorted(rows):

        counts['g'] += 1

        for variant in sorted(rows[gene]):

            ### Check the full set to see if we have a Canonical Transcript
            ### If we don't then we will arbitrarily accept the first one (in loop below)
            ### TODO: instead, make this inspect the impacts and takes the worst one
            rowset = rows[gene][variant]
            canonical_present = 'YES' in { row[f"INFO.{AB.ANN_tag}.CANONICAL"] for row in rowset }

            for row in rowset:

                ### If there was no canonical found, then just mark the first one
                if not canonical_present:
                    row[f"INFO.{AB.ANN_tag}.CANONICAL"] = 'NON'
                    canonical_present = True

                ### we're only going to output one row per variant - either canonical or the 'NON' alternative
                if row[f"INFO.{AB.ANN_tag}.CANONICAL"] != '':

                    ### Vet+Construct the cache row field-by-field, prepending with gene and appending with genotypes
                    outrow = [ gene ]

                    for field in AB.cacheColumns:
                        if field.startswith('EXTRA.'):
                            extra, source, fldname = field.split('.')
                            if gene in per_gene_annots[source][fldname]:
                                val = per_gene_annots[source][fldname][gene]
                            else:
                                val = ''
                        else:
                            val = row[field]
                        if ',' in val:
                            print("BAD DATA: embedded comma in field %s: %s" % (field, val), file=sys.stderr)
                            val = re.sub(',',';',val)
                        outrow.append(val)

                    outrow.extend(row['GTs'])
                    outrow.extend(row['GQs'])

                    ### Ship it!
                    print(','.join(outrow), file=outfile)
                    counts['w'] += 1
                    break

    return

###
### Start the main iteration of the VCF rows
###
counts = defaultdict(lambda: 0)                     # counters so we can report stats when done

###
### NOTE ON POSSIBLE FUTURE IMPROVEMENT:
###
### The code below let's an entire chromosome's worth of data build up before calling
### dumpdata() to output it in gene order. For very large VCFs, this could get memory-prohibitive
###
### An alternative would be to change this implementation so that genes would be output once the
### current position has moved a SAFE DISTANCE (chosen arbitrarily) past the last sighting of a gene. 
### This would require maintaining the last-seen position for each gene currently in the rows dict, 
### and then frequently checking to see if any are far enough past that they can be safely output.
### That would allow purging those genes after output, so the rows dict would not grow too big.
### 
chrom_seen = {}     # we'll confirm that the input is sorted (becauase we trust no one)
for chr, inrows in groupby(VCFreader(), lambda r: r['CHROM']):
    if chr in chrom_seen:
        sys.exit("The input is not sorted by CHROM - exitting")
    chrom_seen[chr] = 1
    if '_' in chr:
        continue

    rows = defaultdict(lambda: defaultdict(lambda: [])) # will be: [<gene>][<variant tuple>]
    
    for record in inrows:
        ### Give the user a visible progress counter...
        counts['r'] += 1
        if counts['r'] % 100000 == 0:
            print("%d rows read" % counts['r'], file=sys.stderr)

        ### skip over various categories of VCF rows we're not interested in
        if 'LowQual' in record['FILTER']:        # we don't want dubious calls
            continue

        if record['ALT'] == '*':   ## these have no annotations
            continue

        if ',' in record["ALT"]:
            print(f"Skipping Multi-Allelic row at {record.CHROM} / {record.POS} / {record.REF}", file=sys.stderr)
            continue

        Fields = {}
        Fields["CHROM"] = re.sub('^chr','',record['CHROM'], flags=re.IGNORECASE)
        Fields["POS"]   = record['POS']
        Fields["REF"]   = record['REF']
        Fields["ALT"]   = record['ALT']
        Fields["INFO"] = INFO = {}

        variant_name = '-'.join([Fields['CHROM'],str(Fields['POS']),Fields['REF'],Fields['ALT' ]])

        ### Lets parse apart the INFO fields
        infovals = re.split(r";\s*", record['INFO'])
        for ival in infovals:
            if '=' not in ival:
                INFO[ival] = 1
            else:
                (key, val) = ival.split('=')
                if key != AB.ANN_tag:
                    INFO[key] = val
                    continue

                ### We hit the feature-based annotation block - need to parse the double levels
                features = val.split(",")
                INFO['ANNLIST'] = []
                for feature in features:
                    feature = feature.replace("%3A", ":")
                    feature = feature.replace("%3D", "=")
                    INFO['ANNLIST'].append({ k:v for (k,v) in zip(ANN_fields, feature.strip().split('|')) } )

        ###  Make sure that we saw the per-feature annotation block
        if 'ANNLIST' not in INFO:
            sys.exit("The '%s' tag is missing from the record for: %s" % (AB.ANN_tag, variant_name))

        ### The top-level INFO annotations are potentially not preset.  So let's go through the
        ### list of what the user intends to use and ensure at least a blank is present
        for column in AB.cacheColumns:
            parts = column.split('.')
            if parts[0] == "INFO" and parts[1] not in Fields['INFO']:
                    Fields['INFO'][parts[1]] = ''

        Fields['COHORT_AC'] = INFO['AC'] if 'AC' in INFO else 0
        Fields['COHORT_AN'] = INFO['AN'] if 'AN' in INFO else 0

        ### Needs adjusting in case its not top-level
        for FreqField in AB.pop_Freq_columns:
            Fields[FreqField] = float(Fields[FreqField]) if FreqField in Fields else ''

        ### get the stripped down genotype calls (one per sample in the VCF)
        ### And watch out for completely missing values, possible result of a merged VCF
        format_fields = record['FORMAT'].split(':')
        AN = 0
        AC = 0
        GTs = deque([])
        GQs = deque([])

        ### This should be performance - analyzed to make sure it is linear time with append calls
        for call in record['CALLS']:
            callvals = call.split(':')
            callinfo = { fld:val for (fld, val) in zip(format_fields, callvals) }

            GT = re.sub(r'\||\/','', callinfo['GT'])
            GQ = callinfo['GQ']

            GTs.append(GT)
            GQs.append(GQ)

            (a1, a2) = list(GT)
            if a1 != '.':     AN += 1
            if a2 != '.':     AN += 1
            if a1 == '1':     AC += 1
            if a2 == '1':     AC += 1

        Fields['COHORT_AC'] = AC
        Fields['COHORT_AN'] = AN

        ### Allow the Bindings to gather up any variant-wide information that they want, prior to...
        if hasattr(AB, 'getSetWideInfo'):
            SWinfo = AB.getSetWideInfo(Fields['INFO']['ANNLIST'])
        else:
            SWinfo = []
        AB.updateFields(Fields)

        ### Now move on the process each 'ANN' row in turn
        for i in range(len(Fields["INFO"]["ANNLIST"])):

            ### promote the current ANN row to be analyzed
            ANN = Fields["INFO"]["ANNLIST"][i]
            Fields["INFO"][AB.ANN_tag] = ANN

            ### Allow the Bindings to apply any of the info collected earlier
            ### and to create any desired derived fields
            AB.updateANNFields(ANN, SWinfo)

            if not AB.rowApplicable(Fields, GeneModel, args.Phenotype, AB):
                continue

            ### To save space, we will encode all the consequence strings as 2-digit #s
            ### yea, yea - I know - look at the calender!  who cares about space?!
            ### I can't help myself...  I thought Radix-50 was a good idea.
            ANN["Consequence"] = AB.encodeConsequence(ANN["Consequence"])

            ###
            ### Let's convert the data structure to something closer to the target cache output
            ###
            FlatFields = {}
            for field in AB.cacheColumns:
                if field.startswith('EXTRA.'):
                    continue
                val = Fields
                for step in field.split('.'):
                    val = val[step]
                FlatFields[field] = str(val)

            ###
            ### All annotations added - just need to add the Genotypes and Qualities
            ### Then need to add to growing list for this gene AND variant.
            ### The 'dumpdata' routine will be responsible for picking just one row per variant
            ###
            FlatFields['GTs'] = GTs
            FlatFields['GQs'] = [ '.' if q == None else str(q) for q in GQs ]

            ### Save it for dumpdata() to process later. 
            ### Note the structure being built:
            ###  rows[ <Gene> ][ <tuple identifying one variant> ] = [ list of <FlatField> structures ]
            ### dumpdata() will need to pick a winner out of the list
            Gene = AB.getGene(Fields, AB)
            rows[Gene][tuple(FlatFields[x] for x in ['CHROM', 'POS', 'REF', 'ALT'])].append(FlatFields)

    dumpdata(rows, counts, chr)

###
### With the input exhausted, don't forget to purge the last contents from the buffer and then close up shop
###
outfile.close()

###
### Move on to providing some summary statistics 
###
print("", file=sys.stderr)
print("%9d rows read " % counts['r'], file=sys.stderr)
print("%9d rows written for %d genes." % ( counts['w'], counts['g'] ), file=sys.stderr)

###
### All Done!
###
sys.exit(0)
